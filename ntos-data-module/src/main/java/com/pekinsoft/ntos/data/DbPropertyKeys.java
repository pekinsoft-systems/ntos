/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-database-utils
 *  Class      :   DbPropertyKeys.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 11, 2024
 *  Modified   :   Jun 11, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 11, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.utils.PropertyKeys;
import java.sql.*;
import java.util.prefs.Preferences;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface DbPropertyKeys extends PropertyKeys {

    /**
     * Application's {@link Preferences} key for the database user's password to
     * use for the {@link Connection}. If the default constructor is used for
     * initializing the {@code Database} object, a default password of
     * &ldquo;dbUser&rdquo; is provided.
     */
    static final String PREFS_KEY_PASSWORD = "jdbc.password";

    /**
     * Application's {@link Preferences} key for recreating the database tables.
     * If the preference is set to {@code true}, then all tables in the database
     * will be dropped and recreated, provided they have a &ldquo;.drop&rdquo;
     * file located in the &ldquo;{@code META-INF/tables}&rdquo; resource
     * directory of the Java Archive.
     */
    public static final String PREFS_KEY_RECREATE = "database.recreate";

    /**
     * Application's {@link Preferences} key for providing same data in the
     * database. If the preferences is set to {@code true}, and the table has a
     * corresponding &ldquo;.sample&rdquo; file located in the resource
     * &ldquo;{@code META-INF/tables}&rdquo; resource directory of the Java
     * Archive, then the contained sample data will be entered into the
     * corresponding tables.
     */
    public static final String PREFS_KEY_SAMPLES = "database.provide.sample.data";
    
    /**
     * Application's {@link Preferences} key for saving the user's logon 
     * credentials, so that they do not need to be entered each time the 
     * application is launched.
     */
    public static final String PREFS_KEY_SAVE_CREDENTIALS = "database.save.credentials";

    /**
     * Application's {@link Preferences} key for the database username to use
     * for the {@link Connection}. If the default constructor is used for
     * initializing the {@code Database} object, a default username of
     * &ldquo;dbUser&rdquo; is provided.
     */
    public static final String PREFS_KEY_USERNAME = "jdbc.username";

}
