/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-data-module
 *  Class      :   AbstractDataTableModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 1, 2024
 *  Modified   :   Jul 1, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 1, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data.support;

import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.table.DefaultTableModel;

/**
 * {@code AbstractDataTableModel} is a model class for the 
 * {@link javax.swing.JTable JTable} that accesses a database table for its
 * data.
 * <p>
 * The implementing class needs to override the data-centric methods that need
 * to have intimate knowledge of the table data fields. Also, the implementing
 * class needs to implement the property change event to update the 
 * {@code original} and {@code curent} fields with the appropriate data. This
 * parent class adds itself as a property change listener to all data objects,
 * as they are required to extend the {@link AbstractBean } class.
 * <p>
 * This class also implements {@link java.awt.event.KeyListener KeyListener}
 * which will need to be implemented by the subclass. This should allow for the
 * data in the table to update as the user types in the editor fields on a
 * master/detail view.
 * <p>
 * This class also implements {@link java.awt.event.ItemListener ItemListener}
 * should the subclass need to listen to changes in a
 * {@link javax.swing.JComboBox JComboBox.
 * <p>
 * For all three listeners, the subclass only needs to override those methods in
 * which they are interested. All methods from the three listeners are stubbed
 * out in this superclass.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public abstract class AbstractDataTableModel<T extends AbstractBean> extends DefaultTableModel 
        implements PropertyChangeListener, KeyListener, ItemListener {

    private static final long serialVersionUID = 1L;
    
    protected AbstractDataTableModel (List<T> list) {
        application = Application.getInstance();
        resourceMap = application.getContext().getResourceMap(getClass(), 
                AbstractDataTableModel.class);
        this.list = list;
        for (T record : this.list) {
            record.addPropertyChangeListener(this);
        }
    }
    
    public T getRecord(int row) {
        return list.get(row);
    }
    
    public abstract void insertRecord(T record);
    
    public abstract void delete(int[] rows);
    
    public void save() {
        if (adding) {
            insertData();
        } else {
            updateData();
        }
        adding = false;
    }
    
    public abstract void refresh();
    
    public void setCurrentRowSelection(int row) {
        this.row = row;
    }

    @Override
    public abstract Object getValueAt(int row, int column);

    @Override
    public abstract void setValueAt(Object aValue, int row, int column);

    @Override
    public abstract int getColumnCount();

    @Override
    public int getRowCount() {
        return list == null
                ? 0
                : list.size();
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
    }
    
    protected final Application getApplication() {
        return application;
    }
    
    protected final ResourceMap getResourceMap() {
        return resourceMap;
    }
    
    protected abstract void insertData();
    
    protected abstract void updateData();
    
    private final Application application;
    private final ResourceMap resourceMap;
    
    protected final List<T> list;
    
    protected int row = -1;
    protected T original = null;
    protected T current = null;
    
    protected boolean adding = false;

}
