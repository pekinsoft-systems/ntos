/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   NorthwindTradersPoS
 *  Class      :   Database.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 4, 2024
 *  Modified   :   Jun 4, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 4, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.*;
import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.framework.*;
import java.io.*;
import java.lang.System.Logger.Level;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.prefs.Preferences;

/**
 * The {@code Database} class is a singleton utility that manages the connection
 * to a database back-end. This class exposes an enumeration of Database Vendors
 * that includes the proper JDBC URL and driver class name, so that the
 * application developer does not need to worry about getting these things
 * right.
 * <p>
 * To make the connection, the database must be instantiated from the primary
 * application class, so that the rest of the classes that make up the
 * application can just call {@link Database#getInstance()} in order to use it.
 * <p>
 * The application developer needs to create a &ldquo;Database.properties&rdquo;
 * file in the Java Archive's &ldquo;{@code META-INF/}&rdquo; directory that can
 * be accessed by this class to configure the {@code Database}. The properties
 * that need to be included are:
 * <dl>
 * <dt>{@code jdbc.vendor}</dt>
 * <dd>This is the name of the vendor for the database back-end that will be
 * used by the application. The valid values are:
 * <ul>
 * <li>DERBY_EMBEDDED</li>
 * <li>DERBY_SERVER</li>
 * <li>H2_DB</li>
 * <li>HSQLDB</li>
 * <li>IBM_DB2</li>
 * <li>MARIADB</li>
 * <li>MICROSOFT</li>
 * <li>MYSQL</li>
 * <li>ORACLE</li>
 * <li>POSTGRESQL</li>
 * </ul></dd>
 * <dt>{@code jdbc.database.name}</dt>
 * <dd>The name of the database to which a connection is to be made.</dd>
 * <dt>{@code jdbc.database.schema}</dt>
 * <dd>The name of the schema in the database in which the tables are to be
 * created and accessed.</dd>
 * </dl>
 * <p>
 * The username and password should be retrieved in whatever manner makes sense
 * in the context of the application, then stored in the user preferences under
 * the node &ldquo;{@code ${Application.User.Node}/jdbc.username}&rdquo; and
 * &ldquo;{@code ${Application.User.Node}/jdbc.password}&rdquo;. This allows the
 * {@code Database} class to pull from the application's preferences node to
 * retrieve this information for the connection. When constructing the
 * {@code Database} object, the alternate constructor that takes a default
 * username and password can be used, which will allow for there to not be any
 * of this information stored in the preferences system.
 * <p>
 * <dl>
 * <dt><strong><em>NOTE</em></strong>:
 * <dd>When storing the user's password in the application's preferences, it
 * <em>must be</em> stored as a {@code byte[]} ({@code byte} array}). This does
 * two things:
 * <ol>
 * <li>Convolutes the password from a casual observer.</li>
 * <li>Simplifies the generacism of obtaining the password by the
 * {@code Database}.</li>
 * </ol>
 * <p>
 * However, if storing the user's password in the preferences system, this
 * should only be done during the application run. As the application exits, the
 * user's password <em>should be</em> removed from the preferences node for the
 * sake of security. If the application provides a means to store the password
 * for future use, a more secure storage model should be used instead for the
 * longer term password storage.
 * </dl>
 * <h2>Database Creation On-the-Fly</h2>
 * For the creation of the database on an application's first run, there needs
 * to be a directory named &ldquo;{@code META-INF/tables/}&rdquo; that contains
 * one file for each table to be created. Each file should be named as
 * &ldquo;TABLE_NAME.create&rdquo;. The file's basename will be used as the name
 * of the table in the database, and the extension lets {@code Database} know
 * that the file contains the field definitions for the table. Understand, the
 * contents of the file should <em>only define</em> the fields of the table.
 * {@code Database} will supply the rest of the SQL statement to create the
 * table. For example, a &ldquo;CUSTOMER_TYPES.create&rdquo; table creation file
 * would be defined as follows:{@snippet lang="properties":
 * TYPE_ID BIGINT NOT NULL PRIMARY KEY, TYPE_NAME CHAR(25) NOT NULL UNIQUE, DESCRIPTION VARCHAR(1000)
 * }
 * Notice, only the field definitions are provided, as well as the primary key
 * being defined. This file is an example of a fairly short table definition. If
 * the table definition is large, each field may be entered on its own line,
 * provided each line ends with a comma to separate the individual field
 * definitions from one another in the generated SQL statement.
 * <p>
 * If tables should be dropped prior to being created, if they exist, then a
 * second file for each such table should be included in the {@code tables}
 * directory, named the same, but with the file extension
 * &ldquo;{@code .drop}&rdquo;. For example, if the database is to be recreated
 * during development each time the application is executed, the matching drop
 * file for the &ldquo;CUSTOMER_TYPES&rdquo; table above would be:
 * &ldquo;CUSTOMER_TYPES.drop&rdquo;. The drop files do not need to have any
 * contents in them, they simply need to exist in the
 * &ldquo;{@code tables}&rdquo; directory under &ldquo;{@code META-INF}&rdquo;.
 * <h2>Providing Example Data</h2>
 * If an application would like to provide sample data for its database, then in
 * the &ldquo;{@code META-INF/tables/}&rdquo; should be additional files for
 * each table that should contain sample data, which is named the same but with
 * the extension &ldquo;.sample&rdquo;. Each line of that file should be the
 * values to be added to the table, listed in the <em>same order</em> as the
 * fields in the &ldquo;{@code .create}&rdquo; file. For example, using our
 * &ldquo;CUSTOMER_TYPES&rdquo; example from above, the sample data file would
 * be named &ldquo;{@code CUSTOMER_TYPES.sample}&rdquo;, and have the following
 * null contents:{@snippet lang="properties":
 * 1, 'Residential', 'An individual person who purchases from us.',
 * 2, 'Commercial', 'A business that purchases from us and typically has a discount and/or credit line.',
 * 3, 'Supplier', 'A business or individual from whom we purchase our goods and raw materials.',
 * 4, 'Shipper', 'A business from which we recieve our products, or with which we ship our goods.'
 * }
 * <p>
 * As you can see in this example, each field is separated by a comma, string
 * ({@code CHAR} and {@code VARCHAR}) values are enclosed in <em>single
 * quotes</em>, and each line ends with a comma. Furthermore, regardless of the
 * number of fields in the table, each record may <strong><em>only</em></strong>
 * be entered on a single line.
 * <p>
 * To prevent SQL errors, the application needs to provide a way of determining
 * if the sample data has already been entered into the database <em>prior
 * to</em>
 * allowing the {@code Database} class to attempt to enter it. This is due to
 * the manner in which SQL handles primary keys and unique values, such as the
 * {@code TYPE_ID} and {@code TYPE_NAME} fields in the example. During
 * development, it can be beneficial to <em>not have</em> the underlying
 * database enforce these rules to prevent such SQL errors from happening.
 * However, this will need to be changed prior to shipping a release and can
 * actually cause more work for the application developer.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Database extends AbstractBean implements DbPropertyKeys {

    /**
     * An enumeration of ten of the most common database vendors that have JDBC
     * drivers for accessing their product from Java applications.
     * <p>
     * This enumeration only provides the <em>information</em> regarding the
     * database vendors' products, <strong>not the drivers themselves</strong>.
     * The application developer <em>must provide</em> the vendor's driver
     * library for the selected database vendor.
     */
    public static enum DatabaseVendor {
        /**
         * Apache Derby Embedded Database.
         * <p>
         * Embedded database that is stored local to the application user's
         * system. This is a valid choice for a <em>single-user</em>
         * application, and simplifies the application setup for the user.
         */
        DERBY_EMBEDDED,
        /**
         * Apache Derby ORM DBMS.
         * <p>
         * Server database system that can be running on the local host or a
         * remote server on the local area network (or even across the
         * internet). This is a valid choice for a <em>multi-user</em>
         * application, but has a more complex setup for first-time use.
         */
        DERBY_SERVER,
        /**
         * H2 Database.
         * <p>
         * This vendor has both the <em>embedded</em> and <em>server</em> system
         * in a single Java Archive library. May be a good choice for
         * prototyping a database application during development, but may want
         * to be replaced for production use.
         */
        H2_DB,
        /**
         * H-SQL Database.
         * <p>
         * This vendor has both the <em>embedded</em> and <em>server</em> system
         * in a single Java Archive library. May be a good choice for
         * prototyping a database application during development, but may want
         * to be replaced for production use.
         */
        HSQLDB,
        /**
         * IBM's Database 2 ORM DBMS.
         * <p>
         * The database server developed and maintained by IBM. This is a
         * commercial product that can be found in some larger businesses and is
         * typically the database back-end for <em>mainframe</em> applications.
         */
        IBM_DB2,
        /**
         * MariaDB.
         * <p>
         * An OSS offering that is a drop-in replacement for {@link MYSQL}
         * (which is now owned by {@link ORACLE Oracle}). This is a solid ORM
         * DBMS, but only runs as a server that must be configured somewhere on
         * the local system, local area network, or internet. This is a solid
         * choice for a production system.
         */
        MARIADB,
        /**
         * Microsoft SQL Server.
         * <p>
         * This is Microsoft's <em>BackOffice</em> SQL server product. It is a
         * commercial server and uses Microsoft's version of the Structured
         * Query Language, which differs from the ANSI standard. If an
         * application is being developed specifically for the Windows operating
         * system, then this could be a solid choice. However, if developing a
         * <em>platform-independent</em> application, staying away from
         * proprietary systems, such as this one, is highly advised.
         */
        MICROSOFT,
        /**
         * MySQL Server.
         * <p>
         * The ORM DBMS originally developed as an OSS system by Sun
         * Microsystems, but is now owned by Oracle. At the time of Oracle
         * purchasing Sun, there was a lot of fear that Oracle would
         * close-source all of Sun's open-source applications, so prior to the
         * buy-out, an adventurous group forked the MySQL repository and created
         * {@link MARIADB MariaDB} as an OSS drop-in replacement for MySQL.
         * However, MySQL is still a solid choice for production systems.
         */
        MYSQL,
        /**
         * Oracle Database ORM DBMS.
         * <p>
         * This is the database server offering from Oracle that has been around
         * since before Oracle bought Sun Microsystems. Many larger businesses
         * have their data stored on Oracle's DBMS, so this is a solid choice
         * for a production system.
         */
        ORACLE,
        /**
         * PostGreSQL.
         * <p>
         * Useful in certain situations, it is included here simply because it
         * is OSS, and we support all OSS applications.
         */
        POSTGRESQL;

        /**
         * Retrieves the proper connection URL prefix for the selected
         * {@code DatabaseVendor}.
         * <p>
         * Use of this method makes sure that the built-up database URL starts
         * with the proper URL prefix for the selected {@code DatabaseVendor}.
         *
         * @return the vendor's URL prefix for connecting to their databases
         */
        public String getURL() {
            return switch (this) {
                case DERBY_EMBEDDED, DERBY_SERVER ->
                    "jdbc:derby:";
                case H2_DB ->
                    "jdbc:h2:";
                case HSQLDB ->
                    "jdbc:hsqldb:hsql:";
                case IBM_DB2 ->
                    "jdbc:db2:";
                case MARIADB ->
                    "jdbc:mariadb:";
                case MICROSOFT ->
                    "jdbc:sqlserver:";
                case MYSQL ->
                    "jdbc:mysql:";
                case ORACLE ->
                    "jdbc:oracle:thin:";
                case POSTGRESQL ->
                    "jdbc:postgresql:";
            };
        }

        /**
         * Retrieves the JDBC driver fully-qualified class name for the current
         * {@code DatabaseVendor} instance.
         * <p>
         * Use of this method helps to load the correct driver and make the
         * connection to the database.
         *
         * @return the fully-qualified class name of the vendor's JDBC driver
         */
        public String getDriverClassName() {
            return switch (this) {
                case DERBY_EMBEDDED ->
                    "org.apache.derby.jdbc.EmbeddedDriver";
                case DERBY_SERVER ->
                    "org.apache.derby.jdbc.ClientDriver";
                case H2_DB ->
                    "org.h2.Driver";
                case HSQLDB ->
                    "org.hsqldb.jdbc.JDBCDriver";
                case IBM_DB2 ->
                    "com.ibm.db2.jcc.DB2Drvier";
                case MARIADB ->
                    "org.mariadb.jdbc.Driver";
                case MICROSOFT ->
                    "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                case MYSQL ->
                    "org.mysql.jdbc.Driver";
                case ORACLE ->
                    "oracle.jdbc.driver.OracleDriver";
                case POSTGRESQL ->
                    "org.postgresql.Driver";
            };
        }

    }

    private static Database instance = null;

    public static Database getInstance() {
        return instance;
    }

    public static void configureDatabase(ApplicationContext context,
            String username, char[] password) {
        instance = new Database(context, username, password);
    }

    /**
     * Constructs a {@code Database} object instance that provides for a default
     * username and password if the application does not set one to the user's
     * preferences node.
     *
     * @param context  the {@code Application} in which the database is being
     *                 used
     * @param username the username for the user logging onto the database
     * @param password the user's database password
     */
    public Database(ApplicationContext context, String username, char[] password) {
        Objects.requireNonNull(context, "The applciation class cannot be null.");

        this.context = context;
        resourceMap = context.getResourceMap(getClass()); // the RM for the App

        this.username = username;
        this.password = String.copyValueOf(password);

        dbProps = new Properties();
        try {
            InputStream is = getClass().getClassLoader()
                    .getResourceAsStream("META-INF/Database.properties");
            dbProps.load(is);
        } catch (IOException e) {
            context.getApplication().exit(SysExits.EX_IOERR);
        }

        vendor = DatabaseVendor.valueOf(dbProps.getProperty("jdbc.vendor"));
        if (vendor == null) {
            context.getApplication().exit(SysExits.EX_DRIVER);
        }
        notDerby = !getVendor().equals(DatabaseVendor.DERBY_EMBEDDED)
                && !getVendor().equals(DatabaseVendor.DERBY_SERVER);
        dbName = generateFullDatabaseName(
                dbProps.getProperty("jdbc.database.name"));
        schema = dbProps.getProperty("jdbc.database.schema");
        connectionURL = vendor.getURL() + dbName;
        if (vendor == DatabaseVendor.DERBY_EMBEDDED
                || vendor == DatabaseVendor.DERBY_SERVER) {
            connectionURL += ";create=true";
        }

        setNeedsCreated(Preferences.userNodeForPackage(getClass())
                .getBoolean(PREFS_KEY_RECREATE, false));

        checkDatabaseConfiguration();
    }

    public Properties getDbProps() {
        return dbProps;
    }

    public DatabaseVendor getVendor() {
        return vendor;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public boolean isNeedsCreated() {
        return Preferences.userNodeForPackage(context.getApplicationClass())
                .getBoolean("first.run", true) || needsCreated;
    }

    public final void setNeedsCreated(boolean needsCreated) {
        this.needsCreated = needsCreated;
    }

    /**
     * Retrieves a <em>one-off</em> {@link Connection} object to use for
     * accessing the database.
     * <p>
     * The {@code Connection} object that is returned <strong><em>is
     * not</em></strong>
     * cached, and a new {@code Connection} is established each time this method
     * is called. Therefore, it is important that the calling class
     * {@link Connection#close() close the {@code Connection}} object when it is
     * no longer needed.
     *
     * @return a <em>one-off</em> {@code Connection} object
     *
     * @throws SQLException if any errors occur while loading the JDBC driver or
     *                      making the connection to the database
     */
    public Connection getConnection() throws SQLException {
        System.setProperty("jdbc.drivers", vendor.getDriverClassName());

        return DriverManager.getConnection(connectionURL, username, password);
    }

    /**
     * Needs to be called when the application exits to securely dispose of the
     * {@code Database} instance. This method cleans up all resources used by
     * the {@code Database} and prepares to return control to the operating
     * system.
     */
    public void dispose() {
        for (int x = 0; x < 50; x++) {
            if ((x % 2) == 0) {
                password = null;
                username = null;
                schema = null;
                dbName = null;
            } else {
                password = String.valueOf(System.currentTimeMillis());
                username = password;
                schema = password;
                dbName = password;
            }
        }
    }

    private void checkDatabaseConfiguration() {
        Connection con = null;
        Statement stmt = null;

        try {
            String appHome = context.getLocalStorage().getDirectory().toString();
            String tempFolder = "var" + File.separator + "temp";
            Path target = Paths.get(appHome, tempFolder);
            ResourceUtils.getInstance().copyFromJar(
                    "/META-INF/tables", // Source "directory"
                    target);
            
            File resourceDir = target.toFile();

            con = getConnection();
            stmt = con.createStatement();

            if (isNeedsCreated()) {
                createTables(stmt, resourceDir);
            }

            if (Preferences.userNodeForPackage(getClass())
                    .getBoolean(PREFS_KEY_SAMPLES, false)) {
                provideSampleData(stmt, resourceDir);
            }

            Preferences.userNodeForPackage(context.getApplicationClass())
                    .putBoolean("first.run", false);
        } catch (IOException
                | URISyntaxException
                | SQLException ex) {
            String msg = ex.getClass().getSimpleName()
                    + " occurred while checking the database configuration: "
                    + ex.getMessage();
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            firePropertyChange(
                    KEY_STATUS_MESSAGE,
                    null, sm);
            Preferences.userNodeForPackage(context.getApplicationClass())
                    .putBoolean("first.run", true);

            System.out.println(sm.getMessage());
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }

                stmt = null;
                con = null;
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    private void createTables(Statement stmt, File resourceDir)
            throws IOException, SQLException {

        maybeDropTables(stmt, resourceDir);

        if (resourceDir.isDirectory()) {
            List<File> createFiles = Arrays.asList(resourceDir.listFiles(
                    f -> f.getName().endsWith(".create")));
            String create = "CREATE TABLE %1$s.%2$s (%3$s)";//"CREATE TABLE %1$s (%2$s)"; 
            if (notDerby) {
                create = "CREATE TABLE %1$s (%2$s)";
            }
            for (File file : createFiles) {
                int dot = file.getName().lastIndexOf('.');
                String tableName = file.getName().substring(0, dot);
                tableName = tableName.replace('.', '_');
                tableName = tableName.toUpperCase();

                BufferedReader in = new BufferedReader(new FileReader(file));
                String line = null;
                String builder = "";
                while ((line = in.readLine()) != null) {
                    builder += line;
                }
//                if (notDerby) {
                stmt.execute(String.format(create, schema, tableName, builder));
//                } else {
//                    stmt.execute(String.format(create,
//                            schema, tableName, builder));
//                }

                in.close();
                // Not closing the Statement object, because we may not be
                //+ done with it.

                StatusMessage status = new StatusMessage("Created Table: "
                        + schema + "." + tableName, MessageType.INFO);
                System.getLogger(Database.class.getName())
                        .log(Level.INFO, status.getMessage());
            }
        } else {
            throw new IOException("The resource \"" + resourceDir
                    + "\" is not a directory.");
        }
    }

    private void maybeDropTables(Statement stmt, File resourceDir)
            throws IOException, SQLException {
        if (resourceDir.isDirectory()) {
            List<File> dropFiles = Arrays.asList(resourceDir.listFiles(
                    f -> f.getName().endsWith(".drop")));
            String drop = "DROP TABLE %1$s";//"DROP TABLE %1$s.%2$s";
            if (notDerby) {
                drop = "DROP TABLE %1$s";
            }

            if (Preferences.userNodeForPackage(getClass())
                    .getBoolean(PREFS_KEY_RECREATE, false)) {
                for (File file : dropFiles) {
                    int dot = file.getName().lastIndexOf('.');
                    String tableName = file.getName().substring(0, dot);
                    tableName = tableName.replace('.', '_');
                    DatabaseMetaData md = getConnection().getMetaData();
                    ResultSet rs = md.getTables(null, schema,
                            tableName, new String[]{"TABLE"});

                    if (rs.next()) {
                        StatusMessage status = new StatusMessage("Dropping table \""
                                + tableName + "\" from schema \"" + schema + "\".",
                                MessageType.INFO);
                        firePropertyChange(
                                KEY_STATUS_MESSAGE,
                                null, status);
                        System.out.println(status.getMessage());
//                        if (notDerby) {
                        stmt.execute(String.format(drop, tableName));
//                        } else {
//                            stmt.execute(String.format(drop, schema,
//                                    tableName));
//                        }
                    }
                }

                // Not closing the Statement object because we are not done with it.
            }
        } else {
            throw new IOException("The resource \"" + resourceDir
                    + "\" is not a directory.");
        }
    }

    private void provideSampleData(Statement stmt, File resourceDir)
            throws IOException, SQLException {
        if (resourceDir.isDirectory()) {
            List<File> sampleFiles = Arrays.asList(resourceDir.listFiles(
                    f -> f.getName().endsWith(".sample")));
            String insert = "INSERT INTO %1$s VALUES (%2$s)";//"INSERT INTO %1$s.%2$s VALUES (%3$s)";
            if (notDerby) {
                insert = "INSERT INTO %1$s VALUES (%2$s)";
            }
            String tableName = "";
            String fieldValues = "";
            for (File file : sampleFiles) {
                int dot = file.getName().lastIndexOf('.');
                tableName = file.getName().substring(0, dot);
                tableName = tableName.replace('.', '_');
                String message = "Inserting data into table \"" + tableName
                        + "\"...";

                try {
                    int rowsAffected = 0;
                    String line = null;
                    BufferedReader in = new BufferedReader(new FileReader(file));

                    while ((line = in.readLine()) != null) {
                        fieldValues = line;
//                        if (notDerby) {
                        rowsAffected += stmt.executeUpdate(String.format(
                                insert, tableName, line));
//                        } else {
//                            rowsAffected += stmt.executeUpdate(
//                                    String.format(insert, schema,
//                                            tableName, line));
//                        }
                    }
                    message += rowsAffected + " records inserted.";
                    StatusMessage status = new StatusMessage(message, MessageType.INFO);
                    firePropertyChange(
                            KEY_STATUS_MESSAGE,
                            null, status);

                    in.close();
                    // Not closing the Statement object because we may not be done
                    //+ with it.
                } catch (SQLException | IOException e) {
                    String msg = e.getClass().getSimpleName()
                            + " in table \"" + tableName + "\", using the field"
                            + "values \"" + fieldValues + "\": "
                            + e.getMessage();
                    StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
                    firePropertyChange(
                            KEY_STATUS_MESSAGE,
                            null, sm);
                }
            }
        }
    }

    private String generateFullDatabaseName(String baseDbName) {
        File dataDir = context.getLocalStorage().getDataDirectory().toFile();
        String fullDbPath = dataDir.getAbsolutePath();
        if (!fullDbPath.endsWith(File.separator)) {
            fullDbPath += File.separator;
        }

        return fullDbPath + baseDbName;
    }

    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    private final Properties dbProps;
    private final DatabaseVendor vendor;

    private String connectionURL = null;
    private String username = "dbUser";
    private String password = "dbUser";
    private String dbName;
    private String schema;
    private boolean notDerby = false;
    private boolean needsCreated = false;

}
