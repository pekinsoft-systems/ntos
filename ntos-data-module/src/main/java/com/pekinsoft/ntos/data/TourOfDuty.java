/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   TourOfDuty.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.PreferenceKeys;
import com.pekinsoft.api.StatusDisplayer;
import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPreferenceKeys;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import static com.pekinsoft.utils.PropertyKeys.KEY_ADD_NOTIFICATION;
import static com.pekinsoft.utils.PropertyKeys.KEY_STATUS_MESSAGE;
import java.io.Serializable;
import java.sql.*;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 * The {@code TourOfDuty} class represents a period of time spent away from home
 * while performing the duties of a truck driver. Every Owner/Operator is 
 * allowed to claim a deduction on their income taxes for time away from home,
 * which is called <em>per diem</em>.
 * <p>
 * The US government allows for two methods by which this deduction can be 
 * calculated: the <em>Standard Deduction Method</em> and the <em>Exact Time
 * Method</em>. Since date math is extremely difficult and time-consuming to
 * perform, most Owner/Operators simply use the Standard Deduction Method, which
 * means that on the day they leave and the day they return home, they can claim
 * each of those days at a rate of 75%. However, this is only beneficial if the
 * Owner/Operator leaves home no earlier than 6:00 AM on the day s/he leaves, 
 * and returns home no later than 6:00 PM on the day s/he returns. The reason
 * is that if they leave prior to 6:00 AM, they are actually away from home for
 * <em>more than</em> 75% of the day. The same holds true if they return after
 * 6:00 PM. The government only allows for a single calculation method to be
 * used for the entire year, so the Owner/Operator has to decide which to use.
 * It is for this reason that almost every Owner/Operator uses the Standard
 * Deduction Method.
 * <p>
 * However, using the Standard Deduction Method tends to leave &ldquo;money on
 * the table&rdquo; come tax time. If there was an easier way to perform the
 * Exact Time Method, the Owner/Operator would be able to choose the method that
 * keeps the most money in their pocket after taxes are paid.
 * <p>
 * This is where the {@code TourOfDuty} class comes to the rescue! This class
 * tracks every tour of duty away from home, down to the millisecond, with the
 * simple click of a button. When the Owner/Operator is getting ready to leave
 * home, s/he simply clicks on a button in the application to begin their tour
 * of duty. The date and time (again, down to the millisecond) is recorded. At
 * the end of the tour of duty, the Owner/Operator simply clicks the button to
 * end the tour, and the date and time is once again stored right down to the
 * millisecond.
 * <p>
 * At the end of the year, the Owner/Operator can simply print the <em>per diem
 * </em> report and two reports will be printed: one showing the Standard
 * Deduction Method total claimable days and money; and the other showing the
 * Exact Time Method showing the total claimable days and money. The
 * Owner/Operator simply takes the report that gives them the largest possible
 * deduction to their tax preparer.
 * <p>
 * All of the tours of duty are stored in the database and may be queried via
 * the static methods of this class.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class TourOfDuty extends AbstractBean 
        implements Serializable, NtosPropertyKeys, NtosPreferenceKeys {
    
    public static final String FLD_ID = "TOUR_ID";
    public static final String FLD_START = "TOUR_START";
    public static final String FLD_END = "TOUR_END";
    public static final String FLD_DURATION = "TOUR_DURATION";

    private static final long serialVersionUID = 1L;
    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final Logger logger = context.getLogger(
            TourOfDuty.class.getName());
    
    private static final String TABLE_NAME = "TOURS_OF_DUTY";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";
    
    private static final double SECONDS_PER_DAY = 86400000.0d;
    
    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     * 
     * @return a list of all records in the table or an empty list
     */
    public static List<TourOfDuty> getAllRecords() {
        return selectRecords(null, null);
    }
    
    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param value the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static TourOfDuty findByPrimaryKey(long value) {
        String where = FLD_ID + " = " + value;
        List<TourOfDuty> records = selectRecords(where, null);
        if (records.isEmpty()) {
            return null;
        } else {
            return records.get(0);
        }
    }
    
    /**
     * Retrieves the record identified by the start date provided.
     * <p>
     * If the specified {@code start} date is {@code null}, or no records match
     * the specified date, a {@code null} value is returned.
     * 
     * @param start the start date to find
     * 
     * @return the record that matches, or {@code null}
     */
    public static TourOfDuty findByStartDate(LocalDateTime start) {
        if (start == null) {
            return null;
        }
        String where = FLD_START + " = '" + Timestamp.valueOf(start) + "'";
        List<TourOfDuty> records = selectRecords(where, null);
        if (records.isEmpty()) {
            return null;
        } else {
            return records.get(0);
        }
    }
    
    /**
     * Retrieves the record identified by the end date provided.
     * <p>
     * If the specified {@code end} date is {@code null}, or no records match
     * the specified date, a {@code null} value is returned.
     * 
     * @param end the end date to find
     * 
     * @return the record that matches, or {@code null}
     */
    public static TourOfDuty findByEndDate(LocalDateTime end) {
        String where = FLD_END + " = '" + Timestamp.valueOf(end) + "'";
        List<TourOfDuty> records = selectRecords(where, null);
        if (records.isEmpty()) {
            return null;
        } else {
            return records.get(0);
        }
    }
    
    /**
     * Retrieves all of the records for the current year. This search is based
     * on the end date. The returned list is sorted on the start date field.
     * 
     * @return all of the current year's records
     */
    public static List<TourOfDuty> findForCurrentYear() {
        LocalDateTime start = LocalDateTime.of(LocalDateTime.now().getYear(), 
                Month.JANUARY, 1, 0, 0, 0, 0);
        int year = LocalDate.now().getYear();
        LocalDate lastDayOfYear = LocalDate.of(year, 12, 31);
        LocalDateTime end = lastDayOfYear.atTime(23, 59, 59);
        String where = FLD_END + " BETWEEN '" + Timestamp.valueOf(start)
                + "' AND '" + Timestamp.valueOf(end) + "'";
        String orderBy = FLD_START + " ASC";
        return selectRecords(where, orderBy);
    }
    
    /**
     * Retrieves all records where the start date is between the two specified
     * dates, matching on the start date field.
     * <p>
     * If either of the specified dates are {@code null}, an empty list is
     * returned.
     * 
     * @param start the start of the period to locate
     * @param end the end of the period to locate
     * 
     * @return all matching records, or an empty list
     */
    public static List<TourOfDuty> findRecordsBetween(LocalDateTime start, 
            LocalDateTime end) {
        if (start == null || end == null) {
            return new ArrayList<>();
        }
        String where = FLD_START + " BETWEEN '" + Timestamp.valueOf(start)
                + "' AND '" + Timestamp.valueOf(end) + "'";
        return selectRecords(where, null);
    }
    
    /**
     * Retrieves all matching records ordered as requested.
     * 
     * @param where the filtering clause
     * @param orderBy the ordering clause
     * 
     * @return all matching records or an empty list
     */
    public static List<TourOfDuty> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }
    
    /**
     * Retrieves the total claimable days of <em>per diem</em>.
     * 
     * @param start the start of the period
     * @param end the end of the period
     * 
     * @return the total number of days of per diem
     */
    public static double getTotalPerDiem(LocalDateTime start, LocalDateTime end) {
        List<TourOfDuty> records = findRecordsBetween(start, end);
        Collections.sort(records, (TourOfDuty o1, TourOfDuty o2) -> {
            if (o1.getStart().isBefore(o2.getEnd())) {
                return -1;
            } else if (o1.getStart().equals(o2.getStart())) {
                return 0;
            } else {
                return 1;
            }
        });
        double total = 0.00d;
        TourOfDuty last = null;
        
        for (TourOfDuty record : records) {
            if (last != null) {
                total += record.getDuration();
            }
            last = record;
        }
        
        return total;
    }
    
    private static List<TourOfDuty> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        List<TourOfDuty> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                TourOfDuty record = new TourOfDuty();
                
                record.tourId = rs.getLong(1);
                record.start = rs.getTimestamp(2).toLocalDateTime();
                record.end = rs.getTimestamp(3).toLocalDateTime();
                
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, StatusDisplayer.MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }
    
    /**
     * Constructs a new, default {@code TourOfDuty}, with all fields set to 
     * their default values.
     */
    public TourOfDuty () {
        rm = context.getResourceMap(getClass());
        Preferences prefs = ((context.getPreferences(ApplicationContext.USER)
                .getBoolean(PreferenceKeys.PREF_DEVELOPMENT_ENVIRONMENT, false))
                ? context.getPreferences(ApplicationContext.USER)
                            : context.getPreferences(ApplicationContext.SYSTEM));
        
        df = DateTimeFormatter.ofPattern(prefs.get(PREF_DATE_TIME_FORMAT, 
                "yyyy-mm-dd"));
    }

    /**
     * Retrieves the unique identity value for this {@code TourOfDuty}. This
     * value is only useful for interacting with the database, as it is not used
     * anywhere else within the application.
     * 
     * @return the unique identity
     */
    public long getTourId() {
        return tourId;
    }

    /**
     * Sets the unique identity for this {@code TourOfDuty}, which is the 
     * primary key in the table. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a long, which is auto-generated by the
     * database. For this reason, the only time this method needs to be used is
     * to create a {@code TourOfDuty} instance from a database record, so this
     * method is protected for use only within this class.
     * <p>
     * This is a bound field.
     * 
     * @param tourId the unique identity
     */
    protected void setTourId(long tourId) {
        long old = getTourId();
        this.tourId = tourId;
        firePropertyChange("tourId", old, getTourId());
    }

    /**
     * Retrieves the date/time of when this {@code TourOfDuty} began.
     * 
     * @return the starting date/time
     */
    public LocalDateTime getStart() {
        return start;
    }

    /**
     * Sets the date/time of when this {@code TourOfDuty} began. This is a 
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a timestamp.
     * <p>
     * If the specified {@code start} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code start} is after the current date/time, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param start the starting date/time
     * 
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setStart(LocalDateTime start) {
        if (start == null) {
            throw new NullPointerException("start is null.");
        } else if (!isValidStartDateTime(start)) {
            throw new InvalidDataException(df.format(start) + " must be on or "
                    + "before the current date/time.");
        }
        LocalDateTime old = getStart();
        this.start = start;
        firePropertyChange("start", old, getStart());
    }

    /**
     * Retrieves the date/time of when this {@code TourOfDuty} ended, or 
     * {@code null} if the tour is ongoing.
     * 
     * @return the ending date/time, or {@code null}
     */
    public LocalDateTime getEnd() {
        return end;
    }

    /**
     * Sets the date/time of when this {@code TourOfDuty} ended. This is an <em>
     * optional</em> field at object creation, but is required later.
     * <p>
     * The data type of this field is a timestamp.
     * <p>
     * If the specified {@code end} is specified, it must fall after the
     * {@link #getStart() start date/time} and on or before the current
     * date/time. If the value is not valid under these rules, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * @param end 
     */
    public void setEnd(LocalDateTime end) {
        if (getStart() != null && end != null) {
            if (!isValidEndDateTime(end)) {
                throw new InvalidDataException(end + " must be after the "
                        + "start date/time, and on or before the current "
                        + "date/time.");
            }
        }
        LocalDateTime old = getEnd();
        this.end = end;
        firePropertyChange("tourEnd", old, getEnd());
    }

    /**
     * Retrieves the duration of this {@code TourOfDuty} as a double value that
     * states how long the tour lasted, down to the milliseconds.
     * <p>
     * <strong><em>Important Note</em></strong>: This method only works when 
     * both the {@link #getStart() start} and {@link #getEnd() end} dates/times
     * are set. If the end date/time is not set, this method returns 
     * {@code 0.0}.
     * 
     * @return the tour duration or {@code 0.0}
     */
    public double getDuration() {
        double totalDuration = 0.00;
        Duration duration = null;
        if (start != null && end != null) {
            duration = Duration.between(start, end);
        }
        
        if (duration != null) {
            totalDuration = (duration.toMillis() / SECONDS_PER_DAY);
        }
        return totalDuration;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.tourId ^ (this.tourId >>> 32));
        hash = 29 * hash + Objects.hashCode(this.start);
        hash = 29 * hash + Objects.hashCode(this.end);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TourOfDuty other = (TourOfDuty) obj;
        if (this.tourId != other.tourId) {
            return false;
        }
        if (!Objects.equals(this.start, other.start)) {
            return false;
        }
        return Objects.equals(this.end, other.end);
    }

    @Override
    public String toString() {
        DecimalFormat nf = new DecimalFormat("#.00");
        String value = rm.getString("toString.on.tour", df.format(start));
        if (end != null && isValidEndDateTime(end)) {
            value = rm.getString("toString", df.format(start), df.format(end),
                    nf.format(getDuration()));
        }
        return value;
    }
    
    public String debugginString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Account} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + ", " + FLD_START + ", " + FLD_END + ") VALUES(?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setTimestamp(1, Timestamp.valueOf(start));
            if (end != null) {
                pst.setTimestamp(2, Timestamp.valueOf(end));
            } else {
                pst.setTimestamp(2, null);
            }
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_START + " = ?, " + FLD_END + " = ? WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setTimestamp(1, Timestamp.valueOf(start));
            if (end != null) {
                pst.setTimestamp(2, Timestamp.valueOf(end));
            } else {
                pst.setTimestamp(2, null);
            }
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        if (start == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "start"));
        }
        if (end != null && !isValidEndDateTime(end)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", end,
                            rm.getString("invalid.value.reason")));
        }
    }
    
    private boolean isValidStartDateTime(LocalDateTime start) {
        LocalDateTime now = LocalDateTime.now();
        return start.isBefore(now) || start.equals(now);
    }
    
    private boolean isValidEndDateTime(LocalDateTime end) {
        LocalDateTime now = LocalDateTime.now();
        return (end.isAfter(start) 
                && (end.isBefore(now) || end.equals(now)));
    }
    
    private final ResourceMap rm;
    private final DateTimeFormatter df;
    
    private long tourId;
    private LocalDateTime start = null;
    private LocalDateTime end = null;

}
