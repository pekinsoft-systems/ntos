/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Journal.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Journal extends AbstractBean 
        implements Serializable, NtosPropertyKeys {
    
    public static final String FLD_ID = "JOURNAL_NAME";
    public static final String FLD_OPENED = "OPEN_DATE";
    public static final String FLD_CLOSED = "CLOSE_DATE";
    public static final String FLD_ACCT = "ACCOUNT";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Entry.class);
    
    private static final String TABLE_NAME = "JOURNALS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     * 
     * @return a list of all records in the table or an empty list
     */
    public static List<Journal> getAllRecords() {
        return selectRecords(null, null);
    }
    
    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     * 
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     * 
     * @return an ordered list of all records, or an empty list
     */
    public static List<Journal> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }
    
    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param value the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static Journal findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        return findRecord(where);
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null} 
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     * 
     * @param where the search criteria by which to find the desired record
     * 
     * @return the matching record if only one matches, the first matching 
     *         record if more than one record matches, {@code null} if no 
     *         records match
     */
    public static Journal findRecord(String where) {
        List<Journal> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     * 
     * @param where the search criteria on which to filter the records
     * 
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Journal> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the 
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it 
     * will have no effect on the ordering of the results and the records will 
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     * 
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be 
     *                ordered
     * 
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Journal> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }
    
    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to 
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     * 
     * @param id the primary key value of the record to be deleted
     * 
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(Long id) {
        if (id == null) {
            return false;
        }
        
        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setLong(1, id);
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Journal.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s", 
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return true;
    }
    
    private static List<Journal> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }
        
        List<Journal> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Journal record = new Journal();
                record.name = rs.getString(1);
                record.openDate = rs.getDate(2).toLocalDate();
                record.closeDate = (rs.getDate(3) != null
                        ? rs.getDate(3).toLocalDate() : null);
                record.account = rs.getString(4);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Journal.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }
    
    /**
     * Constructs a new, empty {@code Journal}. Once created, all fields are set
     * from the database, as this method is only used internally within this
     * class.
     */
    protected Journal() {
        
    }

    /**
     * Constructs a new, empty {@code Journal}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     * 
     * @param name the name of this {@code Journal}
     */
    public Journal (String name) {
        this.name = name;
    }

    /**
     * Retrieves the name of this {@code Journal}.
     * 
     * @return the journal's name
     */
    public String getJournalName() {
        return name;
    }

    /**
     * Sets the name of this {@code Journal}. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters and is the primary key in the table.
     * <p>
     * If the specified {@code name} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code name} is not unique in the table, an
     * {@code InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param name the journal's name
     * 
     * @throws NullPointerException if {@code name} is blank, empty, or 
     *                              {@code null}
     * @throws InvalidDataException if {@code name} is not unique in the table
     */
    public void setName(String name) {
        if (name == null || name.isBlank() || name.isEmpty()) {
            throw new NullPointerException("name is blank, empty, or null.");
        }
        name = verifyNameUniqueInTable(name);
        String old = getJournalName();
        this.name = name;
        firePropertyChange("journalName", old, getJournalName());
    }

    /**
     * Retrieves the date this {@code Journal} was opened.
     * 
     * @return the open date
     */
    public LocalDate getOpenDate() {
        return openDate;
    }

    /**
     * Sets the date this {@code Journal} was opened. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a date. The specified date may not be a
     * future date.
     * <p>
     * If the specified {@code openDate} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code openDate} is after the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param openDate the date the journal was opened
     * 
     * @throws NullPointerException if {@code openDate} is {@code null}
     * @throws InvalidDataException if {@code openDate} is in the future
     */
    public void setOpenDate(LocalDate openDate) {
        if (openDate == null) {
            throw new NullPointerException("openDate is null.");
        }
        if (!isOpenDateValid(openDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(df.format(openDate) + " is not "
                    + "prior to the current date (" + df.format(LocalDate.now())
                    + ").");
        }
        LocalDate old = getOpenDate();
        this.openDate = openDate;
        firePropertyChange("openDate", old, getOpenDate());
    }

    /**
     * Retrieves the close date of this {@code Journal}, or {@code null} if this
     * journal is still open.
     * 
     * @return the close date or {@code null}
     */
    public LocalDate getCloseDate() {
        return closeDate;
    }

    /**
     * Sets the close date of this {@code Journal}. This is an <em>optional</em>
     * field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a date. This field should be set at the
     * same time for all {@code Journal}s, which is at the time the &ldquo;books
     * are closed&rdquo; for an accounting period. If the {@code closeDate} is
     * provided (not {@code null}), it must be valid, meaning that it cannot be 
     * after the current date nor prior to the {@link #getOpenDate() open date}.
     * <p>
     * If the specified {@code closeDate} is after the current date, or prior to
     * the {@code openDate}, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound field.
     * 
     * @param closeDate the close date or {@code null}
     */
    public void setCloseDate(LocalDate closeDate) {
        if (closeDate != null && !isCloseDateValid(closeDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(df.format(closeDate) + " is not "
                    + "valid. It must be after the open date (" 
                    + df.format(getOpenDate()) + ", and before the current "
                    + "date (" + df.format(LocalDate.now()) + ").");
        }
        LocalDate old = getCloseDate();
        this.closeDate = closeDate;
        firePropertyChange("closeDate", old, getCloseDate());
    }

    /**
     * Retrieves the {@link Account} to which this {@code Journal} belongs.
     * 
     * @return the journal's account
     */
    public Account getAccount() {
        return Account.findByPrimaryKey(account);
    }

    /**
     * Sets the {@link Account} to which this {@code Journal} belongs. This is a
     * <strong>required</strong> field.
     * <p>
     * If the specified {@code account} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param account the journal's account
     * 
     * @throws NullPointerException if {@code account} is {@code null}
     */
    public void setAccount(Account account) {
        if (account == null) {
            throw new NullPointerException("account is null.");
        }
        Account old = getAccount();
        this.account = account.getAccountNumber();
        firePropertyChange("account", old, getAccount());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        hash += (openDate != null ? openDate.hashCode() : 0);
        hash += (closeDate != null ? closeDate.hashCode() : 0);
        hash += (getAccount() != null ? getAccount().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Journal)) {
            return false;
        }
        Journal other = (Journal) object;
        if ((this.name == null && other.name != null) ||
                (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        if ((this.openDate == null && other.openDate != null)
                || (this.openDate != null && !this.openDate.equals(other.openDate))) {
            return false;
        }
        if ((this.closeDate == null && other.closeDate != null)
                || (this.closeDate != null 
                && !this.closeDate.equals(other.closeDate))) {
            return false;
        }
        return ((this.getAccount() == null && other.getAccount() != null)
                || (this.getAccount() != null
                && !this.getAccount().equals(other.getAccount())));
    }

    @Override
    public String toString() {
        return context.getResourceMap(getClass()).getString("toString", 
                name, getAccount().toString());
    }
    
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Journal} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getName() name} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            verifyNameUniqueInTable(name);
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_OPENED + ", " + FLD_CLOSED + ",  " 
                + FLD_ACCT + ") VALUES(?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, name);
            pst.setDate(2, Date.valueOf(openDate));
            pst.setDate(3, (closeDate == null) 
                           ? null
                           : Date.valueOf(closeDate));
            pst.setString(4, account);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_OPENED + " = ?, " + FLD_CLOSED + " = ?, " 
                + FLD_ACCT + " = ? WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setDate(1, Date.valueOf(openDate));
            pst.setDate(2, (closeDate == null) 
                           ? null
                           : Date.valueOf(closeDate));
            pst.setString(3, account);
            pst.setString(4, name);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-yy");
        if (name == null || name.isBlank() || name.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "name"));
        }
        if (openDate == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "openDate"));
        } else if (!isOpenDateValid(openDate)) {
            throw new ValidationException(this,
                    resourceMap.getString("invalid.value", df.format(openDate),
                            "Open date must be prior to the current date ("
                            + df.format(LocalDate.now()) + ")."));
        }
        if (closeDate != null && !isCloseDateValid(closeDate)) {
            throw new ValidationException(this,
                    resourceMap.getString("invalid.value", df.format(closeDate),
                            "Close date must be prior to the current date ("
                            + df.format(LocalDate.now()) + "), and after the "
                            + "open date (" + df.format(openDate) + ")."));
        }
        if (account == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "account"));
        }
    }

    private String verifyNameUniqueInTable(String name) {
        int counter = 0;
        for (Journal record : getAllRecords()) {
            if (record.getJournalName().startsWith(name)) {
                counter++;
            }
        }
        if (counter == 0) {
            return name;
        } else if (counter < 10) {
            String append = " #0";
            name += append + counter;
        } else if (counter >= 10) {
            name += "#" + counter;
        }
        
        return name;
    }
    
    private boolean isOpenDateValid(LocalDate date) {
        return date.isAfter(LocalDate.now());
    }
    
    private boolean isCloseDateValid(LocalDate date) {
        return date.isBefore(getOpenDate()) || date.isAfter(LocalDate.now());
    }
    
    private String name = "[UNNAMED]";
    private LocalDate openDate = LocalDate.now();
    private LocalDate closeDate;
    private String account;

}
