/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-data-module
 *  Class      :   DatabaseException.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 17, 2024
 *  Modified   :   Oct 17, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 17, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data.support;

/**
 * An {@link Exception} sub-class that stores the record object that caused the
 * error.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class DatabaseException extends Exception {

    private static final long serialVersionUID = 1L;
    
    private final Object record;

    /**
     * Creates a new instance of <code>DatabaseException</code> without detail message.
     */
    public DatabaseException(Object record) {
        super();
        this.record = record;
    }


    /**
     * Constructs an instance of <code>DatabaseException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DatabaseException(Object record, String msg) {
        super(msg);
        this.record = record;
    }
    
    public Object getRecord() {
        return record;
    }
    
}
