/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Stop.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.ArrivalAuditDialog;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import java.io.Serializable;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code Stop} class defines a single record in the database table named
 * {@code STOPS}. Each record defines a single stop for a {@link Load} at which
 * freight is either picked up or delivered.
 * <p>
 * Each stop has an early and late arrival date/time, which defines the pick-up
 * or delivery window for the stop. If a customer sets exact appointments, then
 * both the early and late dates/times will be the same.
 * <p>
 * Whenever the user arrives at a stop, they must mark the arrival date/time. 
 * The same goes for when they depart a stop. 
 * <p>
 * When departing a stop, the user may select if the stop was a delivery. If it
 * was, then they will be prompted to enter the name of the person who signed
 * for the load. If it was not a delivery, then they are prompted for the bill
 * of lading (BOL) number to be stored in the load.
 * <p>
 * All properties in this class are bound. A good faith effort has been made to
 * make sure the documentation for each setter notes this fact.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Stop extends AbstractBean
        implements Serializable, NtosPropertyKeys {

    public static final String FLD_ID = "STOP_ID";
    public static final String FLD_LOAD = "ORDER_NUMBER";
    public static final String FLD_STOP = "STOP_NUMBER";
    public static final String FLD_CUSTOMER = "CUSTOMER";
    public static final String FLD_EARLY = "EARLY_ARRIVAL";
    public static final String FLD_LATE = "LATE_ARRIVAL";
    public static final String FLD_ARRIVAL = "ARRIVAL";
    public static final String FLD_DEPART = "DEPARTURE";
    public static final String FLD_DEL = "DELIVERY";
    public static final String FLD_NOTES = "STOP_NOTES";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Stop.class);

    private static final String TABLE_NAME = "STOPS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE "
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     *
     * @return a list of all records in the table or an empty list
     */
    public static List<Stop> getAllRecords() {
        return selectRecords(null, null);
    }

    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     *
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     *
     * @return an ordered list of all records, or an empty list
     */
    public static List<Stop> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }

    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then
     * {@code null} is returned.
     *
     * @param value the value to find
     *
     * @return the matching record, or {@code null} if no match
     */
    public static Stop findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        List<Stop> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null}
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     *
     * @param where the search criteria by which to find the desired record
     *
     * @return the matching record if only one matches, the first matching
     *         record if more than one record matches, {@code null} if no
     *         records match
     */
    public static Stop findRecord(String where) {
        List<Stop> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     *
     * @param where the search criteria on which to filter the records
     *
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Stop> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it
     * will have no effect on the ordering of the results and the records will
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     *
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be
     *                ordered
     *
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Stop> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }

    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param id the primary key value of the record to be deleted
     *
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }

        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setString(1, id);

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Stop.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s",
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return true;
    }

    private static List<Stop> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        List<Stop> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stop record = new Stop();
                record.stopId = rs.getLong(1);
                record.load = rs.getString(2);
                record.stopNumber = rs.getInt(3);
                record.customer = rs.getString(4);
                record.early = rs.getTimestamp(5).toLocalDateTime();
                record.late = rs.getTimestamp(6).toLocalDateTime();
                record.arrived = rs.getTimestamp(7).toLocalDateTime();
                record.departed = rs.getTimestamp(8).toLocalDateTime();
                record.delivery = rs.getBoolean(9);
                record.notes = rs.getString(10);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Stop.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return records;
    }

    /**
     * Constructs a new, empty {@code Stop}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     */
    public Stop() {

    }

    /**
     * Retrieves the unique identifier for this {@code Stop}.
     *
     * @return the unique identifier
     */
    public Long getStopId() {
        return stopId;
    }

    /**
     * Sets the unique identifier for this {@code Stop}. This is a <strong>
     * required</strong> field and is the primary key in the table.
     * <p>
     * The data type of this field is a long and is auto-generated by the
     * database. Therefore, this field should only be set via a select statement
     * and this is why this method is set to {@code protected}.
     * <p>
     * This is a bound property.
     *
     * @param stopId the unique identifier
     */
    protected void setStopId(Long stopId) {
        long old = getStopId();
        this.stopId = stopId;
        firePropertyChange("stopId", old, getStopId());
    }

    /**
     * Retrieves the {@code Stop} number, which is the order in which this stop
     * is made on the {@link Load}.
     *
     * @return the stop number
     */
    public int getStopNumber() {
        return stopNumber;
    }

    /**
     * Sets the {@code Stop} number, which defines the order in which this stop
     * is made on the {@code Load}. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is an integer and is set by simply calling
     * this method.
     * <p>
     * This is a bound property.
     */
    public void setStopNumber() {
        int old = getStopNumber();

        stopNumber = Stop.findRecords(FLD_LOAD + " = "
                + getLoad().getOrderNumber()).size() + 1;

        firePropertyChange("stopNumber", old, getStopNumber());
    }

    /**
     * Retrieves the early arrival date and time at this {@code Stop}.
     *
     * @return the early arrival date and time
     */
    public LocalDateTime getEarlyArrival() {
        return early;
    }

    /**
     * Sets the early arrival date and time for this {@code Stop}. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a timestamp.
     * <p>
     * If the specified {@code dateAndTime} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code dateAndTime} is prior to the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param dateAndTime the early arrival date and time
     *
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setEarlyArrival(LocalDateTime dateAndTime) {
        LocalDateTime now = LocalDateTime.now();
        if (dateAndTime == null) {
            throw new NullPointerException("Early dateAndTime is null.");
        } else if (!isValidDateAndTime(dateAndTime, now)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern(
                    "yyyy-mm-dd HH:mm");
            throw new InvalidDataException(df.format(dateAndTime) + " is not "
                    + "valid. It must be on or after the current date ("
                    + df.format(now) + ").");
        }
        LocalDateTime old = getEarlyArrival();
        this.early = dateAndTime;
        firePropertyChange("earlyArrival", old, getEarlyArrival());
    }

    /**
     * Retrieves the late arrival date and time at this {@code Stop}.
     *
     * @return the late arrival date and time
     */
    public LocalDateTime getLateArrival() {
        return late;
    }

    /**
     * Sets the late arrival date and time for this {@code Stop}. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a timestamp.
     * <p>
     * If the specified {@code dateAndTime} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code dateAndTime} is prior to the early date and time,
     * an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param dateAndTime the late arrival date and time
     *
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setLateArrival(LocalDateTime dateAndTime) {
        if (dateAndTime == null) {
            throw new NullPointerException("Late dateAndTime is null");
        } else if (!isValidDateAndTime(dateAndTime, early)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern(
                    "yyyy-mm-dd HH:mm");
            throw new InvalidDataException(df.format(dateAndTime) + " is not "
                    + "valid. It must be on or after the early date ("
                    + df.format(early) + ").");
        }
        LocalDateTime old = getLateArrival();
        this.late = dateAndTime;
        firePropertyChange("lateArrival", old, getLateArrival());
    }

    /**
     * Retrieves the date and time arrived at this {@code Stop}.
     *
     * @return the arrival date and time
     */
    public LocalDateTime getArrived() {
        return arrived;
    }

    /**
     * Sets the date and time arrived at this {@code Stop}. This field is <em>
     * optional</em> immediately, but required later.
     * <p>
     * The date type for this field is a timestamp.
     * <p>
     * When setting this field, if the specified {@code arrived} is
     * {@code null}, a {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code arrived} is before the
     * {@link #getEarlyArrival() early time} or after the
     * {@link #getLateArrival() late time}, a notification is made in the system
     * so that early and late arrivals can be tracked. This notification is made
     * in a &ldquo;hidden&rdquo; table called {@code ARRIVAL_AUDITS}, which has
     * no access from within the application. This table may only be accessed
     * for reading via the command line switch
     * &ldquo;{@code --show-arrivals}&rdquo;.
     * <p>
     * If the specified {@code arrived} is after the current date/time, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param arrived the arrival date/time
     */
    public void setArrived(LocalDateTime arrived) {
        if (arrived == null) {
            throw new NullPointerException("arrived is null.");
        } else if (isValidArrivalDateAndTime(arrived)) {
            if (isArrivedTooEarly(arrived)) {
                logEarlyArrival(arrived);
            } else if (isArrivedTooLate(arrived)) {
                logLateArrival(arrived);
            }
        }
        LocalDateTime now = LocalDateTime.now();
        if (!isValidDateAndTime(arrived, now)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern(
                    "yyyy-mm-dd HH:mm");
            throw new InvalidDataException(df.format(arrived) + " cannot be "
                    + "after the current date/time (" + df.format(now) + ").");
        }
        LocalDateTime old = getArrived();
        this.arrived = arrived;
        firePropertyChange("arrived", old, getArrived());
    }

    /**
     * Retrieves the date and time of departure from this {@code Stop}.
     *
     * @return the departure date/time
     */
    public LocalDateTime getDeparted() {
        return departed;
    }

    /**
     * Sets the date and time of departure from this {@code Stop}. This field is
     * <em>optional</em> immediately, but required later.
     * <p>
     * The data type of this field is a timestamp.
     * <p>
     * When setting this field, if the specified {@code departed} is
     * {@code null}, a {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code departed} is before the
     * {@link #getArrived() arrival time}, or after the current date/time, an
     * {@link InvalidDataException} is thrown
     * <p>
     * This is a bound property.
     *
     * @param departed the departure date/time
     * 
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setDeparted(LocalDateTime departed) {
        LocalDateTime now = LocalDateTime.now();
        if (departed == null) {
            throw new NullPointerException("departed is null.");
        } else if (!isValidDepartureDateTime(departed, now)) {
            throw new InvalidDataException(departed + " must be after the "
                    + "arrival date/time (" + arrived + ") and before or at "
                    + "the current date/time (" + now + ").");
        }
        LocalDateTime old = getDeparted();
        this.departed = departed;
        firePropertyChange("departed", old, getDeparted());
    }

    /**
     * Determines whether this {@code Stop} is a delivery or a pick-up.
     * 
     * @return {@code true} if a delivery; {@code false} if a pick-up
     */
    public boolean getDelivery() {
        return delivery;
    }

    /**
     * Sets whether this {@code Stop} is a delivery or a pick-up. This field is
     * <strong>required</strong>.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}.
     * <p>
     * This is a bound property.
     * 
     * @param delivery {@code true} if a delivery; {@code false} if a pick-up
     */
    public void setDelivery(boolean delivery) {
        boolean old = getDelivery();
        this.delivery = delivery;
        firePropertyChange("delivery", old, getDelivery());
    }

    /**
     * Retrieves any notes about this {@code Stop}.
     * 
     * @return stop notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets any notes about this {@code Stop}. This is an <em>optional</em>
     * field.
     * <p>
     * The data type of this field is a string, with a maximum length which is
     * determined by the system on which the application is running, but is
     * typically measured in gigabytes.
     * <p>
     * This field is typically unused in the most common use-case. However it is
     * available in case their are special instructions for this particular stop
     * due to weather conditions or construction. Stop notes should not be used
     * for storing information regarding the customer itself, as those should be
     * stored in the
     * {@link Customer#setNotes(java.lang.String) customer's notes property}.
     * The typical notes stored in a {@code Stop} record are one-off pieces of
     * information that may only apply for a single load.
     * <p>
     * This is a bound property.
     * 
     * @param notes any notes about this stop
     */
    public void setNotes(String notes) {
        String old = getNotes();
        this.notes = notes;
        firePropertyChange("stopNotes", old, getNotes());
    }

    /**
     * Retrieves the {@link Customer} for this {@code Stop}.
     * 
     * @return the customer
     */
    public Customer getCustomer() {
        return Customer.findByPrimaryKey(customer);
    }

    /**
     * Sets the {@link Customer} for this {@code Stop}. This field is <strong>
     * required</strong>.
     * <p>
     * If the specified {@code customer} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param customer the customer
     * 
     * @throws NullPointerException
     */
    public void setCustomer(Customer customer) {
        if (customer == null) {
            throw new NullPointerException("customer is null.");
        }
        Customer old = getCustomer();
        this.customer = customer.getCustomerId();
        firePropertyChange("customer", old, getCustomer());
    }

    /**
     * Retrieves the {@link Load} to which this {@code Stop} belongs.
     * 
     * @return the load
     */
    public Load getLoad() {
        return Load.findByPrimaryKey(load);
    }

    /**
     * Sets the {@link Load} to which this {@code Stop} belongs. This field is
     * <strong>required</strong>.
     * <p>
     * If the specified {@code load} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param load the load
     * 
     * @throws NullPointerException
     */
    public void setLoad(Load load) {
        if (load == null) {
            throw new NullPointerException("load is null.");
        }
        Load old = getLoad();
        this.load = load.getOrderNumber();
        firePropertyChange("load", old, getLoad());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (stopId != null ? stopId.hashCode() : 0);
        hash += stopNumber;
        hash += (early != null ? early.hashCode() : 0);
        hash += (late != null ? late.hashCode() : 0);
        hash += (arrived != null ? arrived.hashCode() : 0);
        hash += (departed != null ? departed.hashCode() : 0);
        hash += (notes != null ? notes.hashCode() : 0);
        hash += (delivery ? 1 : 0);
        hash += (load != null ? load.hashCode() : 0);
        hash += (customer != null ? customer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Stop)) {
            return false;
        }
        Stop other = (Stop) object;
        if ((this.stopId == null && other.stopId != null)
                || (this.stopId != null && !this.stopId.equals(other.stopId))) {
            return false;
        }
        if ((this.load == null && other.load != null)
                || this.load != null && !this.load.equals(other.load)) {
            return false;
        }
        if ((this.customer == null && other.customer != null)
                || this.customer != null && !this.customer.equals(other.customer)) {
            return false;
        }
        if ((this.stopNumber != other.stopNumber)) {
            return false;
        }
        if ((this.early == null && other.early != null)
                || (this.early != null && !this.early.equals(other.early))) {
            return false;
        }
        if ((this.late == null && other.late != null)
                || (this.late != null && !this.late.equals(other.late))) {
            return false;
        }
        if ((this.arrived == null && other.arrived != null)
                || (this.arrived != null && !this.arrived.equals(other.arrived))) {
            return false;
        }
        if ((this.departed == null && other.departed != null)
                || (this.departed != null
                && !this.departed.equals(other.departed))) {
            return false;
        }
        if ((this.notes == null && other.notes != null)
                || (this.notes != null && !this.notes.equals(other.notes))) {
            return false;
        }
        return this.delivery == other.delivery;
    }

    @Override
    public String toString() {
        ResourceMap rm = context.getResourceMap(getClass());
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        String value = rm.getString("toString", load, stopNumber,
                df.format(early), df.format(late));
        if (arrived != null) {
            value = rm.getString("toString.arrived", load, stopNumber,
                    df.format(arrived));
        }
        if (departed != null) {
            value = rm.getString("toString.departed", load, stopNumber,
                    df.format(arrived), df.format(departed));
        }
        return value;
    }
    
    /**
     * Saves this {@code Stop} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_LOAD + ", " + FLD_STOP + ",  " + FLD_CUSTOMER + ", "
                + FLD_EARLY + ", " + FLD_LATE + ", " + FLD_ARRIVAL + ", "
                + FLD_DEPART + ", " + FLD_DEL + ", " + FLD_NOTES
                + ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, load);
            pst.setInt(2, stopNumber);
            pst.setString(3, customer);
            pst.setTimestamp(4, Timestamp.valueOf(early));
            pst.setTimestamp(5, Timestamp.valueOf(late));
            if (arrived != null) {
                pst.setTimestamp(6, Timestamp.valueOf(arrived));
            } else {
                pst.setTimestamp(6, null);
            }
            if (departed != null) {
                pst.setTimestamp(7, Timestamp.valueOf(departed));
            } else {
                pst.setTimestamp(7, null);
            }
            pst.setBoolean(8, delivery);
            pst.setString(9, notes);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Stop.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_LOAD + " = ?, " + FLD_STOP + ",  " + FLD_CUSTOMER + " = ?, "
                + FLD_EARLY + " = ?, " + FLD_LATE + " = ?, " 
                + FLD_ARRIVAL + " = ?, " + FLD_DEPART + " = ?, " 
                + FLD_DEL + " = ?, " + FLD_NOTES 
                + " = ? WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, load);
            pst.setInt(2, stopNumber);
            pst.setString(3, customer);
            pst.setTimestamp(4, Timestamp.valueOf(early));
            pst.setTimestamp(5, Timestamp.valueOf(late));
            if (arrived != null) {
                pst.setTimestamp(6, Timestamp.valueOf(arrived));
            } else {
                pst.setTimestamp(6, null);
            }
            if (departed != null) {
                pst.setTimestamp(7, Timestamp.valueOf(departed));
            } else {
                pst.setTimestamp(7, null);
            }
            pst.setBoolean(8, delivery);
            pst.setString(9, notes);
            pst.setLong(10, stopId);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Stop.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        ResourceMap rm = context.getResourceMap(getClass());
        LocalDateTime now = LocalDateTime.now();
        if (load == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "load"));
        }
        if (stopNumber <= 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", stopNumber,
                            rm.getString("invalid.stopNumber.reason")));
        }
        if (customer == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "customer"));
        }
        if (early == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "early"));
        } else if (!isValidDateAndTime(early, now)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", early,
                            rm.getString("invalid.early.reason")));
        }
        if (late == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "late"));
        } else if (!isValidDateAndTime(late, early)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", late,
                            rm.getString("invalid.late.reason")));
        }
        if (arrived != null) {
            if (!arrived.isBefore(now) || !arrived.equals(now)) {
                throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", arrived,
                            rm.getString("invalid.arrived.reason")));
            }
        }
        if (departed != null) {
            if (!isValidDepartureDateTime(departed, now)) {
                throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", departed,
                            rm.getString("invalid.departure.reason")));
            }
        }
    }

    private boolean isValidDateAndTime(LocalDateTime dateAndTime,
            LocalDateTime compareTo) {
        return dateAndTime != null
                && (dateAndTime.isAfter(compareTo)
                || dateAndTime.equals(compareTo));
    }

    private boolean isValidArrivalDateAndTime(LocalDateTime arrival) {
        return !isArrivedTooEarly(arrival) && !isArrivedTooLate(arrival);
    }

    private boolean isArrivedTooEarly(LocalDateTime arrival) {
        if (arrival.isBefore(early)) {
            logEarlyArrival(arrival);
            return true;
        }
        return false;
    }

    private boolean isArrivedTooLate(LocalDateTime arrival) {
        if (arrival.isAfter(late)) {
            logLateArrival(arrival);
            return true;
        }
        return false;
    }

    private boolean isValidDepartureDateTime(LocalDateTime departed,
            LocalDateTime comparedTo) {
        return (departed.isAfter(arrived)
                && (departed.isBefore(comparedTo)
                || departed.equals(comparedTo)));
    }

    /**
     * Enters a record into the {@code ARRIVAL_AUDITS} table in the database to
     * track each time the user arrives at a stop earlier than the early
     * date/time.
     * <p>
     * Arriving early at a stop is not a problem, since the customers can
     * sometimes get the trailer unloaded ahead of schedule, thereby getting the
     * driver ahead of schedule and reducing their stress levels.
     * <p>
     * Because of this, an simple entry is made into the audits table to show
     * that the user arrived early, and it has a default comment of
     * &ldquo;Arrived early...no problems.&rdquo;.
     *
     * @param arrival the arrival date/time
     */
    private void logEarlyArrival(LocalDateTime arrival) {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.prepareStatement(auditStmt);

            stmt.setString(1, load);
            stmt.setLong(2, stopId);
            stmt.setTimestamp(3, Timestamp.valueOf(early));
            stmt.setTimestamp(4, Timestamp.valueOf(late));
            stmt.setTimestamp(5, Timestamp.valueOf(arrival));
            stmt.setString(6, "Arrived early...no problems.");
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), auditStmt);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    /**
     * Enters a record into the {@code ARRIVAL_AUDITS} table in the database to
     * track each time the user arrives at a stop later than the late date/time.
     * <p>
     * If the user consistently arrives too late at a stop, it lowers the
     * company's reputation with that customer, which could have a negative
     * effect on the company's revenue. Customers are the life-blood of any
     * company, and therefore should be provided quality service.
     * <p>
     * If a driver is arriving after the late date/time for customers, there is
     * a problem that needs to be solved. Late arrivals cause unnecessary
     * delays for the driver, if the late arrival was due to the driver's own
     * actions. There are times, however, when things out of the driver's
     * control happen that cause the driver to be late.
     * <p>
     * Because of the reason just stated, on an arrival beyond the late
     * date/time for the stop, a dialog window is displayed that requires the
     * user to enter an explanation for why they were late to the stop. This
     * explanation is recorded in the {@code ARRIVAL_AUDITS} table, which can be
     * checked later by the driver's employer, or the company to which the
     * driver is leased. The dialog window <em>cannot be dismissed</em> until
     * the user has entered text of the company's minimum length policy.
     *
     * @param arrival arrival date/time
     */
    private void logLateArrival(LocalDateTime arrival) {
        Connection conn = null;
        PreparedStatement stmt = null;

        ArrivalAuditDialog dlg = new ArrivalAuditDialog(context, notes);
        dlg.pack();
        dlg.setVisible(true);

        String reason = dlg.getReason();

        dlg.dispose();

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.prepareStatement(auditStmt);

            stmt.setString(1, load);
            stmt.setLong(2, stopId);
            stmt.setTimestamp(3, Timestamp.valueOf(early));
            stmt.setTimestamp(4, Timestamp.valueOf(late));
            stmt.setTimestamp(5, Timestamp.valueOf(arrival));
            stmt.setString(6, reason);
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), auditStmt);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    /**
     * SQL {@code INSERT} statement for inserting information regarding being
     * earlier than the early date/time (which is not a problem) and being later
     * than the late date/time (which is a problem) into the
     * {@code ARRIVAL_AUDITS} table in the database.
     * <p>
     * The table is &ldquo;hidden&rdquo;, in that there is no action within the
     * UI to allow the user to view it. However, we will need to allow for a
     * command line switch for entering the auditing system for the employers.
     */
    private final String auditStmt = "INSERT INTO " + SCHEMA + ".ARRIVAL_AUDITS"
            + "(ORDER_NUMBER, STOP_ID, EARLY, LATE, ARRIVAL, REASON) VALUES("
            + "?, ?, ?, ?, ?, ?)";

    private Long stopId;
    private int stopNumber;
    private LocalDateTime early;
    private LocalDateTime late;
    private LocalDateTime arrived;
    private LocalDateTime departed;
    private boolean delivery;
    private String notes;
    private String customer;
    private String load;

}
