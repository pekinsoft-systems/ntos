/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   ServiceRecord.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import java.io.Serializable;
import java.sql.*;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code ServiceRecord} class defines a single record from the database
 * table named {@code SERVICE_RECORDS}. Each record in this table defines the
 * details of a single service performed on a {@link Unit}.
 * <p>
 * Services may be performed by the Owner/Operator or by a vendor. In some
 * situations, as the company grows, the service may be performed by an
 * {@link Employee} who works in the company's shop.
 * <p>
 * All pertinent information is recorded in a service record, including the
 * {@code Unit} serviced, the {@code ServiceType type of service}, the date of
 * the service, the odometer reading of the unit, the complaint (reason) for the
 * service (especially useful for repairs), the estimate given, the work
 * performed (in detail, usually), the completion date, the total cost, the
 * {@link Customer vendor} who performed the work (useful for warranties), and
 * the account into which the expense is recorded.
 * <p>
 * By maintaining these {@code ServiceRecord}s, the company can track the health
 * of their units, which will give them a better idea of:
 * <ul>
 * <li>how their employees (drivers) are treating the units</li>
 * <li>the reason for their maintenance and repair expense being high or
 * low</li>
 * <li>when it is time to trade-in for new units</li>
 * </ul>
 * <p>
 * All properties of this class are bound. A good faith effort has been made to
 * document this on each of the setter methods.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class ServiceRecord extends AbstractBean
        implements Serializable, NtosPropertyKeys {

    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} primary key identity field.
     */
    public static final String FLD_ID = "RECORD_ID";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} date of the service field.
     */
    public static final String FLD_SVC_DATE = "SERVICE_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} complaint field.
     */
    public static final String FLD_COMPLAINT = "COMPLAINT";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} estimate field.
     */
    public static final String FLD_EST = "ESTIMATED_COST";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} work performed field.
     */
    public static final String FLD_WRK_PERFD = "WORK_PERFORMED";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} completion date field.
     */
    public static final String FLD_COMP_DATE = "COMPLETED_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} total cost field.
     */
    public static final String FLD_TOTAL = "TOTAL_COST";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} {@link Customer vendor} field.
     */
    public static final String FLD_VENDOR = "VENDOR";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} {@link ServiceType type of service} field.
     */
    public static final String FLD_TYPE = "SERVICE_TYPE";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} {@link Unit vehicle serviced} field.
     */
    public static final String FLD_UNIT = "UNIT";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} {@link Account account} paid from field.
     */
    public static final String FLD_ACCT = "ACCOUNT";
    /**
     * Constant representing the table field name in the database for the
     * {@code ServiceRecord} odometer reading field.
     */
    public static final String FLD_ODO = "ODOMETER";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(ServiceRecord.class);

    private static final String TABLE_NAME = "SERVICE_RECORDS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE "
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     *
     * @return a list of all records in the table or an empty list
     */
    public static List<ServiceRecord> getAllRecords() {
        return selectRecords(null, null);
    }

    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     *
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     *
     * @return an ordered list of all records, or an empty list
     */
    public static List<ServiceRecord> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }

    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then
     * {@code null} is returned.
     *
     * @param value the value to find
     *
     * @return the matching record, or {@code null} if no match
     */
    public static ServiceRecord findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        List<ServiceRecord> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null}
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     *
     * @param where the search criteria by which to find the desired record
     *
     * @return the matching record if only one matches, the first matching
     *         record if more than one record matches, {@code null} if no
     *         records match
     */
    public static ServiceRecord findRecord(String where) {
        List<ServiceRecord> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     *
     * @param where the search criteria on which to filter the records
     *
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<ServiceRecord> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it
     * will have no effect on the ordering of the results and the records will
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     *
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be
     *                ordered
     *
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<ServiceRecord> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }

    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param id the primary key value of the record to be deleted
     *
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }

        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setString(1, id);

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", ServiceRecord.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s",
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return true;
    }

    private static List<ServiceRecord> selectRecords(String where,
            String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        List<ServiceRecord> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ServiceRecord record = new ServiceRecord();

                record.recordId = rs.getLong(1);
                record.serviceType = rs.getLong(2);
                record.serviceDate = rs.getDate(3).toLocalDate();
                record.complaint = rs.getString(4);
                record.estimate = rs.getDouble(5);
                record.workPerformed = rs.getString(6);
                if (rs.getDate(7) != null) {
                    record.completedDate = rs.getDate(7).toLocalDate();
                } else {
                    record.completedDate = null;
                }
                record.totalPrice = rs.getDouble(8);
                record.vendor = rs.getString(9);
                record.unit = rs.getString(10);
                record.account = rs.getString(11);
                record.odometer = rs.getInt(12);

                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", ServiceRecord.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return records;
    }

    /**
     * Constructs a new, empty {@code ServiceRecord}. Once created, all fields
     * are set
     * to {@code null} and need to be set manually.
     */
    public ServiceRecord() {

    }

    /**
     * Retrieves the unique identifier for this {@code ServiceRecord}.
     *
     * @return the unique identifier
     */
    public Long getRecordId() {
        return recordId;
    }

    /**
     * Sets the unique identifier for this {@code ServiceRecord}. This is a
     * <strong>required</strong> value and is the primary key in the table.
     * <p>
     * The data type of this field is a long, and it is auto-generated by the
     * database. This method is only called when a record is read in from the
     * table, so is protected because all read operations take place within this
     * class.
     * <p>
     * When set, this value cannot be {@code null}, and it is checked. If it is,
     * a {@link NullPointerException} is thrown.
     * <p>
     * When set, this value must be greater than zero and is checked. If it is
     * not, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param recordId the unique identifier
     *
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    protected void setRecordId(Long recordId) {
        if (recordId == null) {
            throw new NullPointerException("recordId is null.");
        } else if (recordId <= 0) {
            throw new InvalidDataException(recordId + " is not greater than "
                    + "zero.");
        }
        long old = getRecordId();
        this.recordId = recordId;
        firePropertyChange("recordId", old, getRecordId());
    }

    /**
     * Retrieves the service date for this {@code ServiceRecord}.
     *
     * @return the service date
     */
    public LocalDate getServiceDate() {
        return serviceDate;
    }

    /**
     * Sets the service date for this {@code ServiceRecord}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a date. This value is the date on which
     * the {@link Unit} goes in for a service.
     * <p>
     * If the specified {@code serviceDate} is {@code null}, it will be set to
     * the current date.
     * <p>
     * If the specified {@code serviceDate} is after the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param serviceDate the date service is performed
     *
     * @throws InvalidDataException
     */
    public void setServiceDate(LocalDate serviceDate) {
        if (serviceDate == null) {
            serviceDate = LocalDate.now();
        } else if (!isValidDate(serviceDate)) {
            throw new InvalidDataException(
                    serviceDate + " is after the current "
                    + "date (" + LocalDate.now() + ").");
        }
        LocalDate old = getServiceDate();
        this.serviceDate = serviceDate;
        firePropertyChange("serviceDate", old, getServiceDate());
    }

    /**
     * Retrieves the complaint for which the {@link Unit} needed service.
     *
     * @return the complaint
     */
    public String getComplaint() {
        return complaint;
    }

    /**
     * Sets the complaint for which the {@link Unit} needs service. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length determined
     * by the system on which the application is running, but is typically
     * measured in gigabytes. This field allows for free-form text.
     * <p>
     * If the specified {@code complaint} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown
     * <p>
     * This is a bound property.
     *
     * @param complaint the complaint causing service to be performed
     *
     * @throws NullPointerException
     */
    public void setComplaint(String complaint) {
        if (complaint == null || complaint.isBlank() || complaint.isEmpty()) {
            throw new NullPointerException("complaint is blank, empty, or null.");
        }
        String old = getComplaint();
        this.complaint = complaint;
        firePropertyChange("complaint", old, getComplaint());
    }

    /**
     * Retrieves the price estimate for the service to be performed.
     *
     * @return the price estimate
     */
    public double getEstimatedPrice() {
        return estimate;
    }

    /**
     * Sets the price estimate for the service to be performed. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a double, with a valid range of
     * {@code -9,999,999,999,999.99} through {@code 9,999,999,999,999.99}. The
     * default value for this field is {@code -0.01}. It is provided because
     * not all service providers give an estimate for all services.
     * <p>
     * This is a bound property.
     *
     * @param estimatedPrice
     */
    public void setEstimatedPrice(double estimatedPrice) {
        if (estimatedPrice <= 0.00d) {
            estimatedPrice = -0.01d;
        }
        double old = getEstimatedPrice();
        this.estimate = estimatedPrice;
        firePropertyChange("estimatedPrice", old, getEstimatedPrice());
    }

    /**
     * Retrieves the work performed during this service.
     *
     * @return the work performed
     */
    public String getWorkPerformed() {
        return workPerformed;
    }

    /**
     * Sets the work performed during this service. This is an <em>optional</em>
     * field.
     * <p>
     * The data type of this field is a string, with a maximum length determined
     * by the system on which the application is running, but is typically
     * measured in gigabytes. This field allows for free-form text.
     * <p>
     * This field is required once the service has been completed. Therefore, if
     * the {@link #getCompletedDate() completed date} property is set, and the
     * specified {@code workPerformed} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param workPerformed the work performed
     *
     * @throws NullPointerException
     */
    public void setWorkPerformed(String workPerformed) {
        if (getCompletedDate() != null && (workPerformed == null
                || workPerformed.isBlank() || workPerformed.isEmpty())) {
            throw new NullPointerException("The service is completed and "
                    + "workPerformed is blank, empty, or null.");
        }
        String old = getWorkPerformed();
        this.workPerformed = workPerformed;
        firePropertyChange("workPerformed", old, getWorkPerformed());
    }

    /**
     * Retrieves the date service was completed.
     *
     * @return the date completed
     */
    public LocalDate getCompletedDate() {
        return completedDate;
    }

    /**
     * Sets the date service was completed. This is an <em>optional</em> field.
     * <p>
     * The data type for this field is a date. It allows for {@code null} values
     * until such time that service has been performed and the
     * {@code ServiceRecord} is ready to be closed.
     * <p>
     * When being provided, the {@code completedDate} must be valid. This means
     * that it must fall on or after the {@link #getServiceDate() service date}
     * and must be before or on the current date. If the specified date does not
     * meet these requirements, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param completedDate the date completed
     *
     * @throws InvalidDataException
     */
    public void setCompletedDate(LocalDate completedDate) {
        if (!isValidClosingDate(completedDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(df.format(completedDate) + " is not "
                    + "valid. It cannot be prior to the service date ("
                    + df.format(serviceDate) + ") and it cannot be after the "
                    + "current date (" + df.format(LocalDate.now()) + ").");
        }
        LocalDate old = getCompletedDate();
        this.completedDate = completedDate;
        firePropertyChange("completeDate", old, getCompletedDate());
    }

    /**
     * Retrieves the total price of the service.
     *
     * @return the total price
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the total price of the service. This is an <em>optional</em> field.
     * <p>
     * The data type of this field is a double, with a valid range of
     * {@code -9,999,999,999,999.99} through {@code 9,999,999,999,999.99}. The
     * default value for this field is {@code -0.01}. It is provided because
     * this price will not be known for sure until the service has been
     * completed.
     * <p>
     * While this value is optional, it becomes <strong>required</strong> once
     * the {@code ServiceRecord} is completed. When setting this value, it is
     * checked for validity, meaning that it cannot be less than zero. We allow
     * for a zero amount due to loyalty points offered by truck stops and shops,
     * which could, in theory, cover the entire cost of a service. If the value
     * is less than zero, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound field.
     *
     * @param totalPrice the total price
     *
     * @throws InvalidDataException
     */
    public void setTotalPrice(double totalPrice) {
        if (getCompletedDate() == null) {
            totalPrice = -0.01d;
        } else if (totalPrice < 0.00d) {
            throw new InvalidDataException(
                    "totalPrice cannot be less than zero.");
        }
        double old = getTotalPrice();
        this.totalPrice = totalPrice;
        firePropertyChange("totalPrice", old, getTotalPrice());
    }

    /**
     * Retrieves the {@link Account} from which this invoice was paid.
     *
     * @return the account from which invoice was paid
     */
    public Account getAccount() {
        return Account.findByPrimaryKey(account);
    }

    /**
     * Sets the {@link Account} from which this invoice is paid. This is a
     * <strong>required</strong> field.
     * <p>
     * While each {@link ServiceType type of service} has a default account set
     * up, it is not required to pay out of that account. If the specified value
     * is {@code null}, the default account will be used.
     * <p>
     * This is a bound field.
     *
     * @param account the account from which to pay
     */
    public void setAccount(Account account) {
        if (account == null) {
            account = getServiceType().getDefaultAccount();
        }
        Account old = getAccount();
        this.account = account.getAccountNumber();
        firePropertyChange("defaultAccount", old, getAccount());
    }

    /**
     * Retrieves the {@link Customer vendor} that performed the service.
     *
     * @return the vendor
     */
    public Customer getVendor() {
        return Customer.findByPrimaryKey(vendor);
    }

    /**
     * Sets the {@link Customer vendor} that is performing the service. This is
     * a <strong>required</strong> field.
     * <p>
     * If the specified {@code vendor} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code vendor} is not the {@link CustomerType} of a
     * vendor, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param vendor the vendor
     *
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setVendor(Customer vendor) {
        if (vendor == null) {
            throw new NullPointerException("vendor is null.");
        } // TODO: Figure out how to check if vendor has the correct CustomerType.
        Customer old = getVendor();
        this.vendor = vendor.getCustomerId();
        firePropertyChange("vendor", old, getVendor());
    }

    /**
     * Retrieves the {@link ServiceType} for this {@code ServiceRecord}.
     *
     * @return the service type
     */
    public ServiceType getServiceType() {
        return ServiceType.findByPrimaryKey(serviceType);
    }

    /**
     * Sets the {@link ServiceType} for this {@code ServiceRecord}. This is a
     * <strong>required</strong> field.
     * <p>
     * The service type defines the characteristics of the service being
     * performed. Certain parameters are tracked based upon the type of the
     * service, and also allows for creating specific, customized reports.
     * <p>
     * If the specified {@code serviceType} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param serviceType the service type
     *
     * @throws NullPointerException
     */
    public void setServiceType(ServiceType serviceType) {
        if (serviceType == null) {
            throw new NullPointerException("serviceType is null.");
        }
        ServiceType old = getServiceType();
        this.serviceType = serviceType.getTypeId();
        firePropertyChange("serviceType", old, getServiceType());
    }

    /**
     * Retrieves the {@link Unit} on which the service was performed.
     *
     * @return the serviced unit
     */
    public Unit getUnit() {
        return Unit.findByPrimaryKey(unit);
    }

    /**
     * Sets the {@link Unit} on which the service was performed. This is a
     * <strong>required</strong> field.
     * <p>
     * By tracking the unit serviced, various parameters may be updated so that
     * the user can be notified when future services are coming due. By allowing
     * the system to help the user in this way, there is less of a likelihood
     * that major problems will arise with the equipment.
     * <p>
     * If the specified {@code unit} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param unit the serviced unit
     *
     * @throws NullPointerException
     */
    public void setUnit(Unit unit) {
        if (unit == null) {
            throw new NullPointerException("unit is null.");
        }
        Unit old = getUnit();
        this.unit = unit.getUnitNumber();
        firePropertyChange("unit", old, getUnit());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        hash += (serviceType != null ? serviceType.hashCode() : 0);
        hash += (serviceDate != null ? serviceDate.hashCode() : 0);
        hash += (complaint != null ? complaint.hashCode() : 0);
        hash += (estimate != null ? estimate.hashCode() : 0);
        hash += (workPerformed != null ? workPerformed.hashCode() : 0);
        hash += (completedDate != null ? completedDate.hashCode() : 0);
        hash += (totalPrice != null ? totalPrice.hashCode() : 0);
        hash += (getVendor() != null ? getVendor().hashCode() : 0);
        hash += (getUnit() != null ? getUnit().hashCode() : 0);
        hash += (getAccount() != null ? getAccount().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServiceRecord)) {
            return false;
        }
        ServiceRecord other = (ServiceRecord) object;
        if ((this.recordId == null && other.recordId != null)
                || (this.recordId != null && !this.recordId.equals(
                        other.recordId))) {
            return false;
        }
        if ((this.getServiceType() == null && other.getServiceType() != null)
                || (this.getServiceType() != null
                && !this.getServiceType().equals(other.getServiceType()))) {
            return false;
        }
        if ((this.serviceDate == null && other.serviceDate != null)
                || (this.serviceDate != null
                && !this.serviceDate.equals(other.serviceDate))) {
            return false;
        }
        if ((this.complaint == null && other.complaint != null)
                || (this.complaint != null
                && !this.complaint.equals(other.complaint))) {
            return false;
        }
        if ((this.estimate == null && other.estimate != null)
                || (this.estimate != null
                && !this.estimate.equals(other.estimate))) {
            return false;
        }
        if ((this.workPerformed == null && other.workPerformed != null)
                || (this.workPerformed != null
                && !this.workPerformed.equals(other.workPerformed))) {
            return false;
        }
        if ((this.completedDate == null && other.completedDate != null)
                || (this.completedDate != null
                && !this.completedDate.equals(other.completedDate))) {
            return false;
        }
        if ((this.totalPrice == null && other.totalPrice != null)
                || (this.totalPrice != null
                && !this.totalPrice.equals(other.totalPrice))) {
            return false;
        }
        if ((this.getVendor() == null && other.getVendor() != null)
                || (this.getVendor() != null
                && !this.getVendor().equals(other.getVendor()))) {
            return false;
        }
        if ((this.getUnit() == null && other.getUnit() != null)
                || (this.getUnit() != null
                && !this.getUnit().equals(other.getUnit()))) {
            return false;
        }
        return ((this.getAccount() == null && other.getAccount() != null)
                || this.getAccount() != null
                && this.getAccount().equals(other.getAccount()));
    }

    @Override
    public String toString() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        NumberFormat cf = NumberFormat.getCurrencyInstance();
        return context.getResourceMap(getClass()).getString("toString",
                getServiceType().getTypeName(), unit, df.format(serviceDate),
                getVendor().toString(), cf.format(totalPrice));
    }

    /**
     * Saves this {@code ServiceRecord} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new
     * and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     *
     * @param nue boolean to notify if this record is new or existing
     *
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue)
            throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }

    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_TYPE + ", " + FLD_SVC_DATE + ",  "
                + FLD_COMPLAINT + ", " + FLD_EST + ", " + FLD_WRK_PERFD
                + ", " + FLD_COMP_DATE + ", " + FLD_TOTAL + ", "
                + FLD_VENDOR + ", " + FLD_UNIT + ", " + FLD_ACCT
                + ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setLong(1, serviceType);
            pst.setDate(2, Date.valueOf(serviceDate));
            pst.setString(3, complaint);
            if (estimate != null) {
                pst.setDouble(4, estimate);
            } else {
                pst.setDouble(4, -0.01d);
            }
            pst.setString(5, workPerformed);
            if (completedDate != null) {
                pst.setDate(6, Date.valueOf(completedDate));
            } else {
                pst.setDate(6, null);
            }
            if (totalPrice != null) {
                pst.setDouble(7, totalPrice);
            } else {
                pst.setDouble(7, -0.01d);
            }
            pst.setString(8, vendor);
            pst.setString(9, unit);
            pst.setString(10, account);

            pst.executeUpdate();

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", ServiceRecord.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);

            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

                // Log the error for future reference.
                msg = String.format(
                        "[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_TYPE + " = ?, " + FLD_SVC_DATE + ",  "
                + FLD_COMPLAINT + " = ?, " + FLD_EST + " = ?, " + FLD_WRK_PERFD
                + " = ?, " + FLD_COMP_DATE + " = ?, " + FLD_TOTAL + " = ?, "
                + FLD_VENDOR + " = ?, " + FLD_UNIT + " = ?, " + FLD_ACCT
                + " = ? WHERE " + FLD_ID + " = ?";

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setLong(1, serviceType);
            pst.setDate(2, Date.valueOf(serviceDate));
            pst.setString(3, complaint);
            pst.setDouble(4, estimate);
            pst.setString(5, workPerformed);
            pst.setDate(6, Date.valueOf(completedDate));
            pst.setDouble(7, totalPrice);
            pst.setString(8, vendor);
            pst.setString(9, unit);
            pst.setString(10, account);
            pst.setLong(11, recordId);

            pst.executeUpdate();

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", ServiceRecord.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);

            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

                // Log the error for future reference.
                msg = String.format(
                        "[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    private void validate()
            throws ValidationException {
        ResourceMap rm = context.getResourceMap(getClass());
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");

        if (serviceType == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "serviceType"));
        }
        if (serviceDate == null) {
            throw new ValidationException(this,
                    resourceMap.getString("invalid.value", serviceDate,
                            rm.getString("serviceDate.invalid.reason",
                                    df.format(LocalDate.now()))));
        }
        if (complaint == null || complaint.isBlank() || complaint.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "complaint"));
        }
        if (workPerformed == null || workPerformed.isBlank()
                || workPerformed.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap
                            .getString("required.value.null", "workPerformed"));
        }
        if (!isValidClosingDate(completedDate)) {
            throw new ValidationException(this,
                    resourceMap.getString("invalid.value", completedDate,
                            rm.getString("completedDate.invalid.reason",
                                    df.format(completedDate),
                                    df.format(serviceDate),
                                    df.format(LocalDate.now()))));
        }
        if (totalPrice == null || totalPrice < 0.00d) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", totalPrice,
                            rm.getString("totalPrice.invalid.reason")));
        }
        if (vendor == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "vendor"));
        }
        if (unit == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "unit"));
        }
        if (account == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "account"));
        }
    }

    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }

    private boolean isValidDate(LocalDate date) {
        return date != null
                && (date.isBefore(LocalDate.now())
                || date.equals(LocalDate.now()));
    }

    private boolean isValidClosingDate(LocalDate date) {
        if (date == null && getServiceDate() == null) {
            return true;
        }
        if (date != null && getServiceDate() == null) {
            return false;
        }
        boolean valid = false;
        if (date != null && getServiceDate() != null) {
            LocalDate today = LocalDate.now();
            valid = (date.isAfter(getServiceDate())
                    || date.equals(getServiceDate())
                    && (date.isBefore(today) || date.equals(today)));
        }
        return valid;
    }

    private Long recordId;
    private Long serviceType;
    private LocalDate serviceDate;
    private String complaint;
    private Double estimate;
    private String workPerformed;
    private LocalDate completedDate;
    private Double totalPrice;
    private String vendor;
    private String account;
    private String unit;
    private Integer odometer;

}
