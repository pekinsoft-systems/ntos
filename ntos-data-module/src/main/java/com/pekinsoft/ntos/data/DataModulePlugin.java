/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-data-module
 *  Class      :   DataModulePlugin.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 17, 2024
 *  Modified   :   Jul 17, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 17, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.Plugin;
import com.pekinsoft.framework.*;
import com.pekinsoft.ntos.data.support.LogonDialog;
import java.util.Collections;
import java.util.List;
import javax.help.HelpSet;
import javax.swing.ActionMap;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class DataModulePlugin implements Plugin {
    
    public DataModulePlugin () {
        
    }

    @Override
    public void install() {
        
    }

    @Override
    public void ready() {
        ApplicationContext ctx = Application.getInstance().getContext();
        ResourceUtils resUtils = new ResourceUtils(ctx);
//        try {
            String username = null;
            char[] password = null;
            LogonDialog logon = new LogonDialog(ctx);
            ctx.getApplication().show(logon);
            
            if (!logon.isCancelled()) {
                username = logon.getUsername();
                password = logon.getPassword();
            } else {
                ctx.getApplication().exit(SysExits.EX_OPCANCEL);
            }
            Database.configureDatabase(ctx, username, password);
//            String fileName = "var" + File.separator + "tables";
//            String appDir = ctx.getLocalStorage().getDirectory().toString();
//            Path tableDir = Paths.get(appDir, fileName);
//            if (!Files.exists(tableDir)) {
//                Files.createDirectories(tableDir);
//            }
//            resUtils.copyFromJar("/META-INF/tables", tableDir);
//        } catch (IOException | URISyntaxException e) {
//            System.err.println("Unable to copy table resources from JAr: " 
//                    + e.getMessage());
//        }
    }

    @Override
    public void activate() {
        
    }

    @Override
    public void deactivate() {
        
    }

    @Override
    public boolean isDeactivated() {
        return false;
    }

    @Override
    public void uninstall() {
        
    }

    @Override
    public boolean isUninstalled() {
        return false;
    }

    @Override
    public List<ActionMap> mergeGlobalActions() {
        return Collections.emptyList();
    }

    @Override
    public List<HelpSet> mergeHelpSets() {
        return Collections.emptyList();
    }

    @Override
    public String getPluginName() {
        return "NTOS Data Module (<em>Required</em>)";
    }

    @Override
    public String getPluginVersion() {
        return "0.5.1";
    }

}
