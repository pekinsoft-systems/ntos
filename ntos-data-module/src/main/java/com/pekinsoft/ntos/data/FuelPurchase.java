/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   FuelPurchase.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.common.RoadConditions;
import com.pekinsoft.ntos.common.Terrains;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import java.io.Serializable;
import java.sql.*;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code FuelPurchase} class defines a single record from the table in the
 * database named {@code FUEL_PURCHASES}.
 * <p>
 * This class is used by applications to record all fuel purchases for each unit
 * owned and/or operated by the company. Fuel purchases record all pertinent
 * information regarding the purchase of fuel for the units, including the
 * number of gallons and price per gallon of fuel and, possibly, diesel exhaust
 * fluid (DEF); the vendor from whom the fuel/DEF was purchased; the date of the
 * purchase; the odometer reading at the time of purchase; and, the unit for
 * which the fuel/DEF was purchased.
 * <p>
 * This class also stores some optional data, such as average speed driven 
 * during the prior tank, the average temperature, the road conditions, and the
 * terrain through which the unit was driven. All of these data points affect
 * the fuel economy of a vehicle, so the user may store this information to see
 * why, for example, the vehicle got lower fuel economy during a certain period.
 * <p>
 * The financial data (i.e., total fuel + total DEF prices) is stored in the
 * accounting system's <em>Fuel Journal</em>, and the 
 * {@link #toString() toString} method becomes the {@link Entry entry's} 
 * memo line.
 * <p>
 * All properties in this class are bound, and good faith attempt has been made
 * to note this fact on each getter method's documentation.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class FuelPurchase extends AbstractBean
        implements Serializable, NtosPropertyKeys {

    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} identification field.
     */
    public static final String FLD_ID = "PURCHASE_ID";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} date field.
     */
    public static final String FLD_DATE = "PURCHASE_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} odometer field.
     */
    public static final String FLD_ODO = "ODOMETER";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} gallons of fuel field.
     */
    public static final String FLD_FUEL = "FUEL_GALLONS";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} price per gallon of fuel field.
     */
    public static final String FLD_FUEL_PR = "FUEL_PRICE_GALLON";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} diesel exhaust fluid (DEF) purchased field.
     */
    public static final String FLD_DEF_PURCH = "DEF_PURCHASED";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} gallons of diesel exhaust fluid (DEF) field.
     */
    public static final String FLD_DEF = "DEF_GALLONS";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} price per gallon of diesel exhaust fluid (DEF) field.
     */
    public static final String FLD_DEF_PR = "DEF_PRICE_GALLON";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} average speed field.
     */
    public static final String FLD_AVG_SPD = "AVERAGE_SPEED";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} average temperature field.
     */
    public static final String FLD_AVG_TEMP = "AVERAGE_TEMPERATURE";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} road conditions field.
     */
    public static final String FLD_ROAD = "ROAD_CONDITIONS";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} terrain field.
     */
    public static final String FLD_TERR = "TERRAIN";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} {@link Account} field.
     */
    public static final String FLD_ACCT = "DEFAULT_ACCOUNT";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} {@link Customer vendor} field.
     */
    public static final String FLD_VENDOR = "VENDOR";
    /**
     * Constant representing the table field name in the database for the
     * {@code FuelPurchase} {@link Unit} field.
     */
    public static final String FLD_UNIT = "UNIT";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Entry.class);

    private static final String TABLE_NAME = "FUEL_PURCHASES";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE "
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     *
     * @return a list of all records in the table or an empty list
     */
    public static List<FuelPurchase> getAllRecords() {
        return selectRecords(null, null);
    }

    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     *
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     *
     * @return an ordered list of all records, or an empty list
     */
    public static List<FuelPurchase> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }

    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then
     * {@code null} is returned.
     *
     * @param value the value to find
     *
     * @return the matching record, or {@code null} if no match
     */
    public static FuelPurchase findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        return findRecord(where);
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null}
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     *
     * @param where the search criteria by which to find the desired record
     *
     * @return the matching record if only one matches, the first matching
     *         record if more than one record matches, {@code null} if no
     *         records match
     */
    public static FuelPurchase findRecord(String where) {
        List<FuelPurchase> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     *
     * @param where the search criteria on which to filter the records
     *
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<FuelPurchase> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it
     * will have no effect on the ordering of the results and the records will
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     *
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be
     *                ordered
     *
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<FuelPurchase> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }

    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param id the primary key value of the record to be deleted
     *
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(Long id) {
        if (id == null) {
            return false;
        }

        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setLong(1, id);

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", FuelPurchase.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s",
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return true;
    }

    private static List<FuelPurchase> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        List<FuelPurchase> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                FuelPurchase record = new FuelPurchase();
                record.purchaseId = rs.getLong(1);
                record.purchaseDate = rs.getDate(2).toLocalDate();
                record.fuelGallons = rs.getDouble(3);
                record.fuelPriceGallon = rs.getDouble(4);
                record.defPurchased = rs.getBoolean(5);
                record.defGallons = rs.getDouble(6);
                record.defPriceGallon = rs.getDouble(7);
                record.averageSpeed = rs.getDouble(8);
                record.averageTemperature = rs.getDouble(9);
                record.roadConditions = RoadConditions.of(rs.getString(10));
                record.terrain = Terrains.of(rs.getString(11));
                record.account = rs.getString(12);
                record.vendor = rs.getString(13);
                record.unit = rs.getString(14);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", FuelPurchase.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return records;
    }

    /**
     * Constructs a new, empty {@code FuelPurchase}. Once created, all fields
     * are set to {@code null} and need to be set manually.
     */
    public FuelPurchase() {

    }

    /**
     * Retrieves the unique identity of this {@code FuelPurchase}.
     *
     * @return the unique identity
     */
    public Long getPurchaseId() {
        return purchaseId;
    }

    /**
     * Sets the unique identity of this {@code FuelPurchase}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a long, and is auto-generated by the
     * database. Because it is auto-generated, there is no need for this method
     * to be visible from outside of this class.
     * <p>
     * If the specified {@code entryId} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code entryId} is less than or equal to zero, a
     * {@link NumberFormatException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param purchaseId
     */
    protected void setPurchaseId(Long purchaseId) {
        long old = getPurchaseId();
        this.purchaseId = purchaseId;
        firePropertyChange("purchaseId", old, getPurchaseId());
    }

    /**
     * Retrieves the date of this {@code FuelPurchase}.
     *
     * @return the purchase date
     */
    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * Sets the date of this {@code FuelPurchase}. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a date, with a default value of the
     * current date.
     * <p>
     * If the specified {@code purchaseDate} is {@code null}, it will be set to
     * the current date.
     * <p>
     * If the specified {@code purchaseDate} is later than the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param purchaseDate the purchase date
     *
     * @throws InvalidDataException if {@code purchaseDate} is later than the
     *                              current date
     */
    public void setPurchaseDate(LocalDate purchaseDate) {
        if (purchaseDate == null) {
            purchaseDate = LocalDate.now();
        } else if (!isValidDate(purchaseDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(
                    df.format(purchaseDate) + " is after "
                    + "the current date. Future purchases may not be entered.");
        }
        LocalDate old = getPurchaseDate();
        this.purchaseDate = purchaseDate;
        firePropertyChange("purchaseDate", old, getPurchaseDate());
    }

    /**
     * Retrieves the number of gallons of fuel purchased.
     *
     * @return gallons of fuel purchased
     */
    public double getFuelGallons() {
        return fuelGallons;
    }

    /**
     * Sets the number of gallons of fuel purchased. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a double, and must be greater than zero.
     * <p>
     * If the specified {@code fuelGallons} is less than or equal to zero, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param fuelGallons number of gallons fuel
     *
     * @throws InvalidDataException if {@code fuelGallons} is less than or equal
     *                              to zero
     */
    public void setFuelGallons(double fuelGallons) {
        if (fuelGallons <= 0.00d) {
            throw new InvalidDataException(fuelGallons + " is less than or "
                    + "equal to zero.");
        }
        double old = getFuelGallons();
        this.fuelGallons = fuelGallons;
        firePropertyChange("fuelGallons", old, getFuelGallons());
    }

    /**
     * Retrieves the price per gallon of fuel purchased.
     *
     * @return the price per gallon of fuel
     */
    public double getFuelPriceGallon() {
        return fuelPriceGallon;
    }

    /**
     * Sets the price per gallon of fuel purchased. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a double, and must be greater than zero.
     * <p>
     * If the specified {@code fuelPriceGallon} is less than or equal to zero,
     * an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound field.
     *
     * @param fuelPriceGallon price per gallon of fuel
     *
     * @throws InvalidDataException if {@code fuelPriceGallon} is less than or
     *                              equal to zero
     */
    public void setFuelPriceGallon(double fuelPriceGallon) {
        if (fuelPriceGallon <= 0.00d) {
            throw new InvalidDataException(fuelPriceGallon + " is less than or "
                    + "equal to zero.");
        }
        double old = getFuelPriceGallon();
        this.fuelPriceGallon = fuelPriceGallon;
        firePropertyChange("fuelPriceGallon", old, getFuelPriceGallon());
    }

    /**
     * Determines whether diesel exhaust fluid (DEF) was purchased.
     *
     * @return {@code true} if purchase includes DEF; {@code false} if not
     */
    public boolean isDefPurchased() {
        return defPurchased;
    }

    /**
     * Sets whether diesel exhaust fluid (DEF) was purchased. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a boolean, and has a default value of
     * {@code false}.
     * <p>
     * This is a bound property.
     *
     * @param defPurchased {@code true} if DEF is purchased; {@code false} if
     *                     not
     */
    public void setDefPurchased(boolean defPurchased) {
        boolean old = isDefPurchased();
        this.defPurchased = defPurchased;
        firePropertyChange("defPurchased", old, isDefPurchased());
    }

    /**
     * Retrieves the number of gallons of diesel exhaust fluid (DEF) purchased.
     *
     * @return number of gallons of DEF
     */
    public double getDefGallons() {
        return defGallons;
    }

    /**
     * Sets the number of gallons of diesel exhaust fluid (DEF) purchased. This
     * is a <em>optional</em> field.
     * <p>
     * The data type of this field is a double, and it has a default value of
     * {@code 0.000} gallons. While this field may optionally be set, it is
     * required in the database.
     * <p>
     * If the specified {@code defGallons} is less than zero, an
     * {@link InvalidDataException} is thrown. When setting this value, it <em>
     * must be</em> greater than or equal zero.
     * <p>
     * This is a bound property.
     *
     * @param defGallons the number of gallons of DEF
     *
     * @throws InvalidDataException if {@code defGallons} is less than zero
     */
    public void setDefGallons(double defGallons) {
        if (defGallons < 0.000d) {
            throw new InvalidDataException(defGallons + " is less than zero.");
        }
        double old = getDefGallons();
        this.defGallons = defGallons;
        firePropertyChange("defGallons", old, getDefGallons());
    }

    /**
     * Retrieves the price per gallon of diesel exhaust fluid (DEF) purchased.
     *
     * @return the price per gallon for DEF
     */
    public double getDefPriceGallon() {
        return defPriceGallon;
    }

    /**
     * Sets the price per gallon of diesel exhaust fluid (DEF) purchased. This
     * is an <em>optional</em> field.
     * <p>
     * The data type of this field is a double, and it has a default value of
     * {@code 0.000} per gallon. While this field may optionally be set, it is
     * required in the database.
     * <p>
     * If the specified {@code defPriceGallon} is less than zero, an
     * {@link InvalidDataException} is thrown. When setting this value, it <em>
     * must be</em> greater than or equal to zero.
     * <p>
     * This is a bound property.
     *
     * @param defPriceGallon the price per gallon for DEF
     *
     * @throws InvalidDataException if {@code defPriceGallon} is less than zero
     */
    public void setDefPriceGallon(double defPriceGallon) {
        if (defPriceGallon < 0.000d) {
            throw new InvalidDataException(defPriceGallon + " is less than "
                    + "zero.");
        }
        double old = getDefPriceGallon();
        this.defPriceGallon = defPriceGallon;
        firePropertyChange("defPriceGallon", old, getDefPriceGallon());
    }

    /**
     * Retrieves the average speed driven during the prior tank of fuel.
     *
     * @return the average speed
     */
    public double getAverageSpeed() {
        return averageSpeed;
    }

    /**
     * Sets the average speed driving during the prior tank of fuel. This is an
     * <em>optional</em> field.
     * <p>
     * The data type of this field is a float, and has a default value of 63.4
     * miles per hour. While this field may optionally be set, it is required in
     * the database.
     * <p>
     * If the specified {@code averageSpeed} is less than zero, or greater than
     * 80, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param averageSpeed the average speed driven
     *
     * @throws InvalidDataException if {@code averageSpeed} is less than zero or
     *                              greater than 80
     */
    public void setAverageSpeed(double averageSpeed) {
        if (averageSpeed < 0.0f || averageSpeed > 80.0f) {
            throw new InvalidDataException(averageSpeed + " is outside of the "
                    + "range of 0-80.");
        }
        double old = getAverageSpeed();
        this.averageSpeed = averageSpeed;
        firePropertyChange("averageSpeed", old, getAverageSpeed());
    }

    /**
     * Retrieves the average ambient temperature during the prior tank of fuel.
     *
     * @return the average temperature
     */
    public double getAverageTemperature() {
        return averageTemperature;
    }

    /**
     * Sets the average ambient temperature during the prior tank of fuel. This
     * is an <em>optional</em> field.
     * <p>
     * The data type of this field is a float, with a default value of 76.2
     * degrees Fahrenheit. While this field may optionally be set, it is
     * required in the database.
     * <p>
     * If the specified {@code averageTemperature} is less than -10, or greater
     * than 115, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param averageTemperature
     */
    public void setAverageTemperature(double averageTemperature) {
        if (averageTemperature < -10.0 || averageTemperature > 115) {
            throw new InvalidDataException(
                    averageTemperature + " is outside of "
                    + "the valid range of -10 to 115 degrees Fahrenheit");
        }
        double old = getAverageTemperature();
        this.averageTemperature = averageTemperature;
        firePropertyChange("averageTemperature", old, getAverageTemperature());
    }

    /**
     * Retrieves the average road conditions during the prior tank of fuel.
     *
     * @return the average road conditions
     */
    public RoadConditions getRoadConditions() {
        return roadConditions;
    }

    /**
     * Sets the average road conditions during the prior tank of fuel. This is
     * an <em>optional</em> field.
     * <p>
     * The data type of this field is a string, with a maximum length of fifty
     * (50) characters. The {@link RoadConditions} enumeration is provided for
     * ease of use. This field is required in the database and has a default
     * value of {@code RoadConditions.SUNNY_EXPRESSWAY} (&ldquo;Sunny, dry
     * interstate/expressway&rdquo;).
     * <p>
     * If the specified {@code roadConditions} is {@code null}, it is set to
     * the default value.
     * <p>
     * This is a bound property.
     *
     * @param roadConditions the average road conditions
     */
    public void setRoadConditions(RoadConditions roadConditions) {
        if (roadConditions == null) {
            roadConditions = RoadConditions.SUNNY_EXPRESSWAY;
        }
        RoadConditions old = getRoadConditions();
        this.roadConditions = roadConditions;
        firePropertyChange("roadConditions", old, getRoadConditions());
    }

    /**
     * Retrieves the terrain driven through during the prior tank of fuel.
     *
     * @return the terrain
     */
    public Terrains getTerrain() {
        return terrain;
    }

    /**
     * Sets the terrain driven through during the prior tank of fuel. This is an
     * <em>optional</em> field.
     * <p>
     * The data type of this field is a string, with a maximum length of fifty
     * (50) characters. The {@link Terrains} enumeration is provided for ease of
     * use. This field is required in the database and has a default value of
     * {@code Terrains.ROLLING_PRAIRIE} (&ldquo;Rolling prairie&rdquo;).
     * <p>
     * If the specified {@code terrain} is {@code null}, it is set to the
     * default value.
     * <p>
     * This is a bound property.
     *
     * @param terrain the terrain
     */
    public void setTerrain(Terrains terrain) {
        if (terrain == null) {
            terrain = Terrains.ROLLING_PRAIRIE;
        }
        Terrains old = getTerrain();
        this.terrain = terrain;
        firePropertyChange("terrain", old, getTerrain());
    }

    /**
     * Retrieves the default account from which fuel is purchased.
     *
     * @return the default purchase account
     */
    public Account getAccount() {
        return Account.findByPrimaryKey(account);
    }

    /**
     * Sets the default account from which fuel is purchased. This is a <strong>
     * required</strong> field.
     * <p>
     * If the specified {@code defaultAccount} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param defaultAccount the default purchase account
     */
    public void setAccount(Account defaultAccount) {
        if (defaultAccount == null) {
            throw new NullPointerException("defaultAccount is null.");
        }
        Account old = getAccount();
        this.account = defaultAccount.getAccountNumber();
        firePropertyChange("defaultAccount", old, getAccount());
    }

    /**
     * Retrieves the vendor of this {@code FuelPurchase}.
     *
     * @return the vendor
     */
    public Customer getVendor() {
        return Customer.findByPrimaryKey(vendor);
    }

    /**
     * Sets the vendor of this {@code FuelPurchase}. This is a <strong>required
     * </strong> field.
     * <p>
     * If the {@code vendor} is {@code null}, a {@link NullPointerException} is
     * thrown.
     * <p>
     * This is a bound property.
     *
     * @param vendor the vendor
     */
    public void setVendor(Customer vendor) {
        if (vendor == null) {
            throw new NullPointerException("vendor is null.");
        }
        Customer old = getVendor();
        this.vendor = vendor.getCustomerId();
        firePropertyChange("vendor", old, getVendor());
    }

    /**
     * Retrieves the unit for which the fuel was purchased.
     *
     * @return the unit
     */
    public Unit getUnit() {
        return Unit.findByPrimaryKey(unit);
    }

    /**
     * Sets the unit for which the fuel was purchased. This is a <strong>
     * required</strong> field.
     * <p>
     * If the specified {@code unity} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param unit the unit
     */
    public void setUnit(Unit unit) {
        if (unit == null) {
            throw new NullPointerException("unit is null.");
        }
        Unit old = getUnit();
        this.unit = unit.getUnitNumber();
        firePropertyChange("unit", old, getUnit());
    }

    /**
     * Retrieves the odometer reading at the time of this {@code FuelPurchase}.
     *
     * @return the odometer reading
     */
    public int getOdometer() {
        return getUnit().getOdometer();
    }

    /**
     * Sets the odometer reading at the time of this {@code FuelPurchase}. This
     * is a <strong>required</strong> field.
     * <p>
     * The data type of this field is an integer.
     * <p>
     * If the specified {@code odometer} is less than zero, a
     * {@link NumberFormatException} is thrown.
     * <p>
     * If the specified {@code odometer} is less than the odometer of the prior
     * {@code FuelPurchase} for this {@link #getUnit() unit}, a
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param odometer the odometer reading
     */
    public void setOdometer(int odometer) {
        getUnit().setOdometer(odometer);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (purchaseId != null ? purchaseId.hashCode() : 0);
        hash += (purchaseDate != null ? purchaseDate.hashCode() : 0);
        hash += (fuelGallons != null ? fuelGallons.hashCode() : 0);
        hash += (fuelPriceGallon != null ? fuelPriceGallon.hashCode() : 0);
        hash += (defPurchased ? 1 : 0);
        hash += (defGallons != null ? defGallons.hashCode() : 0);
        hash += (defPriceGallon != null ? defPriceGallon.hashCode() : 0);
        hash += (averageSpeed != null ? averageSpeed.hashCode() : 0);
        hash += (averageTemperature != null ? averageTemperature.hashCode() : 0);
        hash += (roadConditions != null ? roadConditions.hashCode() : 0);
        hash += (terrain != null ? terrain.hashCode() : 0);
        hash += (account != null ? account.hashCode() : 0);
        hash += (vendor != null ? vendor.hashCode() : 0);
        hash += (unit != null ? unit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FuelPurchase)) {
            return false;
        }
        FuelPurchase other = (FuelPurchase) object;
        if ((this.purchaseId == null && other.purchaseId != null)
                || (this.purchaseId != null
                && !this.purchaseId.equals(other.purchaseId))) {
            return false;
        }
        if ((this.purchaseDate == null && other.purchaseDate != null)
                || this.purchaseDate != null
                && !this.purchaseDate.equals(other.purchaseDate)) {
            return false;
        }
        if ((this.fuelGallons == null && other.fuelGallons != null)
                || (this.fuelGallons != null
                && !this.fuelGallons.equals(other.fuelGallons))) {
            return false;
        }
        if ((this.fuelPriceGallon == null && other.fuelPriceGallon != null)
                || (this.fuelPriceGallon != null
                && !this.fuelPriceGallon.equals(other.fuelPriceGallon))) {
            return false;
        }
        if (this.defPurchased != other.defPurchased) {
            return false;
        }
        if ((this.defGallons == null && other.defGallons != null)
                || (this.defGallons != null
                && !this.defGallons.equals(other.defGallons))) {
            return false;
        }
        if ((this.defPriceGallon == null && other.defPriceGallon != null)
                || (this.defPriceGallon != null
                && !this.defPriceGallon.equals(other.defPriceGallon))) {
            return false;
        }
        if ((this.averageSpeed == null && other.averageSpeed != null)
                || (this.averageSpeed != null
                && !this.averageSpeed.equals(other.averageSpeed))) {
            return false;
        }
        if ((this.averageTemperature == null && other.averageTemperature != null)
                || (this.averageTemperature != null
                && !this.averageTemperature.equals(other.averageTemperature))) {
            return false;
        }
        if ((this.roadConditions == null && other.roadConditions != null)
                || (this.roadConditions != null
                && !this.roadConditions.equals(other.roadConditions))) {
            return false;
        }
        if ((this.terrain == null && other.terrain != null)
                || (this.terrain != null && !this.terrain.equals(other.terrain))) {
            return false;
        }
        if ((this.account == null && other.account != null)
                || (this.account != null
                && !this.account.equals(other.account))) {
            return false;
        }
        if ((this.vendor == null && other.vendor != null)
                || (this.vendor != null && !this.vendor.equals(other.vendor))) {
            return false;
        }
        return this.unit.equals(other.unit);
    }

    /**
     * The value returned from this method represents the {@code FuelPurchase}
     * as a string value, which could be used to show the {@code FuelPurchase}
     * in a UI component, but is formatted in such a way as to be used to set
     * the <em>memo</em> field in the {@link Entry} for this
     * {@code FuelPurchase}.
     * 
     * @return the memo line for this {@code FuelPurchase}
     */
    @Override
    public String toString() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        NumberFormat cf = NumberFormat.getCurrencyInstance();
        String value = null;
        if (defPurchased) {
            value = context.getResourceMap(getClass()).getString("toString.DEF", 
                    df.format(purchaseDate), getVendor().getCompany(),
                    getVendor().getCity(), getVendor().getRegion(),
                    String.format("%.3f", fuelGallons), 
                    cf.format(fuelPriceGallon),
                    String.format("%.3f", defGallons),
                    cf.format(defPriceGallon));
        } else {
            value = context.getResourceMap(getClass()).getString("toString.noDEF",
                    df.format(purchaseDate), getVendor().getCompany(),
                    getVendor().getCity(), getVendor().getRegion(),
                    String.format("%.3f", fuelGallons),
                    cf.format(fuelPriceGallon));
        }
        
        return value;
    }
    
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Account} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_DATE + ", " + FLD_FUEL + ", " 
                + FLD_FUEL_PR + ", " + FLD_DEF_PURCH + ", " + FLD_DEF + ", "
                + FLD_DEF_PR + ", " + FLD_AVG_SPD + ", " + FLD_AVG_TEMP + ", "
                + FLD_ROAD + ", " + FLD_TERR + ", " + FLD_ACCT + ", " 
                + FLD_VENDOR + ", " + FLD_UNIT + ") VALUES(?, ?, ?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setDate(1, Date.valueOf(purchaseDate));
            pst.setDouble(2, fuelGallons);
            pst.setDouble(3, fuelPriceGallon);
            pst.setBoolean(4, defPurchased);
            pst.setDouble(5, defGallons);
            pst.setDouble(6, defPriceGallon);
            pst.setDouble(7, averageSpeed);
            pst.setDouble(8, averageTemperature);
            pst.setString(9, roadConditions.toString());
            pst.setString(10, terrain.toString());
            pst.setString(11, account);
            pst.setString(12, vendor);
            pst.setString(13, unit);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_DATE + " = ?, " + FLD_FUEL + " = ?, " 
                + FLD_FUEL_PR + " = ?, " + FLD_DEF_PURCH + " = ?, " 
                + FLD_DEF + " = ?, " + FLD_DEF_PR + " = ?, " 
                + FLD_AVG_SPD + " = ?, " + FLD_AVG_TEMP + " = ?, "
                + FLD_ROAD + " = ?, " + FLD_TERR + " = ?, " + FLD_ACCT + " = ?, " 
                + FLD_VENDOR + " = ?, " + FLD_UNIT + " = ?" + " WHERE " 
                + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setDate(1, Date.valueOf(purchaseDate));
            pst.setDouble(2, fuelGallons);
            pst.setDouble(3, fuelPriceGallon);
            pst.setBoolean(4, defPurchased);
            pst.setDouble(5, defGallons);
            pst.setDouble(6, defPriceGallon);
            pst.setDouble(7, averageSpeed);
            pst.setDouble(8, averageTemperature);
            pst.setString(9, roadConditions.toString());
            pst.setString(10, terrain.toString());
            pst.setString(11, account);
            pst.setString(12, vendor);
            pst.setString(13, unit);
            pst.setLong(14, purchaseId);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        if (purchaseDate == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "purchaseDate"));
        } else if (!isValidDate(purchaseDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", df.format(purchaseDate),
                            "The purchase date must be prior to or the current "
                            + "date. Cannot enter future purchases."));
        }
        if (fuelGallons == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "fuelGallons"));
        } else if (fuelGallons < 0.00) {
                            throw new ValidationException(this,
                    resourceMap.getString("invalid.value", "The number of "
                            + "gallons purchased must be greater than or equal "
                            + "to zero."));

        }
        if (fuelPriceGallon == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "fuelPriceGallon"));
        } else if (fuelPriceGallon < 0.00) {
            throw new ValidationException(this,
                    resourceMap.getString("invalid.value", "The price per "
                            + "gallon must be greater than or equal to zero."));

        }
        if (defPurchased) {
            if (defGallons == null) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", 
                                "defGallons"));
            } else if (defGallons < 0.00) {
                throw new ValidationException(this,
                    resourceMap.getString("invalid.value", "The number of "
                            + "gallons purchased must be greater than or equal "
                            + "to zero."));
            }
            if (defPriceGallon == null) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", 
                                "defPriceGallon"));
            } else if (defPriceGallon < 0.00) {
                throw new ValidationException(this,
                        resourceMap.getString("invalid.value", "The price per "
                                + "gallon must be greater than or equal to "
                                + "zero."));
            }
        }
        if (averageSpeed == null) {
            averageSpeed = 63.4d;
        }
        if (averageTemperature == null) {
            averageTemperature = 76.2d;
        }
        if (roadConditions == null) {
            roadConditions = RoadConditions.SUNNY_EXPRESSWAY;
        }
        if (terrain == null) {
            terrain = Terrains.ROLLING_PRAIRIE;
        }
        if (account == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "defaultAccount"));
        }
        if (vendor == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "vendor"));
        }
        if (unit == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "unit"));
        }
    }

    private boolean isValidDate(LocalDate date) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        LocalDate curDate = LocalDate.now();
        LocalDate oldDate = curDate.minusWeeks(2);
        String where = FLD_UNIT + " = '" + unit + "' AND " + FLD_DATE 
                + " BETWEEN '" + df.format(oldDate) + "' AND '" 
                + df.format(curDate) + "'";
        
        List<FuelPurchase> records = findRecords(where);
        
        if (records.isEmpty()) {
        return date.isBefore(LocalDate.now()) || date.equals(LocalDate.now());
        } else {
            LocalDate lastDate = curDate.minusMonths(2);
            for (FuelPurchase fp : records) {
                if (fp.getPurchaseDate().isAfter(lastDate)) {
                    lastDate = fp.getPurchaseDate();
                }
            }
            
            return lastDate.isBefore(curDate);
        }
    }

    private boolean isOdometerValid(int odometer) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        LocalDate curDate = LocalDate.now();
        LocalDate oldDate = curDate.minusWeeks(2);
        String where = FLD_UNIT + " = '" + unit + "' AND " + FLD_DATE 
                + " BETWEEN '" + df.format(oldDate) + "' AND '" 
                + df.format(curDate) + "'";
        
        List<FuelPurchase> records = findRecords(where);
        
        if (records.isEmpty()) {
            return true;
        } else {
            int lastOdo = odometer;
            for (FuelPurchase fp : records) {
                if (fp.getOdometer() < lastOdo) {
                    lastOdo = fp.getOdometer();
                }
            }
            
            return lastOdo < odometer;
        }
    }

    private Long purchaseId = null;
    private LocalDate purchaseDate = LocalDate.now();
    private Double fuelGallons = 120.00;
    private Double fuelPriceGallon = 4.159;
    private boolean defPurchased = false;
    private Double defGallons = 0.00;
    private Double defPriceGallon = 3.969;
    private Double averageSpeed = 63.5;
    private Double averageTemperature = 76.2;
    private RoadConditions roadConditions = RoadConditions.SUNNY_EXPRESSWAY;
    private Terrains terrain = Terrains.ROLLING_PRAIRIE;
    private String account = null;
    private String vendor = null;
    private String unit = null;

}
