/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Employee.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.PreferenceKeys;
import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPreferenceKeys;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code Employee} class defines a single record within the database table
 * named {@code EMPLOYEES}. Each record in this table defines a single employee
 * which works for, and is paid by, the company.
 * <p>
 * Each employee is assigned to an {@link EmploymentType} in order for updates
 * to a specific employment type to be performed in a batched operation, instead
 * of each record being manually updated.
 * <p>
 * For example, each employee is paid in a certain manner, such as per mile or
 * hour, or even by an annual salary. If all mileage employees are to get a
 * raise of five cents per mile, a batch update could be run against all
 * employees that are paid mileage and increase their mileage rate by the five
 * cents per mile raise. If not assigned to an employment type, each mileage
 * employee's record would need to be located and updated individually.
 * <p>
 * Furthermore, some employees may need to receive bonuses, so this system of
 * storing employees assists the payroll staff in paying out those bonuses
 * without the need of checking each employee's record to see if s/he is
 * entitled to the bonus pay.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Employee extends AbstractBean
        implements Serializable, NtosPropertyKeys {

    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} primary key identity field.
     */
    public static final String FLD_ID = "EMPLOYEE_ID";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} last name field.
     */
    public static final String FLD_LAST_NAME = "LAST_NAME";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} first name field.
     */
    public static final String FLD_FIRST_NAME = "FIRST_NAME";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} middle name field.
     */
    public static final String FLD_MID_NAME = "MIDDLE_NAME";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} street address field.
     */
    public static final String FLD_STREET = "STREET";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} apartment number field.
     */
    public static final String FLD_APT = "APARTMENT";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee} city field.
     */
    public static final String FLD_CITY = "CITY";
    /**
     * Constant representing the table field name in the database for the
     * US State or Canadian Province in which the {@code Employee} is located.
     */
    public static final String FLD_REGION = "REGION";
    /**
     * Constant representing the table field name in the database for the
     * US Zip or Canadian Postal Code for the {@code Employee}'s address.
     */
    public static final String FLD_POSTAL = "POSTAL_CODE";
    /**
     * Constant representing the table field name in the database for the
     * country in which the {@code Employee} is located.
     */
    public static final String FLD_COUNTRY = "COUNTRY";
    /**
     * Constant representing the table field name in the database for the
     * company phone for the {@code Employee}.
     */
    public static final String FLD_COMP_PHONE = "COMPANY_PHONE";
    /**
     * Constant representing the table field name in the database for the phone
     * number for the {@code Employee}.
     */
    public static final String FLD_PERS_PHONE = "PERSONAL_PHONE";
    /**
     * Constant representing the table field name in the database for the
     * company email address for the {@code Employee}.
     */
    public static final String FLD_COMP_EMAIL = "COMPANY_EMAIL";
    /**
     * Constant representing the table field name in the database for the
     * personal email address for the {@code Employee}.
     */
    public static final String FLD_PERS_EMAIL = "PERSONAL_EMAIL";
    /**
     * Constant representing the table field name in the database for the
     * Social Security (or other governmental) Number for the {@code Employee}.
     */
    public static final String FLD_SSN = "SSN";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee}'s driver's license number.
     */
    public static final String FLD_DL_NUM = "DL_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * class of the {@code Employee}'s driver's license.
     */
    public static final String FLD_DL_CLASS = "DL_CLASS";
    /**
     * Constant representing the table field name in the database for the
     * expiration date for the {@code Employee}'s driver's license.
     */
    public static final String FLD_DL_EXP = "DL_EXPIRATION";
    /**
     * Constant representing the table field name in the database for the
     * restrictions on the {@code Employee}'s driver's license.
     */
    public static final String FLD_DL_RES = "DL_RESTRICTIONS";
    /**
     * Constant representing the table field name in the database for the
     * endorsements on the {@code Employee}'s driver's license.
     */
    public static final String FLD_DL_END = "DL_ENDORSEMENTS";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee}'s birth date.
     */
    public static final String FLD_DOB = "DOB";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee}'s hire date.
     */
    public static final String FLD_DOH = "DOH";
    /**
     * Constant representing the table field name in the database for the
     * {@code Employee}'s termination date.
     */
    public static final String FLD_DOT = "DOT";
    /**
     * Constant representing the table field name in the database for the
     * evaluations for the {@code Employee}.
     */
    public static final String FLD_EVALS = "EVALS";
    /**
     * Constant representing the table field name in the database for the
     * {@code EmploymentType} to which the {@code Employee} is assigned.
     */
    public static final String FLD_TYPE = "EMPLOYEE_TYPE";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Employee.class);

    private static final String TABLE_NAME = "EMPLOYEES";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT
            = "DELETE FROM %1$s.%2$s WHERE "
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     *
     * @return a list of all records in the table or an empty list
     */
    public static List<Employee> getAllRecords() {
        return selectRecords(null, null);
    }

    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     *
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     *
     * @return an ordered list of all records, or an empty list
     */
    public static List<Employee> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }

    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then
     * {@code null} is returned.
     *
     * @param value the value to find
     *
     * @return the matching record, or {@code null} if no match
     */
    public static Employee findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        return findRecord(where);
    }

    /**
     * Retrieves the record identified by the specified {@code email} address.
     * <p>
     * If no record matches the email address specified, then {@code null} is
     * returned.
     *
     * @param email the email address to find
     *
     * @return the record with the specified email address or {@code null}
     */
    public static Employee findByPersonalEmailAddress(String email) {
        if (email == null || email.isBlank() || email.isEmpty()) {
            return null;
        }
        String where = FLD_PERS_EMAIL + " = " + email;
        return findRecord(where);
    }

    /**
     * Retrieves the record identified by the specified {@code email} address.
     * <p>
     * If no record matches the email address specified, then {@code null} is
     * returned.
     *
     * @param email the email address to find
     *
     * @return the record with the specified email address or {@code null}
     */
    public static Employee findByCompanyEmailAddress(String email) {
        if (email == null || email.isBlank() || email.isEmpty()) {
            return null;
        }
        String where = FLD_COMP_EMAIL + " = " + email;
        return findRecord(where);
    }

    /**
     * Retrieves the record identified by the specified {@code phone} number.
     * <p>
     * If no record matches the phone number specified, then {@code null} is
     * returned.
     *
     * @param phone the phone number to find
     *
     * @return the record with the specified phone number or {@code null}
     */
    public static Employee findByPersonalPhoneNumber(String phone) {
        if (phone == null || phone.isBlank() || phone.isEmpty()) {
            return null;
        }
        String where = FLD_PERS_PHONE + " = " + phone;
        return findRecord(where);
    }

    /**
     * Retrieves the record identified by the specified {@code phone} number.
     * <p>
     * If no record matches the phone number specified, then {@code null} is
     * returned.
     *
     * @param phone the phone number to find
     *
     * @return the record with the specified phone number or {@code null}
     */
    public static Employee findByCompanyPhoneNumber(String phone) {
        if (phone == null || phone.isBlank() || phone.isEmpty()) {
            return null;
        }
        String where = FLD_COMP_PHONE + " = " + phone;
        return findRecord(where);
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null}
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     *
     * @param where the search criteria by which to find the desired record
     *
     * @return the matching record if only one matches, the first matching
     *         record if more than one record matches, {@code null} if no
     *         records match
     */
    public static Employee findRecord(String where) {
        List<Employee> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves a {@link List} of records that have the specified
     * {@code company} name.
     *
     * @param lastName the company name to find
     *
     * @return a list of records matching the company name or an empty list
     */
    public static List<Employee> findByLastName(String lastName) {
        String where = FLD_LAST_NAME + " = " + lastName;
        return findRecords(where);
    }

    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     *
     * @param where the search criteria on which to filter the records
     *
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Employee> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it
     * will have no effect on the ordering of the results and the records will
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     *
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be
     *                ordered
     *
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Employee> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }

    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param id the primary key value of the record to be deleted
     *
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }

        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setString(1, id);

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Employee.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s",
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return true;
    }

    private static List<Employee> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        List<Employee> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee record = new Employee();
                record.employeeId = rs.getString(1);
                record.lastName = rs.getString(2);
                record.firstName = rs.getString(3);
                record.middleName = rs.getString(4);
                record.street = rs.getString(5);
                record.apt = rs.getString(6);
                record.city = rs.getString(7);
                record.region = rs.getString(8);
                record.postalCode = rs.getString(9);
                record.country = rs.getString(10);
                record.personalPhone = rs.getString(11);
                record.personalEmail = rs.getString(12);
                record.ssn = rs.getString(13);
                record.dob = rs.getDate(19).toLocalDate();
                record.doh = rs.getDate(20).toLocalDate();
                record.dot = rs.getDate(21).toLocalDate();
                record.companyPhone = rs.getString(22);
                record.companyEmail = rs.getString(23);
                record.employeeType = rs.getLong(24);
                record.evaluations = rs.getString(25);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Employee.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return records;
    }

    /**
     * Constructs a new, empty {@code Employee}. Once created, all fields are
     * set
     * to {@code null} and need to be set manually.
     */
    public Employee() {

    }

    /**
     * Retrieves the unique identifier value for this {@code Employee}.
     *
     * @return the unique identifier
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the unique identifier value for this {@code Employee}. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type for this field is a string, with a maximum length of ten
     * (10) characters. This value <em>must be unique</em> in the table and its
     * uniqueness will be checked when the record is saved.
     * <p>
     * If the specified {@code employeeId} is blank, empty, or {@code null}, a
     * {@link NullPointerException} exception is thrown.
     * <p>
     * This is a bound property.
     *
     * @param employeeId the unique identifier
     *
     * @throws NullPointerException if {@code employeeId} is blank, empty, or
     *                              {@code null}
     */
    public void setEmployeeId(String employeeId) {
        if (employeeId == null || employeeId.isBlank() || employeeId.isEmpty()) {
            throw new NullPointerException(
                    "employeeId is blank, empty, or null.");
        }
        String old = getEmployeeId();
        this.employeeId = employeeId.substring(0, 10);
        firePropertyChange("employeeId", old, getEmployeeId());
    }

    /**
     * Retrieves the last name of this {@code Employee}.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of this {@code Employee}. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters.
     * <p>
     * If the specified {@code lastName} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param lastName the last name
     *
     * @throws NullPointerException if {@code lastName} is blank, empty, or
     *                              {@code null}
     */
    public void setLastName(String lastName) {
        String old = getLastName();
        this.lastName = lastName.substring(0, 30);
        firePropertyChange("lastName", old, getLastName());
    }

    /**
     * Retrieves the first name of this {@code Employee}.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name of this {@code Employee}. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of
     * twenty-five (25) characters.
     * <p>
     * If the specified {@code firstName} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param firstName the first name
     *
     * @throws NullPointerException if {@code firstName} is blank, empty, or
     *                              {@code null}
     */
    public void setFirstName(String firstName) {
        if (firstName == null || firstName.isBlank() || firstName.isEmpty()) {
            throw new NullPointerException("firstName is blank, empty, or null.");
        }
        String old = getFirstName();
        this.firstName = firstName.substring(0, 25);
        firePropertyChange("firstName", old, getFirstName());
    }

    /**
     * Retrieves the middle name of this {@code Employee}.
     * <p>
     * The return value should be checked if it is {@code null}.
     *
     * @return the middle name
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the middle name of this {@code Employee}. This is a <em>optional
     * </em> field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of
     * twenty-five (25) characters.
     * <p>
     * This is a bound property.
     *
     * @param middleName the middle name
     *
     * @throws NullPointerException if {@code middleName} is blank, empty, or
     *                              {@code null}
     */
    public void setMiddleName(String middleName) {
        String old = getMiddleName();
        this.middleName = middleName.substring(0, 25);
        firePropertyChange("middleName", old, getMiddleName());
    }

    /**
     * Retrieves the street address for this {@code Employee}.
     *
     * @return the street address
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the street address of this {@code Employee}. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters.
     * <p>
     * If the specified {@code street} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param street the street address
     *
     * @throws NullPointerException if {@code street} is blank, empty, or
     *                              {@code null}
     */
    public void setStreet(String street) {
        if (street == null || street.isBlank() || street.isEmpty()) {
            throw new NullPointerException("street is blank, empty, or null.");
        }
        String old = getStreet();
        this.street = street.substring(0, 40);
        firePropertyChange("street", old, getStreet());
    }

    /**
     * Retrieves the apartment number for this {@code Employee}, if any.
     * <p>
     * The return value should be checked if it is {@code null}.
     *
     * @return the apartment number, if any.
     */
    public String getApt() {
        return apt;
    }

    /**
     * Sets the apartment number for this {@code Employee}, if any. This is an
     * <em>optional</em> field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of fifteen
     * (15) characters.
     * <p>
     * This is a bound property.
     *
     * @param apt the apartment number, if any
     */
    public void setApt(String apt) {
        String old = getApt();
        this.apt = apt.substring(0, 15);
        firePropertyChange("apt", old, getApt());
    }

    /**
     * Retrieves the name of the city in which this {@code Employee} lives.
     *
     * @return the city name
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the name of the city in which this {@code Employee} lives. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters.
     * <p>
     * If the specified {@code city} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param city the name of the city
     *
     * @throws NullPointerException if {@code city} is blank, empty, or
     *                              {@code null}
     */
    public void setCity(String city) {
        if (city == null || city.isBlank() || city.isEmpty()) {
            throw new NullPointerException("city is blank, empty, or null.");
        }
        String old = getCity();
        this.city = city.substring(0, 40);
        firePropertyChange("city", old, getCity());
    }

    /**
     * Retrieves the US State or Canadian Province in which this
     * {@code Employee} lives.
     *
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the US State or Canadian Province in which this {@code Employee}
     * lives. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a required length of two
     * (2) characters.
     * <p>
     * If the specified {@code region} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code region} is not two characters in length, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param region the region
     *
     * @throws NullPointerException if {@code region} is blank, empty, or
     *                              {@code null}
     */
    public void setRegion(String region) {
        if (region == null || region.isBlank() || region.isEmpty()) {
            throw new NullPointerException("region is blank, empty, or null.");
        }
        if (region.length() != 2) {
            throw new InvalidDataException("region must be two characters in "
                    + "length. The specified value is " + region.length()
                    + " characters long.");
        }
        String old = getRegion();
        this.region = region;
        firePropertyChange("stateOrProvince", old, getRegion());
    }

    /**
     * Retrieves the US Zip or Canadian Postal Code for this {@code Employee}.
     *
     * @return the postal code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the US Zip or Canadian Postal Code for this {@code Employee}. This
     * is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of fifteen
     * (15) characters.
     * <p>
     * If the specified {@code postalCode} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param postalCode the postal code
     *
     * @throws NullPointerException if {@code postalCode} is blank, empty, or
     *                              {@code null}
     */
    public void setPostalCode(String postalCode) {
        if (postalCode == null || postalCode.isBlank() || postalCode.isEmpty()) {
            throw new NullPointerException(
                    "postalCode is blank, empty, or null.");
        }
        String old = getPostalCode();
        this.postalCode = postalCode.substring(0, 15);
        firePropertyChange("postalCode", old, getPostalCode());
    }

    /**
     * Retrieves the country in which this {@code Employee} lives.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the country in which this {@code Employee} lives. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters.
     * <p>
     * If the specified {@code country} is blank, empty, or {@code null}, it is
     * defaulted to &ldquo;USA&rdquo;
     * <p>
     * This is a bound property.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        if (country == null || country.isBlank() || country.isEmpty()) {
            country = "USA";
        }
        String old = getCountry();
        this.country = country.substring(0, 30);
        firePropertyChange("country", old, getCountry());
    }

    /**
     * Retrieves the personal phone number for this {@code Employee}.
     *
     * @return the phone number
     */
    public String getPersonalPhone() {
        return personalPhone;
    }

    /**
     * Sets the personal phone number of this {@code Employee}. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of twenty
     * (20) characters. The value presented must be a valid US or Canadian phone
     * number, in any format, with or without the international dialing code.
     * <p>
     * If the specified {@code personalPhone} is blank, empty, or {@code null},
     * a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param personalPhone the personal phone number
     *
     * @throws NullPointerException if {@code country} is blank, empty, or
     *                              {@code null}
     */
    public void setPersonalPhone(String personalPhone) {
        if (personalPhone == null || personalPhone.isBlank()
                || personalPhone.isEmpty()) {
            throw new NullPointerException("personalPhone is blank, empty, or "
                    + "null.");
        }
        String old = getPersonalPhone();
        this.personalPhone = personalPhone;
        firePropertyChange("personalPhone", old, getPersonalPhone());
    }

    /**
     * Retrieves the personal email address for this {@code Employee}, if any.
     *
     * @return the email address or {@code null}
     */
    public String getPersonalEmail() {
        return personalEmail;
    }

    /**
     * Sets the personal email address for this {@code Employee}. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters.
     * <p>
     * If the specified {@code personalEmail} is blank, empty, or {@code null},
     * a {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param personalEmail the personal email address
     *
     * @throws NullPointerException if {@code personalEmail} is blank, empty, or
     *                              {@code null}
     */
    public void setPersonalEmail(String personalEmail) {
        String old = getPersonalEmail();
        this.personalEmail = personalEmail;
        firePropertyChange("personalEmail", old, getPersonalEmail());
    }

    /**
     * Retrieves the Social Security Number (or other governmental ID) for this
     * {@code Employee}.
     *
     * @return the SSN
     */
    public String getSsn() {
        return ssn;
    }

    /**
     * Sets the Social Security Number (or other governmental ID) for this
     * {@code Employee} lives. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of eleven
     * (11) characters.
     * <p>
     * If the specified {@code ssn} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param ssn the ssn
     *
     * @throws NullPointerException if {@code ssn} is blank, empty, or
     *                              {@code null}
     */
    public void setSsn(String ssn) {
        if (ssn == null || ssn.isBlank() || ssn.isEmpty()) {
            throw new NullPointerException("ssn is blank, empty, or null.");
        }
        String old = getSsn();
        this.ssn = ssn;
        firePropertyChange("ssn", old, getSsn());
    }

    /**
     * Retrieves the {@code Employee}'s date of birth.
     *
     * @return the date of birth
     */
    public LocalDate getDob() {
        return dob;
    }

    /**
     * Sets the {@code Employee}'s date of birth. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a date. The date of birth must conform to
     * company rules. If the {@code dob} does not conform to those rules, an
     * {@code InvalidDataException} is thrown.
     * <p>
     * If the specified {@code dob} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param dob the date of birth
     *
     * @throws NullPointerException if {@code dob} is {@code null}
     * @throws InvalidDataException if {@code dob} does not conform to company
     *                              rules
     */
    public void setDob(LocalDate dob) {
        if (dob == null) {
            throw new NullPointerException("dob is null.");
        }
        if (!isHireable(dob)) {
            DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(resourceMap
                    .getString("invalid.value",
                            fmt.format(dob), "Minimum age to hire is "
                            + getMinimumHiringAge()
                            + ", which requires a birth "
                            + "date of at least "
                            + fmt.format(getMinimumHiringBirthDate())));
        }
        LocalDate old = getDob();
        this.dob = dob;
        firePropertyChange("dob", old, getDob());
    }

    /**
     * Retrieves the date on which this {@code Employee} was hired.
     *
     * @return the hire date
     */
    public LocalDate getDoh() {
        return doh;
    }

    /**
     * Sets the date on which this {@code Employee} was hired. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a date. The specified date must be valid
     * within the confines of the integrity check. This includes being after the
     * employee's date of birth and before or on the current date. If the date
     * fails the integrity check, a {@link InvalidDataException} is thrown.
     * <p>
     * If the specified {@code doh} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param doh the hire date
     *
     * @throws NullPointerException if {@code doh} is {@code null}
     * @throws InvalidDataException if {@code doh} fails the integrity checks
     */
    public void setDoh(LocalDate doh) {
        if (doh == null) {
            throw new NullPointerException("doh is null.");
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        if (!isDateValid(doh, dob, false)) {
            throw new InvalidDataException(fmt.format(doh)
                    + " is not valid. It "
                    + "comes before the employee's date of birth");
        }
        if (!doh.equals(LocalDate.now()) && !isDateValid(doh, LocalDate.now(),
                true)) {
            throw new InvalidDataException(fmt.format(doh)
                    + " is not valid. It "
                    + "comes after today's date.");
        }
        LocalDate old = getDoh();
        this.doh = doh;
        firePropertyChange("doh", old, getDoh());
    }

    /**
     * Retrieves the {@code Employee}'s date of termination.
     *
     * @return the date of termination
     */
    public LocalDate getDot() {
        return dot;
    }

    /**
     * Sets the {@code Employee}'s date of termination. This is an <em>optional
     * </em> field and {@code null} values are permitted.
     * <p>
     * The date type of this field is a date. If this field is set, the date
     * supplied must pass an integrity check. This means that it <em>must not
     * </em> predate the hire date.
     * <p>
     * This is a bound property.
     *
     * @param dot the date of termination
     */
    public void setDot(LocalDate dot) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        if (dot != null && !isDateValid(dot, doh, false)) {
            throw new InvalidDataException(fmt.format(dot)
                    + " is not valid. It "
                    + "predates the employee's date of hire.");
        }
        LocalDate old = getDot();
        this.dot = dot;
        firePropertyChange("dot", old, getDot());
    }

    /**
     * Retrieves the company assigned phone number for this {@code Employee}, if
     * any.
     *
     * @return the company phone number or {@code null}
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * Sets the company assigned phone number for this {@code Employee}. This is
     * an <em>optional</em> field.
     * <p>
     * The data type of this field is a string, with a maximum length of twenty
     * (20) characters. If this field is set must be a valid US or Canadian
     * phone number, in any format, with or without the international dialing
     * code. If this field is set, the phone number will be validated on save.
     * <p>
     * This is a bound property.
     *
     * @param companyPhone the company phone number
     */
    public void setCompanyPhone(String companyPhone) {
        String old = getCompanyPhone();
        this.companyPhone = companyPhone;
        firePropertyChange("companyPhone", old, getCompanyPhone());
    }

    /**
     * Retrieves the company assigned email address for this {@code Employee},
     * if any.
     *
     * @return the company email address or {@code null}
     */
    public String getCompanyEmail() {
        return companyEmail;
    }

    /**
     * Sets the company assigned email address for this {@code Employee}. This
     * is an <em>optional</em> field.
     * <p>
     * The data type for this field is a string, with a maximum length of forty
     * (40) characters. If this field is set, the value needs to be a valid
     * email address. The value will be validated when the record is saved.
     * <p>
     * This is a bound property.
     *
     * @param companyEmail the company email address
     */
    public void setCompanyEmail(String companyEmail) {
        String old = getCompanyEmail();
        this.companyEmail = companyEmail;
        firePropertyChange("companyEmail", old, getCompanyEmail());
    }

    /**
     * Retrieves the {@link EmploymentType} of this {@code Employee}.
     *
     * @return the employment type
     */
    public EmploymentType getEmployeeType() {
        return EmploymentType.findByPrimaryKey(employeeType);
    }

    /**
     * Sets the {@link EmploymentType} of this {@code Employee}. This is a
     * <strong>required</strong> field.
     * <p>
     * If the specified {@code employeeType} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param employeeType the employment type
     *
     * @throws NullPointerException if {@code employeeType} is {@code null}
     */
    public void setEmployeeType(EmploymentType employeeType) {
        if (employeeType == null) {
            throw new NullPointerException("employeeType is null.");
        }
        EmploymentType old = getEmployeeType();
        this.employeeType = employeeType.getTypeId();
        firePropertyChange("employmentType", old, getEmployeeType());
    }

    /**
     * Retrieves the {@code Employee}'s evaluations, if any.
     *
     * @return the evaluations or {@code null}
     */
    public String getEvaluations() {
        return evaluations;
    }

    /**
     * Sets the {@code Employee}'s evaluations. This is an <em>optional</em>
     * field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length determined
     * by the system on which the application is running, but typically measured
     * in gigabytes.
     * <p>
     * This is a bound property.
     *
     * @param evaluations the evaluations
     */
    public void setEvaluations(String evaluations) {
        String old = getEvaluations();
        this.evaluations = evaluations;
        firePropertyChange("evaluations", old, getEvaluations());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (employeeId != null
                ? employeeId.hashCode()
                : 0);
        hash += (lastName != null
                ? lastName.hashCode()
                : 0);
        hash += (firstName != null
                ? firstName.hashCode()
                : 0);
        hash += (street != null
                ? street.hashCode()
                : 0);
        hash += (city != null
                ? city.hashCode()
                : 0);
        hash += (region != null
                ? region.hashCode()
                : 0);
        hash += (postalCode != null
                ? postalCode.hashCode()
                : 0);
        hash += (country != null
                ? country.hashCode()
                : 0);
        hash += (personalPhone != null
                ? personalPhone.hashCode()
                : 0);
        hash += (ssn != null
                ? ssn.hashCode()
                : 0);
        hash += (dob != null
                ? dob.hashCode()
                : 0);
        hash += (doh != null
                ? doh.hashCode()
                : 0);
        hash += (employeeType != null
                ? employeeType.hashCode()
                : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        if ((this.employeeId == null && other.employeeId != null)
                || (this.employeeId != null
                && !this.employeeId.equals(other.employeeId))) {
            return false;
        }
        if ((this.lastName == null && other.lastName != null)
                || (this.lastName != null && !this.lastName.equals(
                        other.lastName))) {
            return false;
        }
        if ((this.firstName == null && other.firstName != null)
                || (this.firstName != null && !this.firstName.equals(
                        other.firstName))) {
            return false;
        }
        if ((this.street == null && other.street != null)
                || (this.street != null && !this.street.equals(other.street))) {
            return false;
        }
        if ((this.city == null && other.city != null)
                || (this.city != null && !this.city.equals(other.city))) {
            return false;
        }
        if ((this.region == null && other.region != null)
                || (this.region != null && !this.region.equals(other.region))) {
            return false;
        }
        if ((this.postalCode == null && other.postalCode != null)
                || (this.postalCode != null && !this.postalCode.equals(
                        other.postalCode))) {
            return false;
        }
        if ((this.country == null && other.country != null)
                || (this.country != null && !this.country.equals(other.country))) {
            return false;
        }
        if ((this.personalPhone == null && other.personalPhone != null)
                || (this.personalPhone != null
                && !this.personalPhone.equals(other.personalPhone))) {
            return false;
        }
        if ((this.dob == null && other.dob != null)
                || (this.dob != null && !this.dob.equals(other.dob))) {
            return false;
        }
        if ((this.doh == null && other.doh != null)
                || (this.doh != null && !this.doh.equals(other.doh))) {
            return false;
        }
        return ((this.employeeType == null && other.employeeType != null)
                || (this.employeeType != null && !this.employeeType.equals(
                        other.employeeType)));
    }

    @Override
    public String toString() {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        return "[" + employeeId + "] " + lastName + ", " + firstName
                + " (DoH: " + fmt.format(doh) + ")";
    }

    /**
     * Describes this {@code Employee} in manner useful for debugging
     * purposes. If a UI display string is desired, use
     * {@link #toString() toString} instead.
     *
     * @return a debugging string that describes this object
     */
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }

    /**
     * Saves this {@code Employee} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() typeName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     *
     * @param nue boolean to notify if this record is new or existing
     *
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        if (nue) {
            if (!isUniqueInTable()) {
                throw new ValidationException(this,
                        resourceMap.getString("not.unique", employeeId));
            }
            insert();
        } else {
            validate();
            update();
        }
    }

    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_LAST_NAME + ", " + FLD_FIRST_NAME
                + ", "
                + FLD_MID_NAME + ", " + FLD_STREET + ", " + FLD_APT + ", "
                + FLD_CITY + ", " + FLD_REGION + ", " + FLD_POSTAL + ", "
                + FLD_COUNTRY + ", " + FLD_PERS_PHONE + ", " + FLD_PERS_EMAIL
                + ", " + FLD_SSN + ", " + FLD_DOB + ", " + FLD_DOH + ", "
                + FLD_DOT + ", " + FLD_COMP_PHONE + ", " + FLD_COMP_EMAIL + ", "
                + FLD_TYPE + ", " + FLD_EVALS
                + ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setString(1, employeeId);
            pst.setString(2, lastName);
            pst.setString(3, firstName);
            pst.setString(4, middleName);
            pst.setString(5, street);
            pst.setString(6, apt);
            pst.setString(7, city);
            pst.setString(8, region);
            pst.setString(9, postalCode);
            pst.setString(10, country);
            pst.setString(11, personalPhone);
            pst.setString(12, personalEmail);
            pst.setString(13, ssn);
            pst.setDate(14, java.sql.Date.valueOf(dob));
            pst.setDate(15, java.sql.Date.valueOf(doh));
            pst.setDate(16, java.sql.Date.valueOf(dot));
            pst.setString(17, companyPhone);
            pst.setString(18, companyEmail);
            pst.setLong(19, employeeType);
            pst.setString(20, evaluations);

            pst.executeUpdate();

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Employee.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);

            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

                // Log the error for future reference.
                msg = String.format(
                        "[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_LAST_NAME + " = ?, " + FLD_FIRST_NAME + " = ?, "
                + FLD_MID_NAME + " = ?, " + FLD_STREET + " = ?, "
                + FLD_APT + " = ?, " + FLD_CITY + " = ?, "
                + FLD_REGION + " = ?, " + FLD_POSTAL + " = ?, "
                + FLD_COUNTRY + " = ?, " + FLD_PERS_PHONE + " = ?, "
                + FLD_PERS_EMAIL + " = ?, " + FLD_SSN + " = ?, "
                + FLD_DOB + " = ?, " + FLD_DOH + " = ?, "
                + FLD_DOT + " = ?, " + FLD_COMP_PHONE + " = ?, "
                + FLD_COMP_EMAIL + " = ?, " + FLD_TYPE + " = ?, "
                + FLD_EVALS + " = ? WHERE " + FLD_ID + " = ?";

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setString(1, lastName);
            pst.setString(2, firstName);
            pst.setString(3, middleName);
            pst.setString(4, street);
            pst.setString(5, apt);
            pst.setString(6, city);
            pst.setString(7, region);
            pst.setString(8, postalCode);
            pst.setString(9, country);
            pst.setString(10, personalPhone);
            pst.setString(11, personalEmail);
            pst.setString(12, ssn);
            pst.setDate(13, java.sql.Date.valueOf(dob));
            pst.setDate(14, java.sql.Date.valueOf(dob));
            pst.setDate(15, java.sql.Date.valueOf(dob));
            pst.setString(16, companyPhone);
            pst.setString(17, companyEmail);
            pst.setLong(18, employeeType);
            pst.setString(19, evaluations);
            pst.setString(20, employeeId);

            pst.executeUpdate();

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Employee.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);

            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

                // Log the error for future reference.
                msg = String.format(
                        "[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }

    private void validate() throws ValidationException {
        if (employeeId == null || employeeId.isBlank() || employeeId.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "employeeId"));
        }
        if (getEmployeeType() == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "employeeType"));
        }
        if (lastName == null || lastName.isBlank() || lastName.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "lastName"));
        }
        if (firstName == null || firstName.isBlank() || firstName.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "firstName"));
        }
        if (street == null || street.isBlank() || street.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "street"));
        }
        if (city == null || city.isBlank() || city.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "city"));
        }
        if (region == null || region.isBlank() || region.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "region"));
        }
        if (postalCode == null || postalCode.isBlank() || postalCode
                .isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null",
                            "postalCode"));
        }
        if (country == null || country.isBlank() || country.isEmpty()) {
            country = "USA"; // Default value
        }
        if (personalPhone == null || personalPhone.isBlank() 
                || personalPhone.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "personalPhone"));
        }
        if (ssn == null || ssn.isBlank() || ssn.isEmpty()) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "ssn"));
        }
        if (dob == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "dob"));
        }
        if (doh == null) {
            throw new ValidationException(this,
                    resourceMap.getString("required.value.null", "doh"));
        }
    }

    private boolean isUniqueInTable() {
        List<Employee> records = getAllRecords();
        int counter = 0;
        for (Employee record : records) {
            if (record.getEmployeeId().startsWith(employeeId)) {
                counter++;
            }
        }

        if (counter == 0) {
            return true;
        } else {
            if (counter < 10) {
                employeeId += "0" + counter;
                return true;
            } else if (counter < 100) {
                employeeId += counter;
                return true;
            } else {
                return false;
            }
        }
    }

    private boolean isHireable(LocalDate date) {
        return date.isBefore(getMinimumHiringBirthDate());
    }

    private int getMinimumHiringAge() {
        return (context.getPreferences(ApplicationContext.USER)
                .getBoolean(PreferenceKeys.PREF_DEVELOPMENT_ENVIRONMENT, false)
                ? context.getPreferences(ApplicationContext.USER)
                        .getInt(NtosPreferenceKeys.PREF_MINIMUM_WORK_AGE, 21)
                : context.getPreferences(ApplicationContext.SYSTEM)
                        .getInt(NtosPreferenceKeys.PREF_MINIMUM_WORK_AGE, 21));
    }

    private LocalDate getMinimumHiringBirthDate() {
        return LocalDate.now().minusYears(getMinimumHiringAge());
    }

    /**
     * Checks that a date is valid relative to another date.
     *
     * @param checkDate the date of interest to validate
     * @param compareTo the date to which it should be compared
     * @param before    {@code true} if {@code checkDate} should be before
     *                  {@code compareTo}; {@code false} for after
     *
     * @return whether the {@code checkDate} is valid per the given requirements
     */
    private boolean isDateValid(LocalDate checkDate, LocalDate compareTo,
            boolean before) {
        if (before) {
            return checkDate.isBefore(compareTo);
        } else {
            return checkDate.isAfter(compareTo);
        }
    }

    private String employeeId = null;
    private String lastName = null;
    private String firstName = null;
    private String middleName = null;
    private String street = null;
    private String apt = null;
    private String city = null;
    private String region = null;
    private String postalCode = null;
    private String country = null;
    private String personalPhone = null;
    private String personalEmail = null;
    private String ssn = null;
    private LocalDate dob = null;
    private LocalDate doh = LocalDate.now();
    private LocalDate dot = null;
    private String companyPhone = null;
    private String companyEmail = null;
    private Long employeeType = null;
    private String evaluations = null;

}
