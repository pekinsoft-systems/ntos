/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Account.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import static com.pekinsoft.utils.PropertyKeys.KEY_ADD_NOTIFICATION;
import static com.pekinsoft.utils.PropertyKeys.KEY_STATUS_MESSAGE;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code Account} class defines a single record within the database table
 * named {@code ACCOUNTS}. Each record in this table defines a single account in
 * which financial data is stored for the application's accounting system.
 * <p>
 * An account is assigned to an {@link AccountType} to specify to the accounting
 * system how it is affected by <em>Credits (Cr)</em> and <em>Debits (Dr)</em>.
 * For example, if the account is assigned to a Credit account type, whenever a
 * credit is posted to the account, its balance is increased.
 * <p>
 * An account maintains the financial transactions for a specific type of data,
 * such as an expense, an income, and asset, or a liability. These types are 
 * defined by the account type to which the account is assigned and effects how
 * monies are reported for tax and business loan purposes. Other accounts detail
 * the financial net worth of the business, which is why proper categorization 
 * of financial transactions is important. Furthermore, the accounting system
 * provides a transactional history of the company's finances.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Account extends AbstractBean 
        implements Serializable, NtosPropertyKeys {

    /**
     * Constant representing the table field name in the database for the
     * {@code Account} primary key identity field.
     */
    public static final String FLD_ID = "ACCOUNT_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Account} <em>human-readable</em> name field.
     */
    public static final String FLD_NAME = "ACCOUNT_NAME";
    /**
     * Constant representing the table field name in the database for the
     * {@code Account} short description of the account and its use.
     */
    public static final String FLD_DESC = "DESCRIPTION";
    /**
     * Constant representing the table field name in the database for the
     * {@code Account} that defines the beginning balance for the account.
     */
    public static final String FLD_BALANCE = "BEGINNING_BALANCE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Account} that defines the {@link AccountType} of this account.
     */
    public static final String FLD_TYPE = "ACCOUNT_TYPE";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Account.class);
    
    private static final String TABLE_NAME = "ACCOUNTS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     * 
     * @return a list of all records in the table or an empty list
     */
    public static List<Account> getAllRecords() {
        return selectRecords(null, null);
    }
    
    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     * 
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     * 
     * @return an ordered list of all records, or an empty list
     */
    public static List<Account> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }
    
    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param value the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static Account findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        List<Account> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null} 
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     * 
     * @param where the search criteria by which to find the desired record
     * 
     * @return the matching record if only one matches, the first matching 
     *         record if more than one record matches, {@code null} if no 
     *         records match
     */
    public static Account findRecord(String where) {
        List<Account> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     * 
     * @param where the search criteria on which to filter the records
     * 
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Account> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the 
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it 
     * will have no effect on the ordering of the results and the records will 
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     * 
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be 
     *                ordered
     * 
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Account> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }
    
    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to 
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     * 
     * @param id the primary key value of the record to be deleted
     * 
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }
        
        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, id);
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s", 
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return true;
    }
    
    private static List<Account> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }
        
        List<Account> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Account record = new Account();
                record.accountNumber = rs.getString(1);
                record.accountName = rs.getString(2);
                record.description = rs.getString(3);
                record.beginningBalance = rs.getDouble(4);
                record.accountType = rs.getLong(5);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }

    /**
     * Constructs a new, empty {@code Account}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     */
    public Account () {
        
    }

    /**
     * Retrieves the <em>unique</em> identifying number for this 
     * {@code Account}.
     * 
     * @return the unique {@code Account} number
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the <em>unique</em> identifying number for this {@code Account}.
     * This is a <strong>required</strong> value.
     * <p>
     * The data type of this field is a string, with a maximum length of twenty
     * (20) characters and <em>must be unique</em> in the table. The uniqueness
     * of this value is checked when the record is
     * {@link #saveRecord(boolean) saved}.
     * <p>
     * Implementation Note: Once an account number is set, the user should be
     * prevented from changing it due to it being required to be unique in the
     * table, along with the {@link #getAccountName() accountName}. Since the
     * number is the primary key, once it is set, it should be protected from
     * change.
     * <p>
     * If the specified {@code accountNumber} is blank, empty, or {@code null},
     * a {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param accountNumber the unique {@code Account} number
     * 
     * @throws NullPointerException if {@code accountNumber} is blank, empty, or
     *                              {@code null}
     */
    public void setAccountNumber(String accountNumber) {
        if (accountNumber == null || accountNumber.isBlank()
                || accountNumber.isEmpty()) {
            throw new NullPointerException("accountNumber is blank, empty, or "
                    + "null.");
        }
        String old = getAccountNumber();
        this.accountNumber = accountNumber;
        firePropertyChange("accountNumber", old, getAccountNumber());
    }

    /**
     * Retrieves the <em>unique, human-readable</em> name for this 
     * {@code Account}.
     * 
     * @return the unique {@code Account} name
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the <em>unique, human-readable</em> name for this {@code Account}.
     * This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters and <em>must be unique</em> in the table. The uniqueness
     * of this value is checked when the record is
     * {@link #saveRecord(boolean) saved}.
     * <p>
     * If the specified {@code accountName} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param accountName the unique {@code Account} name
     * 
     * @throws NullPointerException if {@code accountName} is blank, empty, or
     *                              {@code null}
     */
    public void setAccountName(String accountName) {
        if (accountName == null || accountName.isBlank()
                || accountName.isEmpty()) {
            throw new NullPointerException("accountName is blank, empty, or "
                    + "null.");
        }
        String old = getAccountName();
        this.accountName = accountName;
        firePropertyChange("accountName", old, getAccountName());
    }

    /**
     * Retrieves the description of this {@code Account}.
     * 
     * @return the {@code Account} description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of this {@code Account}. This value is <em>optional
     * </em>.
     * <p>
     * The data type of this field is a string, with a maximum length of one
     * thousand (1,000) characters. This field allows {@code null} values.
     * <p>
     * This is a bound property.
     * 
     * @param description the {@code Account} description
     */
    public void setDescription(String description) {
        String old = getDescription();
        this.description = description;
        firePropertyChange("description", old, getDescription());
    }

    /**
     * Retrieves the beginning balance for this {@code Account}.
     * 
     * @return the {@code Account}'s beginning balance
     */
    public double getBeginningBalance() {
        return beginningBalance;
    }

    /**
     * Sets the beginning balanace for this {@code Account}. This value is
     * <strong>required</strong>.
     * <p>
     * The data type of this field is a double, that allows values from
     * {@code -9,999,999,999,999.99} through {@code 9,999,999,999,999.99}. This
     * field does not allow for {@code null} values.
     * <p>
     * If the specified {@code beginningBalance} is {@code null}, the value is
     * set to {@code 0.00}.
     * <p>
     * This is a bound property.
     * 
     * @param beginningBalance the beginning balance for this {@code Account}
     */
    public void setBeginningBalance(Double beginningBalance) {
        if (beginningBalance == null) {
            beginningBalance = 0.00d;
        }
        double old = getBeginningBalance();
        this.beginningBalance = beginningBalance;
        firePropertyChange("beginningBalance", old, getBeginningBalance());
    }

    /**
     * Retrieves the {@link AccountType} to which this {@code Account} is
     * assigned.
     * 
     * @return this {@code Account}'s {@code AccountType}
     */
    public AccountType getAccountType() {
        return AccountType.findByPrimaryKey(accountType);
    }

    /**
     * Sets the {@link AccountType} to which this {@code Account} is assigned.
     * This value is <strong>required</strong>.
     * <p>
     * The data type of this field is a long, which is taken from the passed in
     * {@code AccountType} object's {@link AccountType#getTypeId() typeId}
     * property.
     * <p>
     * If the specified {@code accountType} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param accountType the {@code AccountType} to which this {@code Account}
     *                    should be assigned
     * 
     * @throws NullPointerException if {@code acountType} is {@code null}
     */
    public void setAccountType(AccountType accountType) {
        if (accountType == null) {
            throw new NullPointerException("accountType is null.");
        }
        AccountType old = getAccountType();
        this.accountType = accountType.getTypeId();
        firePropertyChange("accountType", old, getAccountType());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountNumber != null ? accountNumber.hashCode() : 0);
        hash += (accountName != null ? accountName.hashCode() : 0);
        hash += (description != null ? description.hashCode() : 0);
        hash += (beginningBalance != null ? beginningBalance.hashCode() : 0);
        hash += (accountType != null ? accountType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (object == null || !(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        if ((this.accountNumber == null && other.accountNumber != null) ||
                (this.accountNumber != null &&
                !this.accountNumber.equals(other.accountNumber))) {
            return false;
        }
        if ((this.accountName == null && other.accountName != null
                || (this.accountName != null
                && !this.accountName.equals(other.accountName)))) {
            return false;
        }
        if ((this.beginningBalance == null && other.beginningBalance != null)
                || (this.beginningBalance != null
                && !this.beginningBalance.equals(other.beginningBalance))) {
            return false;
        }
        return ((this.accountType == null && other.accountType != null)
                || (this.accountType != null
                && !this.accountType.equals(other.accountType)));
    }

    @Override
    public String toString() {
        return accountName + " [" + accountNumber + "]";
    }
    
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Account} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_NAME + ", " + FLD_DESC + ",  " 
                + FLD_BALANCE + ", " + FLD_TYPE + ") VALUES(?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, accountNumber);
            pst.setString(2, accountName);
            pst.setString(3, description);
            pst.setDouble(4, beginningBalance);
            pst.setLong(5, accountType);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_NAME + " = ?, " + FLD_DESC + " = ?, " + FLD_BALANCE 
                + " = ?, " + FLD_TYPE + " = ? WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, accountName);
            pst.setString(2, description);
            pst.setDouble(3, beginningBalance);
            pst.setLong(4, accountType);
            pst.setString(5, accountNumber);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        if (accountNumber == null || accountNumber.isBlank()
                || accountNumber.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "accountNumber"));
        }
        if (accountName == null || accountName.isBlank() || accountName.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "accountName"));
        }
        Account t = findByPrimaryKey(accountNumber);
        if (t != null) {
            // Validating the uniqueness of an account name that has been changed.
            if (!t.accountName.equals(accountName)) {
                // The name value was changed in the edit.
                if (!isNameUniqueInTable()) {
                    throw new ValidationException(this, 
                            resourceMap.getString("not.unique", accountName));
                }
            }
        } else {
            // Validating the uniqueness of a new record's number and name.
            if (!isNumberUniqueInTable()) {
                throw new ValidationException(this, 
                        resourceMap.getString("not.unique", accountNumber));
            }
            if (!isNameUniqueInTable()) {
                throw new ValidationException(this, 
                        resourceMap.getString("not.unique", accountName));
            }
        }
        if (beginningBalance == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", 
                            "beginningBalance"));
        }
        if (accountType == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "accountType"));
        }
    }
    
    private boolean isNameUniqueInTable() {
        List<Account> records = findRecords(FLD_NAME + " = " + accountName);
        return (!records.isEmpty() && records.size() == 1);
    }
    
    private boolean isNumberUniqueInTable() {
        List<Account> records = findRecords(FLD_ID + " = " + accountNumber);
        return (!records.isEmpty() && records.size() == 1);
    }

    private String accountNumber = null;
    private String accountName = null;
    private String description = null;
    private Double beginningBalance = 0.00;
    private Long accountType = null;

}
