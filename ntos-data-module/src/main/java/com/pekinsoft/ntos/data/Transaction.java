/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Transaction.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code Transaction} class defines a single record in the database table
 * named {@code TRANSACTIONS}. Each of these records define a single financial
 * transaction in the accounting system.
 * <p>
 * A transaction belongs to a specific {@link Journal} and records all of the
 * <em>Credits</em> and <em>Debits</em> made to that Journal. Each transaction
 * consists of a date, code, and <em>optional</em> (<strong>but 
 * recommended</strong>) memo. Also, a transaction tracks whether it has been
 * posted to the <em>General Ledger</em>.
 * <p>
 * While the above described details make up the properties of the 
 * {@code Transaction} class, a transaction is also made up of at least two
 * {@link Entry entries}. The various entries cannot be saved into the 
 * transaction until all Debits and Credits are balanced. Therefore, while it is
 * required that a transaction consists of a minimum of two (2) entries, there
 * could be more than two entries in a single transaction.
 * <p>
 * All properties of this class are bound. A good faith effort has been made to
 * note this in each method's documentation.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Transaction extends AbstractBean 
        implements Serializable, NtosPropertyKeys {
    
    public static final String FLD_ID = "TX_ID";
    public static final String FLD_JOURNAL = "LEDGER";
    public static final String FLD_DATE = "TX_DATE";
    public static final String FLD_TYPE = "TX_CODE";
    public static final String FLD_MEMO = "MEMO";
    public static final String FLD_POSTED = "POSTED";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Account.class);
    
    private static final String TABLE_NAME = "TRANSACTIONS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     * 
     * @return a list of all records in the table or an empty list
     */
    public static List<Transaction> getAllRecords() {
        return selectRecords(null, null);
    }
    
    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     * 
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     * 
     * @return an ordered list of all records, or an empty list
     */
    public static List<Transaction> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }
    
    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param value the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static Transaction findByPrimaryKey(long value) {
        String where = FLD_ID + " = " + value;
        List<Transaction> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null} 
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     * 
     * @param where the search criteria by which to find the desired record
     * 
     * @return the matching record if only one matches, the first matching 
     *         record if more than one record matches, {@code null} if no 
     *         records match
     */
    public static Transaction findRecord(String where) {
        List<Transaction> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     * 
     * @param where the search criteria on which to filter the records
     * 
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Transaction> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the 
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it 
     * will have no effect on the ordering of the results and the records will 
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     * 
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be 
     *                ordered
     * 
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Transaction> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }
    
    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to 
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     * 
     * @param id the primary key value of the record to be deleted
     * 
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(Long id) {
        if (id == null || id <= 0L) {
            return false;
        }
        
        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);
        
        Connection conn = null;
        PreparedStatement pst = null;
        int affectedRecords = 0;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setLong(1, id);
            
            affectedRecords = pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Transaction.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s", 
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
            
            return affectedRecords > 0;
        }
    }
    
    private static List<Transaction> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }
        
        List<Transaction> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Transaction record = new Transaction();
                
                record.txId = rs.getLong(1);
                record.journal = rs.getString(2);
                record.txDate = rs.getDate(3).toLocalDate();
                record.txCode = rs.getLong(4);
                record.memo = rs.getString(5);
                record.posted = rs.getBoolean(6);
                
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Transaction.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }

    /**
     * Constructs a new, empty {@code Transaction}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     */
    public Transaction () {
        
    }

    /**
     * Retrieves the unique identifier value for this {@code Transaction}.
     * 
     * @return the unique identifier
     */
    public Long getTxId() {
        return txId;
    }

    /**
     * Sets the unique identifier value for this {@code Transaction}. This is a
     * <strong>required</strong> field and is the primary key in the table.
     * <p>
     * The data type of this field is a long, and it is auto-generated by the
     * database. No external class should be able to set this value. This method
     * exists only for reading records in from the database in the static 
     * methods of this class. Therefore, it is defined as {@code protected}, 
     * which will allow a subclass to redefine the manner in which unique keys
     * might be generated.
     * <p>
     * If the specified {@code txId} is {@code null}, a
     * {@link NullPointerException} is thrown. This should never happen for the
     * reasons detailed above.
     * <p>
     * This is a bound property.
     * 
     * @param txId the unique identifier
     */
    protected void setTxId(Long txId) {
        if (txId == null) {
            throw new NullPointerException("txId is null.");
        }
        long old = getTxId();
        this.txId = txId;
        firePropertyChange("txId", old, getTxId());
    }

    /**
     * Retrieves the date of this {@code Transaction}. 
     * 
     * @return the transaction date
     */
    public LocalDate getTxDate() {
        return txDate;
    }

    /**
     * Sets the date of this {@code Transaction}.This is a <strong> required
     * </strong> field.
     * <p>
     * The data type of this field is a date. Each transaction is tied to a 
     * single date on which it was performed.
     * <p>
     * If the specified {@code txDate} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code txDate} is after the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param txDate the transaction date
     * 
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setTxDate(LocalDate txDate) {
        if (txDate == null) {
            throw new NullPointerException("txDate is null.");
        } else if(!isValidDate(txDate)) {
            throw new InvalidDataException(txDate + " is not on or before the "
                    + "current date.");
        }
        LocalDate old = getTxDate();
        this.txDate = txDate;
        firePropertyChange("txDate", old, getTxDate());
    }

    /**
     * Retrieves the memo for this {@code Transaction}.
     * 
     * @return the memo
     */
    public String getMemo() {
        return memo;
    }

    /**
     * Sets the memo for this {@code Transaction}. This field is <em>optional
     * </em>, but highly recommended to provide a detailed history of 
     * transactions.
     * <p>
     * The data type of this field is a string, with a maximum length of five
     * hundred (500) characters. It is allowed to be blank, empty, or 
     * {@code null}, but this is highly discouraged.
     * <p>
     * This is a bound property.
     * 
     * @param memo the memo
     */
    public void setMemo(String memo) {
        String old = getMemo();
        this.memo = memo;
        firePropertyChange("memo", old, getMemo());
    }

    /**
     * Determines whether this {@code Transaction} has been posted to the <em>
     * General Ledger</em>.
     * 
     * @return {@code true} if posted to the General Ledger; {@code false} 
     *         otherwise
     */
    public boolean getPosted() {
        return posted;
    }

    /**
     * Sets whether this {@code Transaction} has been posted to the <em>General
     * Ledger</em>. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of 
     * {@code false}. This field is only useful in accounting systems that 
     * allow for batch posting of <em>Journals</em>. If all transactions are, by
     * default, automatically posted when they are saved, then this field is of
     * no importance.
     * <p>
     * This is a bound property.
     * 
     * @param posted {@code true} if posted to the General Ledger; {@code false}
     *               otherwise
     */
    public void setPosted(boolean posted) {
        boolean old = getPosted();
        this.posted = posted;
        firePropertyChange("posted", old, getPosted());
    }

    /**
     * Retrieves the {@link Journal} to which this {@code Transaction} belongs.
     * 
     * @return the Journal
     */
    public Journal getJournal() {
        return Journal.findByPrimaryKey(journal);
    }

    /**
     * Sets the {@link Journal} to which this {@code Transaction} belongs. This
     * is a <strong>required</strong> field.
     * <p>
     * Every transaction in an accounting system belongs to a <em>Journal</em>,
     * which is related to a specific {@link Account}. This allows for detailed
     * examination of financial transactions. The transaction remains only in
     * the Journal until such time that it is posted to the <em>General 
     * Ledger</em>. Once the transaction is posted to the General Ledger, it 
     * becomes a <strong><em>permanent record</em></strong> of the accounting 
     * system.
     * <p>
     * While the transaction remains unposted, it may be freely deleted or 
     * edited. However, once it is posted to the General Ledger, it may no 
     * longer be edited nor deleted. Therefore, the transaction should be highly
     * scrutinized prior to posting to be sure that it is 100% accurate.
     * <p>
     * This is not to say that a posted transaction cannot be changed at all.
     * However, to change a transaction that has already been posted involves a
     * few steps:
     * <ol>
     * <li>A &ldquo;reverse transaction&rdquo; must be entered into the General
     * Ledger. This is a transaction that is the <em>exact opposite</em> of the
     * incorrect transaction.</li>
     * <li>A &ldquo;corrected transaction&rdquo; must be entered into the 
     * General Ledger. This is a repeat of the original transaction, except 
     * without the incorrect data from before.</li>
     * </ol>
     * <p>
     * If the specified {@code journal} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param journal the Journal to which this transaction belongs
     * 
     * @throws NullPointerException
     */
    public void setJournal(Journal journal) {
        Journal old = getJournal();
        this.journal = journal.getJournalName();
        firePropertyChange("journal", old, getJournal());
    }

    /**
     * Retrieves the {@link TxType transaction code} for this
     * {@code Transaction}.
     * 
     * @return the transaction code
     */
    public TxType getTxCode() {
        return TxType.findByPrimaryKey(txCode);
    }

    /**
     * Sets the {@link TxType transaction code} for this {@code Transaction}.
     * This is a <strong>required</strong> field.
     * <p>
     * The transaction code gives a simple overview of what action was taken in
     * this transaction. Simple transaction codes are values that describe how
     * a transaction was performed, such as using a debit or credit card, an
     * electronic funds transfer, an automatic payment, etc. The transaction 
     * code provides another way of filtering the transactions that a user may
     * want to look at.
     * <p>
     * If the specified {@code txCode} is {@code null}, a 
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param txCode the transaction code
     * 
     * @throws NullPointerException
     */
    public void setTxCode(TxType txCode) {
        if (txCode == null) {
            throw new NullPointerException("txCode is null.");
        }
        TxType old = getTxCode();
        this.txCode = txCode.getTypeId();
        firePropertyChange("txCode", old, getTxCode());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (txId != null ? txId.hashCode() : 0);
        hash += (txDate != null ? txDate.hashCode() : 0);
        hash += (getTxCode() != null ? getTxCode().hashCode() : 0);
        hash += (memo != null ? memo.hashCode() : 0);
        hash += (posted ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transaction)) {
            return false;
        }
        Transaction other = (Transaction) object;
        if ((this.txId == null && other.txId != null) ||
                (this.txId != null && !this.txId.equals(other.txId))) {
            return false;
        }
        if ((this.getJournal() == null && other.getJournal() != null)
                || (this.getJournal() != null
                && !this.getJournal().equals(other.getJournal()))) {
            return false;
        }
        if ((this.txDate == null && other.txDate != null)
                || (this.txDate != null && !this.txDate.equals(other.txDate))) {
            return false;
        }
        if ((this.getTxCode() == null && other.getTxCode() != null)
                || (this.getTxCode() != null
                && !this.getTxCode().equals(other.getTxCode()))) {
            return false;
        }
        if ((this.memo == null && other.memo != null)
                || (this.memo != null && !this.memo.equals(other.memo))) {
            return false;
        }
        return this.posted == other.posted;
    }

    @Override
    public String toString() {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        return getJournal().getJournalName() + ": " + df.format(txDate)
                + " [" + getTxCode().getTypeName() + "] " + memo
                + (posted ? "(P)" : "");
    }
    
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Account} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_JOURNAL + ", " + FLD_DATE + ", " + FLD_TYPE 
                + ", " + FLD_MEMO + ", " + FLD_POSTED + ") VALUES(?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, journal);
            pst.setDate(2, Date.valueOf(txDate));
            pst.setLong(3, txCode);
            pst.setString(4, memo);
            pst.setBoolean(5, posted);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_JOURNAL + " = ?, " + FLD_DATE + " = ?, " + FLD_TYPE 
                + " = ?, " + FLD_MEMO + " = ?, " + FLD_POSTED 
                + " = ? WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, journal);
            pst.setDate(2, Date.valueOf(txDate));
            pst.setLong(3, txCode);
            pst.setString(4, memo);
            pst.setBoolean(5, posted);
            pst.setLong(6, txId);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        ResourceMap rm = context.getResourceMap(getClass());
        if (getJournal() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "journal"));
        }
        if (txDate == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "txDate"));
        } else if (!isValidDate(txDate)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", df.format(txDate),
                            rm.getString("invalid.value.reason")));
        }
        if (getTxCode() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "txCode"));
        }
    }
    
    private boolean isValidDate(LocalDate date) {
        LocalDate now = LocalDate.now();
        return date.isBefore(now) || date.equals(now);
    }

    private Long txId;
    private LocalDate txDate;
    private String memo;
    private boolean posted;
    private String journal;
    private long txCode;

}
