/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Unit.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import java.beans.PropertyChangeEvent;
import java.io.Serializable;
import java.sql.*;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@code Unit} class defines a single record in the database table named
 * {@code UNITS}. A unit is a vehicle, either self-powered or not, such as cars,
 * trucks, semis, and trailers.
 * <p>
 * Each unit is of a specified {@link UnitType type}, which describes its uses
 * and maintenance routines.
 * <p>
 * All properties of this class are bound. A good faith effort has been made to
 * notate this on each setter method's documentation.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Unit extends AbstractBean 
        implements Serializable, NtosPropertyKeys {
    
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} primary key identity field.
     */
    public static final String FLD_ID = "UNIT_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} Vehicle Identification Number (VIN) field.
     */
    public static final String FLD_VIN = "VIN";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} vehicle make field.
     */
    public static final String FLD_MAKE = "MAKE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} vehicle model field.
     */
    public static final String FLD_MODEL = "MODEL";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} model year field.
     */
    public static final String FLD_YEAR = "MODEL_YEAR";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} current odometer reading field.
     */
    public static final String FLD_ODOMETER = "ODOMETER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} purchase date field.
     */
    public static final String FLD_PURCH_DATE = "PURCHASE_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} purchase price field.
     */
    public static final String FLD_PURCH_PRICE = "PURCHASE_PRICE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} purchase odometer reading field.
     */
    public static final String FLD_PURCH_ODO = "PURCHASE_ODOMETER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} sales date field.
     */
    public static final String FLD_SALE_DATE = "SALES_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} sales price field.
     */
    public static final String FLD_SALE_PRICE = "SALES_PRICE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} sales odometer field.
     */
    public static final String FLD_SALE_ODO = "SALES_ODOMETER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} depreciation type field.
     */
    public static final String FLD_DEP = "DEPRECIATION";
    /**
     * Constant representing the table field name in the database for the
     * {@code Unit} {@link UnitType unit type} field.
     */
    public static final String FLD_TYPE = "UNIT_TYPE";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Unit.class);
    
    private static final String TABLE_NAME = "UNITS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     * 
     * @return a list of all records in the table or an empty list
     */
    public static List<Unit> getAllRecords() {
        return selectRecords(null, null);
    }
    
    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     * 
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     * 
     * @return an ordered list of all records, or an empty list
     */
    public static List<Unit> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }
    
    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param value the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static Unit findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        List<Unit> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null} 
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     * 
     * @param where the search criteria by which to find the desired record
     * 
     * @return the matching record if only one matches, the first matching 
     *         record if more than one record matches, {@code null} if no 
     *         records match
     */
    public static Unit findRecord(String where) {
        List<Unit> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     * 
     * @param where the search criteria on which to filter the records
     * 
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Unit> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the 
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it 
     * will have no effect on the ordering of the results and the records will 
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     * 
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be 
     *                ordered
     * 
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Unit> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }
    
    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to 
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     * 
     * @param id the primary key value of the record to be deleted
     * 
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }
        
        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, id);
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Unit.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s", 
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return true;
    }
    
    private static List<Unit> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }
        
        List<Unit> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Unit record = new Unit();
                
                record.unitNumber = rs.getString(1);
                record.vin = rs.getString(2);
                record.make = rs.getString(3);
                record.model = rs.getString(4);
                record.modelYear = rs.getInt(5);
                record.odometer = rs.getInt(6);
                record.purchaseDate = rs.getDate(7).toLocalDate();
                record.purchasePrice = rs.getDouble(8);
                record.purchaseOdometer = rs.getInt(9);
                if (rs.getDate(10) == null) {
                    record.salesDate = null;
                } else {
                    record.salesDate = rs.getDate(10).toLocalDate();
                }
                record.salesPrice = rs.getDouble(11);
                record.salesOdometer = rs.getInt(12);
                record.depreciation = rs.getLong(13);
                record.unitType = rs.getLong(14);
                
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Unit.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }

    /**
     * Constructs a new, empty {@code Unit}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     */
    public Unit () {
        
    }

    /**
     * Retrieves the unit number for this {@code Unit}.
     * 
     * @return the unit number
     */
    public String getUnitNumber() {
        return unitNumber;
    }

    /**
     * Sets the unit number for this {@code Unit}. This is a <strong>required
     * </strong> field and is the primary key in the table.
     * <p>
     * The data type of this field is a string, with a maximum length of twenty
     * (20) characters. A unit number is typically assigned by the company that
     * owns the unit.
     * <p>
     * If the specified {@code unitNumber} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code unitNumber} is not unique within the table, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param unitNumber the unit number
     * 
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setUnitNumber(String unitNumber) {
        if (unitNumber == null || unitNumber.isBlank() || unitNumber.isEmpty()) {
            throw new NullPointerException("unitNumber is blank, empty, or null.");
        } else if (!isUniqueInTable(unitNumber)) {
            throw new InvalidDataException(unitNumber + " has already been used.");
        }
        String old = getUnitNumber();
        this.unitNumber = unitNumber;
        firePropertyChange("unitNumber", old, getUnitNumber());
    }

    /**
     * Retrieves the vehicle identification number (VIN) for this {@code Unit}.
     * 
     * @return the VIN
     */
    public String getVin() {
        return vin;
    }

    /**
     * Sets the vehicle identification number (VIN) for this {@code Unit}. This
     * is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of thrity
     * (30) characters. This number is assigned by the vehicle manufacturer.
     * <p>
     * If the specified {@code vin} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param vin the VIN
     * 
     * @throws NullPointerException
     */
    public void setVin(String vin) {
        if (vin == null || vin.isBlank() || vin.isEmpty()) {
            throw new NullPointerException("vin is blank, empty, or null.");
        }
        String old = getVin();
        this.vin = vin;
        firePropertyChange("vin", old, getVin());
    }

    /**
     * Retrieves the make of this {@code Unit}.
     * 
     * @return the vehicle make
     */
    public String getMake() {
        return make;
    }

    /**
     * Sets the make of this {@code Unit}. This is a <strong>required</strong>
     * field.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters.
     * <p>
     * If the specified {@code make} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param make the vehicle make
     * 
     * @throws NullPointerException
     */
    public void setMake(String make) {
        if (make == null || make.isBlank() || make.isEmpty()) {
            throw new NullPointerException("make is blank, empty, or null.");
        }
        String old = getMake();
        this.make = make;
        firePropertyChange("make", old, getMake());
    }

    /**
     * Retrieves the model of this {@code Unit}.
     * 
     * @return the vehicle model
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets the model of the {@code Unit}. This is a <strong>required</strong>
     * field.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters.
     * <p>
     * If the specified {@code model} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param model the vehicle model
     * 
     * @throws NullPointerException
     */
    public void setModel(String model) {
        if (model == null || model.isBlank() || model.isEmpty()) {
            throw new NullPointerException("model is blank, empty, or null.");
        }
        String old = getModel();
        this.model = model;
        firePropertyChange("model", old, getModel());
    }

    /**
     * Retrieves the manufacturing year of this {@code Unit}.
     * 
     * @return the model year
     */
    public int getModelYear() {
        return modelYear;
    }

    /**
     * Sets the manufacturing year of this {@code Unit}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is an integer, which must be greater than 
     * 1900.
     * <p>
     * If the specified {@code modelYear} is less than 1900, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound field.
     * 
     * @param modelYear the model year
     * 
     * @throws InvalidDataException
     */
    public void setModelYear(int modelYear) {
        if (modelYear < 1900) {
            throw new InvalidDataException(modelYear + " is less than 1900.");
        }
        int old = getModelYear();
        this.modelYear = modelYear;
        firePropertyChange("modelYear", old, getModelYear());
    }
    
    /**
     * Retrieves the odometer reading for this {@code Unit}.
     * 
     * @return the current odometer reading
     */
    public int getOdometer() {
        return odometer;
    }
    
    /**
     * Sets the odometer reading for this {@code Unit}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is an integer. The basic requirements are 
     * that this value must be greater than zero for a new record, or greater
     * than the last odometer reading for an existing record.
     * <p>
     * If the specified {@code odometer} is less than zero, or less than the
     * previous setting, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * <dl><dt>Property Note:</dt>
     * <dd>When the odometer reading is set, the {@code salesOdometer} property
     * is updated in such a manner as to not fire a {@link PropertyChangeEvent}
     * on that property. This is done as a convenience to the user, so that when
     * they sell the unit, they do not need to manually set the sales odometer
     * reading.</dd></dl>
     * 
     * @param odometer the current odometer reading
     * 
     * @throws InvalidDataException
     */
    public void setOdometer(int odometer) {
        if (odometer <= 0 || odometer <= getOdometer()) {
            throw new InvalidDataException(odometer + " is not valid. It must "
                    + "be either greater than zero for a new record, or greater "
                    + "that the prior odometer reading for existing records.");
        }
        int old = getOdometer();
        this.odometer = odometer;
        
        // Upate the value of the salesOdometer as an automation setting and
        //+ convenience to the user.
        salesOdometer = odometer;
        firePropertyChange("odometer", old, getOdometer());
    }

    /**
     * Retrieves the date the {@code Unit} was purchased.
     * 
     * @return the purchase date
     */
    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * Sets the date the {@code Unit} was purchased. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a date.
     * <p>
     * If the specified {@code purchaseDate} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code purchaseDate} is after the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param purchaseDate the purchase date
     * 
     * @throws NullPointerException
     * @throws InvalidDataException
     */
    public void setPurchaseDate(LocalDate purchaseDate) {
        if (purchaseDate == null) {
            throw new NullPointerException("purchaseDate is null.");
        } else if (!purchaseDate.isBefore(LocalDate.now())
                || !purchaseDate.equals(LocalDate.now())) {
            throw new InvalidDataException(purchaseDate + " is after the "
                    + "current date.");
        }
        LocalDate old = getPurchaseDate();
        this.purchaseDate = purchaseDate;
        firePropertyChange("purchaseDate", old, getPurchaseDate());
    }

    /**
     * Retrieves the purchase price amount for this {@code Unit}.
     * 
     * @return the purchase price
     */
    public double getPurchasePrice() {
        return purchasePrice;
    }

    /**
     * Sets the purchase price amount for this {@code Unit}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a double. The value must be greater than
     * zero.
     * <p>
     * If the specified {@code purchasePrice} is less than zero, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param purchasePrice 
     */
    public void setPurchasePrice(double purchasePrice) {
        if (purchasePrice < 0) {
            throw new InvalidDataException(purchasePrice + " is less than zero.");
        }
        double old = getPurchasePrice();
        this.purchasePrice = purchasePrice;
        firePropertyChange("purchasePrice", old, getPurchasePrice());
    }

    /**
     * Retrieves the odometer reading at the time of purchase or delivery of 
     * this {@code Unit}.
     * 
     * @return the purchase odometer reading
     */
    public int getPurchaseOdometer() {
        return purchaseOdometer;
    }

    /**
     * Sets the odometer reading a the time of purchase or delivery of this
     * {@code Unit}. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is an integer. The value must be greater than
     * or equal to zero.
     * <p>
     * If the specified {@code purchaseOdometer} is less than zero, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param purchaseOdometer the purchase odometer reading
     * 
     * @throws InvalidDataException
     */
    public void setPurchaseOdometer(int purchaseOdometer) {
        if (purchaseOdometer < 0) {
            throw new InvalidDataException("The purchase odometer reading is "
                    + "less than zero.");
        }
        int old = getPurchaseOdometer();
        this.purchaseOdometer = purchaseOdometer;
        firePropertyChange("purchaseOdometer", old, getPurchaseOdometer());
    }

    /**
     * Retrieves the date on which this {@code Unit} was sold, or {@code null}
     * if the unit is still owned by the company.
     * 
     * @return the sales date or {@code null}
     */
    public LocalDate getSalesDate() {
        return salesDate;
    }

    /**
     * Sets the date on which this {@code Unit} was sold. This is an <em>
     * optional</em> field.
     * <p>
     * The data type of this field is a date, and it accepts {@code null} 
     * values. If the value is specified, it must be later than the purchase
     * date, and on or before the current date.
     * <p>
     * If the specified {@code salesDate} <strong>is not</strong> {@code null},
     * and is prior to the purchase date, or after the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param salesDate the sales date, or {@code null} is allowed
     */
    public void setSalesDate(LocalDate salesDate) {
        if (salesDate != null && !isValidSalesDate(salesDate)) {
            throw new InvalidDataException("salesDate cannot be prior to the "
                    + "purchase date, nor after the current date.");
        }
        LocalDate old = getSalesDate();
        this.salesDate = salesDate;
        firePropertyChange("salesLocalDate", old, getSalesDate());
    }

    /**
     * Retrieves the price for which this {@code Unit} was sold, or zero if it
     * has not yet been sold.
     * 
     * @return the sales price amount
     */
    public double getSalesPrice() {
        return salesPrice;
    }

    /**
     * Sets the price for which this {@code Unit} was sold. This is an <em>
     * optional</em> field.
     * <p>
     * The data type for this field is a double, with a default value of zero.
     * <p>
     * If this field is set, the specified {@code salesPrice} <em>must be</em>
     * greater than zero. If not, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param salesPrice the price paid
     * 
     * @throws InvalidDataException
     */
    public void setSalesPrice(double salesPrice) {
        if (salesPrice <= 0.00d) {
            throw new InvalidDataException("salesPrice must be greater than "
                    + "zero.");
        }
        double old = getSalesPrice();
        this.salesPrice = salesPrice;
        firePropertyChange("salesPrice", old, getSalesPrice());
    }

    /**
     * Retrieves the odometer reading at the time this {@code Unit} was sold.
     * <dl><dt>Property Note:</dt>
     * <dd>This property's value is updated each time the current odometer
     * reading property is updated. This is done as a convenience so that this
     * property <em>may not</em> need to be set when the unit is sold. However,
     * it is a <em>best practice</em> to verify this property's value at the 
     * time of the sale, as it may be slightly behind the current odometer
     * reading depending upon when the last service was performed, the last fuel
     * was purchased, or the last load was completed.</dd></dl>
     * 
     * @return the odometer reading at the time of sale
     */
    public Integer getSalesOdometer() {
        return salesOdometer;
    }

    /**
     * Sets the odometer reading at the time this {@code Unit} is sold. This is
     * an <em>optional</em> value, until the vehicle is sold.
     * <p>
     * The data type of this field is an integer, with a default value of zero.
     * <p>
     * If this value is set, the the specified {@code salesOdometer} must be
     * greater than the {@link #getPurchaseOdometer() purchase odometer}. If it
     * is not, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     * <dl><dt>Property Note:</dt>
     * <dd>This property's value is updated each time the current odometer
     * reading property is updated. This is done as a convenience so that this
     * property <em>may not</em> need to be set when the unit is sold. However,
     * it is a <em>best practice</em> to verify this property's value at the 
     * time of the sale, as it may be slightly behind the current odometer
     * reading depending upon when the last service was performed, the last fuel
     * was purchased, or the last load was completed.</dd></dl>
     * 
     * @param salesOdometer odometer reading at the time of sale
     * 
     * @throws InvalidDataException
     */
    public void setSalesOdometer(Integer salesOdometer) {
        if (salesOdometer != null && !isValidSalesOdometer(salesOdometer)) {
            throw new InvalidDataException("salesOdometer must be greater than "
                    + "the odometer reading at time of purchase.");
        }
        int old = getSalesOdometer();
        this.salesOdometer = salesOdometer;
        firePropertyChange("salesOdometer", old, getSalesOdometer());
    }

    /**
     * Retrieves the {@link DepreciationType type of depreciation} used for tax
     * purposes with this {@code Unit}.
     * 
     * @return the depreciation type
     */
    public DepreciationType getDepreciation() {
        return DepreciationType.findByPrimaryKey(depreciation);
    }

    /**
     * Sets the {@link DepreciationType type of depreciation} used for tax
     * purposes with this {@code Unit}. This is a <strong>required</strong>
     * field.
     * <p>
     * If the specified {@code depreciation} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param depreciation the depreciation to use
     * 
     * @throws NullPointerException
     */
    public void setDepreciation(DepreciationType depreciation) {
        if (depreciation == null) {
            throw new NullPointerException("depreciation is null.");
        }
        DepreciationType old = getDepreciation();
        this.depreciation = depreciation.getTypeId();
        firePropertyChange("depreciation", old, getDepreciation());
    }

    /**
     * Retrieves the {@link UnitType type} of this {@code Unit}.
     * 
     * @return the unit type
     */
    public UnitType getUnitType() {
        return UnitType.findByPrimaryKey(unitType);
    }

    /**
     * Sets the {@link UnitType type} of this {@code Unit}. This is a <strong>
     * required</strong> field.
     * <p>
     * If the specified {@code unitType} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param unitType the unit type
     * 
     * @throws NullPointerException
     */
    public void setUnitType(UnitType unitType) {
        if (unitType == null) {
            throw new NullPointerException("unitType is null.");
        }
        UnitType old = getUnitType();
        this.unitType = unitType.getTypeId();
        firePropertyChange("unitType", old, getUnitType());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (unitNumber != null ? unitNumber.hashCode() : 0);
        hash += (vin != null ? vin.hashCode() : 0);
        hash += (make != null ? make.hashCode() : 0);
        hash += (model != null ? model.hashCode() : 0);
        hash += odometer;
        hash += (purchaseDate != null ? purchaseDate.hashCode() : 0);
        hash += ((Double) purchasePrice).intValue();
        hash += purchaseOdometer;
        hash += (salesDate != null ? salesDate.hashCode() : 0);
        hash += ((Double) salesPrice).intValue();
        hash += salesOdometer;
        hash += (getDepreciation() != null ? getDepreciation().hashCode() : 0);
        hash += (getUnitType() != null ? getUnitType().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Unit)) {
            return false;
        }
        Unit other = (Unit) object;
        if ((this.unitNumber == null && other.unitNumber != null) ||
                (this.unitNumber != null &&
                !this.unitNumber.equals(other.unitNumber))) {
            return false;
        }
        if ((vin == null && other.vin != null)
                || (this.vin != null && !this.vin.equals(other.vin))) {
            return false;
        }
        if ((this.make == null && other.make != null)
                || (this.make != null && !this.make.equals(other.make))) {
            return false;
        }
        if ((this.model == null && other.model != null)
                || (this.model != null && !this.model.equals(other.model))) {
            return false;
        }
        if (this.modelYear != other.modelYear) {
            return false;
        }
        if (this.odometer != other.odometer) {
            return false;
        }
        if ((this.purchaseDate == null && other.purchaseDate != null)
                || (this.purchaseDate != null
                && !this.purchaseDate.equals(other.purchaseDate))) {
            return false;
        }
        if (this.purchasePrice != other.purchasePrice) {
            return false;
        }
        if (this.purchaseOdometer != other.purchaseOdometer) {
            return false;
        }
        if ((this.salesDate == null && other.salesDate != null)
                || (this.salesDate != null 
                && !this.salesDate.equals(other.salesDate))) {
            return false;
        }
        if (this.salesPrice != other.salesPrice) {
            return false;
        }
        if (this.salesOdometer != other.salesOdometer) {
            return false;
        }
        if ((this.getDepreciation() == null && other.getDepreciation() != null)
                || (this.getDepreciation() != null
                && !this.getDepreciation().equals(other.getDepreciation()))) {
            return false;
        }
        return ((this.getUnitType() == null && other.getUnitType() != null)
                || (this.getUnitType() != null 
                && !this.getUnitType().equals(other.getUnitType())));
    }

    @Override
    public String toString() {
        ResourceMap rm = context.getResourceMap(getClass());
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        NumberFormat cf = NumberFormat.getCurrencyInstance();
        String value = rm.getString("toString", unitNumber, modelYear, make, 
                model, odometer);
        if (salesDate != null) {
            value = rm.getString("toString.sold", unitNumber, modelYear, make,
                    model, salesOdometer, df.format(salesDate), 
                    cf.format(salesPrice));
        }
        return value;
    }
    
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
      
    /**
     * Saves this {@code Unit} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() accountName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_VIN + ", " + FLD_MAKE + ",  " 
                + FLD_MODEL + ", " + FLD_ODOMETER + ", " + FLD_PURCH_DATE
                + ", " + FLD_PURCH_PRICE + ", " + FLD_PURCH_ODO + ", "
                + FLD_SALE_DATE + ", " + FLD_SALE_PRICE + ", " 
                + FLD_SALE_ODO + ", " + FLD_DEP + ", " + FLD_TYPE 
                + ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, unitNumber);
            pst.setString(2, vin);
            pst.setString(3, make);
            pst.setString(4, model);
            pst.setInt(5, modelYear);
            pst.setInt(6, odometer);
            pst.setDate(7, Date.valueOf(purchaseDate));
            pst.setDouble(8, purchasePrice);
            pst.setInt(9, purchaseOdometer);
            if (salesDate != null) {
                pst.setDate(10, Date.valueOf(salesDate));
            } else {
                pst.setDate(10, null);
            }
            pst.setDouble(11, salesPrice);
            pst.setInt(12, salesOdometer);
            pst.setLong(13, depreciation);
            pst.setLong(14, unitType);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Unit.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_VIN + " = ?, " + FLD_MAKE + ",  " 
                + FLD_MODEL + " = ?, " + FLD_ODOMETER + " = ?, " + FLD_PURCH_DATE
                + " = ?, " + FLD_PURCH_PRICE + " = ?, " + FLD_PURCH_ODO + " = ?, "
                + FLD_SALE_DATE + " = ?, " + FLD_SALE_PRICE + " = ?, " 
                + FLD_SALE_ODO + " = ?, " + FLD_DEP + " = ?, " + FLD_TYPE + " = ? WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, vin);
            pst.setString(2, make);
            pst.setString(3, model);
            pst.setInt(4, modelYear);
            pst.setInt(5, odometer);
            pst.setDate(6, Date.valueOf(purchaseDate));
            pst.setDouble(7, purchasePrice);
            pst.setInt(8, purchaseOdometer);
            if (salesDate != null) {
                pst.setDate(9, Date.valueOf(salesDate));
            } else {
                pst.setDate(9, null);
            }
            pst.setDouble(10, salesPrice);
            pst.setInt(11, salesOdometer);
            pst.setLong(12, depreciation);
            pst.setLong(13, unitType);
            pst.setString(14, unitNumber);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Unit.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        ResourceMap rm = context.getResourceMap(getClass());
        LocalDate now = LocalDate.now();
        if (unitNumber == null || unitNumber.isBlank() || unitNumber.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "unitNumber"));
        }
        if (vin == null || vin.isBlank() || vin.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "vin"));
        }
        if (make == null || make.isBlank() || make.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "make"));
        }
        if (model == null || model.isBlank() || model.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "model"));
        }
        if (odometer < 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", odometer,
                            rm.getString("invalid.odometer.reason")));
        }
        if (purchaseDate == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "purchaseDate"));
        } else if (!purchaseDate.isBefore(now) || purchaseDate.isAfter(now)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", purchaseDate,
                            rm.getString("invalid.purchaseDate.reason")));
        }
        if (purchasePrice < 0d) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", purchasePrice,
                            rm.getString("invalid.purchasePrice.reason")));
        }
        if (purchaseOdometer < 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", 
                            purchaseOdometer,
                            rm.getString("invalid.purchaseOdometer.reason")));
        }
        if (salesDate != null && !isValidSalesDate(salesDate)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", salesDate,
                            rm.getString("invalid.salesDate.reason")));
        }
        if (salesPrice < 0d) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", salesPrice,
                            rm.getString("invalid.salesPrice.reason")));
        }
        if (salesOdometer < purchaseOdometer) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", salesOdometer,
                            rm.getString("invalid.salesOdometer.reason")));
        }
        if (getDepreciation() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "depreciation"));
        }
        if (getUnitType() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "unitType"));
        }
    }
  
    private boolean isUniqueInTable(String value) {
        return findByPrimaryKey(value) == null;
    }
    
    private boolean isValidSalesDate(LocalDate date) {
        return date.isAfter(purchaseDate) && (!date.isAfter(LocalDate.now()));
    }
    
    private boolean isValidSalesOdometer(int odometer) {
        return odometer > getPurchaseOdometer();
    }

    private String unitNumber = null;
    private String vin = null;
    private String make = null;
    private String model = null;
    private int odometer = -1;
    private int modelYear = 1900;
    private LocalDate purchaseDate = null;
    private double purchasePrice = 0.00d;
    private int purchaseOdometer = 0;
    private LocalDate salesDate = null;
    private double salesPrice = 0.00d;
    private int salesOdometer = 0;
    private long depreciation = -1L;
    private long unitType = -1L;

}
