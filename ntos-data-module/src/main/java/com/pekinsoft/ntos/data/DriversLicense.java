/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-data-module
 *  Class      :   DriversLicense.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 18, 2024
 *  Modified   :   Oct 18, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 18, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer;
import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import static com.pekinsoft.utils.PropertyKeys.KEY_ADD_NOTIFICATION;
import static com.pekinsoft.utils.PropertyKeys.KEY_STATUS_MESSAGE;
import java.io.Serializable;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class DriversLicense extends AbstractBean
        implements Serializable, NtosPropertyKeys{
    
    /**
     * Constant representing the table field name in the database for the
     * {@code DriversLicense} primary key identity field.
     */
    public static final String FLD_ID = "LICENSE_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code DriversLicense} {@link Employee} holder.
     */
    public static final String FLD_EMPLOYEE = "EMPLOYEE_ID";
    public static final String FLD_CLASS = "DL_CLASS";
    public static final String FLD_ISSUE = "DL_ISSUE";
    public static final String FLD_EXPIRES = "DL_EXPIRATION";
    public static final String FLD_RESTICT = "DL_RESTRICTIONS";
    public static final String FLD_ENDORSE = "DL_ENDORSEMENTS";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Account.class);
    
    private static final String TABLE_NAME = "DRIVERS_LICENSES";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";
    
    /**
     * Retrieves the record identified by the specified {@code number} in its
     * primary key field.
     * <p>
     * If no record's primary key number matches the number specified, then 
     * {@code null} is returned.
     * 
     * @param number the number to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static DriversLicense findByPrimaryKey(String number) {
        if (number == null || number.isBlank() || number.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + number;
        List<DriversLicense> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves the record associated with the specified {@link Employee}.
     * <p>
     * If no record's have an employee that matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param employee the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static DriversLicense findByEmployee(Employee employee) {
        if (employee == null) {
            return null;
        }
        String where = FLD_EMPLOYEE + " = " + employee.getEmployeeId();
        List<DriversLicense> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }
        
        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, id);
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s", 
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return true;
    }
    
    private static List<DriversLicense> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }
        
        List<DriversLicense> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                DriversLicense record = new DriversLicense();
                record.number = rs.getString(1);
                record.licenseClass = rs.getString(2).toCharArray()[0];
                record.issued = rs.getDate(3).toLocalDate();
                record.expires = rs.getDate(4).toLocalDate();
                record.restrictions = rs.getString(5);
                record.endorsements = rs.getString(6);
                record.employee = rs.getString(7);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, StatusDisplayer.MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", DriversLicense.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }
    
    public DriversLicense () {
        
    }
    
    /**
     * Retrieves the driver's license number.
     * 
     * @return the number
     */
    public String getNumber() {
        return number;
    }
    
    /**
     * Sets the driver's license number. This is a <strong>required</strong>
     * field.
     * <p>
     * The data type for this field is a string, with a maximum length of twenty
     * (20) characters. This number is issued by a governmental agency and 
     * printed on the driver's license card.
     * <p>
     * If the specified {@code number} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param number the driver's license number
     * 
     * @throws NullPointerException if {@code number} is blank, empty, or 
     *                              {@code null}
     */
    public void setNumber(String number) {
        if (number == null || number.isBlank() || number.isEmpty()) {
            throw new NullPointerException("number is blank, empty, or null.");
        }
        String old = getNumber();
        this.number = number.substring(0, 20);
        firePropertyChange("number", old, getNumber());
    }
    
    /**
     * Retrieves the class of the driver's license.
     * 
     * @return the license class
     */
    public char getLicenseClass() {
        return licenseClass;
    }
    
    /**
     * Sets the class of the driver's license. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a single character. Driver's licenses are
     * broken down into varying classes that determine the type of vehicle(s) a
     * driver may operate. These classes are designated by a single letter.
     * <p>
     * If the specified {@code licenseClass} is not alphabetic, an
     * {@link InvalidDataException} is thrown. Case does not matter, as the
     * character will be converted to upper case before it is stored.
     * <p>
     * This is a bound property.
     * 
     * @param licenseClass the class of the license
     * 
     * @throws InvalidDataException if the specified character is non-alphabetic
     */
    public void setLicenseClass(char licenseClass) {
        if (licenseClass == '\0' || !Character.isAlphabetic(licenseClass)) {
            throw new InvalidDataException(licenseClass + " is not a letter.");
        }
        char old = getLicenseClass();
        this.licenseClass = Character.toUpperCase(licenseClass);
        firePropertyChange("licenseClass", old, getLicenseClass());
    }
    
    /**
     * Retrieves the issue date of the driver's license.
     * 
     * @return the issue date
     */
    public LocalDate getIssued() {
        return issued;
    }
    
    /**
     * Sets the issue date of the driver's license. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a date.
     * <p>
     * If the specified {@code issued} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param issued the date the license was issued
     * 
     * @throws NullPointerException if {@code issued} is {@code null}
     */
    public void setIssued(LocalDate issued) {
        if (issued == null) {
            throw new NullPointerException("issued is null.");
        }
        LocalDate old = getIssued();
        this.issued = issued;
        firePropertyChange("issued", old, getIssued());
    }
    
    /**
     * Retrieves the expiration date of the driver's license.
     * 
     * @return the expiration date
     */
    public LocalDate getExpires() {
        return expires;
    }
    
    /**
     * Sets the expiration date of the driver's license. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a date.
     * <p>
     * If the specified {@code expires} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param expires the expiration date
     * 
     * @throws NullPointerException if {@code expires} is {@code null}
     */
    public void setExpires(LocalDate expires) {
        if (expires == null) {
            throw new NullPointerException("expires is null.");
        }
        LocalDate old = getExpires();
        this.expires = expires;
        firePropertyChange("expires", old, getExpires());
    }
    
    /**
     * Retrieves the restrictions on the driver's license, if any.
     * 
     * @return the restrictions or {@code null}
     */
    public String getRestrictions() {
        return restrictions;
    }
    
    /**
     * Sets the restrictions on the driver's license. This is an <em>optional
     * </em> field, as not all drivers have restrictions.
     * <p>
     * The data type of this field is a string, with a maximum length of fifteen
     * (15) characters.
     * <p>
     * This is a bound property.
     * 
     * @param restrictions the restrictions on this license
     */
    public void setRestrictions(String restrictions) {
        String old = getRestrictions();
        this.restrictions = restrictions;
        firePropertyChange("restrictions", old, getRestrictions());
    }
    
    /**
     * Retrieves the endorsements on the driver's license, if any.
     * 
     * @return the endorsements or {@code null}
     */
    public String getEndorsements() {
        return endorsements;
    }
    
    /**
     * Sets the endorsements on the driver's license. This is an <em>optional
     * </em> field, as not all drivers have endorsements.
     * <p>
     * The data type of this field is a string, with a maximum length of fifteen
     * (15) characters.
     * <p>
     * This is a bound property.
     * 
     * @param endorsements the endorsements on this license
     */
    public void setEndorsements(String endorsements) {
        String old = getEndorsements();
        this.endorsements = endorsements;
        firePropertyChange("endorsements", old, getEndorsements());
    }
    
    /**
     * Retrieves the {@link Employee} to whom this driver's license belongs.
     * 
     * @return the licensed employee
     */
    public Employee getEmployee() {
        return Employee.findByPrimaryKey(employee);
    }
    
    /**
     * Sets the {@link Employee} to whom this drivers license belongs. This is a
     * <strong>required</strong>field.
     * <p>
     * If the specified {@code employee} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param employee the licensed employee
     * 
     * @throws NullPointerException if {@code employee} is {@code null}
     */
    public void setEmployee(Employee employee) {
        if (employee == null) {
            throw new NullPointerException("employee is null.");
        }
        Employee old = getEmployee();
        this.employee = employee.getEmployeeId();
        firePropertyChange("employee", old, getEmployee());
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (number != null ? number.hashCode() ^ 5 : 0);
        hash += (licenseClass != '\0' ? Character.hashCode(licenseClass) ^ 5 : 0);
        hash += (issued != null ? issued.hashCode() ^ 5 : 0);
        hash += (expires != null ? expires.hashCode() ^ 5 : 0);
        hash += (restrictions != null ? restrictions.hashCode() ^ 5 : 0);
        hash += (endorsements != null ? endorsements.hashCode() ^ 5 : 0);
        hash += (getEmployee() != null ? getEmployee().hashCode() ^ 5 : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof DriversLicense)) {
            return false;
        }
        DriversLicense other = (DriversLicense) object;
        if ((this.number == null && other.number != null) 
                || (this.number != null && !this.number.equals(other.number))) {
            return false;
        }
        if ((this.licenseClass != other.licenseClass)) {
            return false;
        }
        if ((this.issued == null && other.issued != null)
                || (this.issued != null && !this.issued.equals(other.issued))) {
            return false;
        }
        if ((this.expires == null && other.expires != null)
                || (this.expires != null && !this.expires.equals(other.expires))) {
            return false;
        }
        if ((this.restrictions == null && other.restrictions != null)
                || (this.restrictions != null 
                && !this.restrictions.equals(other.restrictions))) {
            return false;
        }
        if ((this.getEmployee() == null && other.getEmployee() != null)
                || this.getEmployee() != null
                && !this.getEmployee().equals(other.getEmployee())) {
            return false;
        }
        
        return ((this.endorsements == null && other.endorsements != null)
                || (this.endorsements != null 
                && !this.endorsements.equals(other.endorsements)));
    }
    
    @Override
    public String toString() {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-mm-dd");
        return number + ": " + getEmployee().getEmployeeId() + " [Expires: "
                + fmt.format(expires) + "] (END: " + endorsements + ", REST: " 
                + restrictions + ")";
    }
    
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code DriversLicense} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getNumber() number} value is unique in the table for new records. 
     * If the record passes validation, it will be inserted or updated in the 
     * table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            isUniqueNumber();
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_CLASS + ", " + FLD_ISSUE + ",  " 
                + FLD_EXPIRES + ", " + FLD_RESTICT + ", " + FLD_ENDORSE + ", "
                + FLD_EMPLOYEE + ") VALUES(?, ?, ?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, number);
            pst.setString(2, String.valueOf(licenseClass));
            pst.setDate(3, java.sql.Date.valueOf(issued));
            pst.setDate(4, java.sql.Date.valueOf(expires));
            pst.setString(5, restrictions);
            pst.setString(6, endorsements);
            pst.setString(7, employee);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_CLASS + " = ?, " + FLD_ISSUE + ",  " 
                + FLD_EXPIRES + " = ?, " + FLD_RESTICT + " = ?, " 
                + FLD_ENDORSE + " = ?, " + FLD_EMPLOYEE + " = ? WHERE " 
                + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, String.valueOf(licenseClass));
            pst.setDate(2, java.sql.Date.valueOf(issued));
            pst.setDate(3, java.sql.Date.valueOf(expires));
            pst.setString(4, restrictions);
            pst.setString(5, endorsements);
            pst.setString(6, employee);
            pst.setString(7, number);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Account.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        if (number == null || number.isBlank() || number.isBlank()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "number"));
        }
        if (issued == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "issued"));
        }
        if (expires == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "expires"));
        }
        if (issued.isAfter(expires)) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", issued, "is a later "
                            + "date than the expiration (" + expires + ")"));
        }
        if (getEmployee() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "employee"));
        }
    }
    
    private void isUniqueNumber() throws ValidationException {
        DriversLicense dl = DriversLicense.findByPrimaryKey(number);
        if (dl != null) {
            throw new ValidationException(this, 
                    resourceMap.getString("not.unique", number));
        }
    }
    
    private String number = null;
    private char licenseClass = '\0';
    private LocalDate issued = null;
    private LocalDate expires = null;
    private String restrictions = null;
    private String endorsements = null;
    private String employee = null;

}
