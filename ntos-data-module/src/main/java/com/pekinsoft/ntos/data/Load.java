/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Load.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer;
import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.common.NtosPropertyKeys;
import com.pekinsoft.ntos.data.support.InvalidDataException;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import static com.pekinsoft.utils.PropertyKeys.KEY_ADD_NOTIFICATION;
import static com.pekinsoft.utils.PropertyKeys.KEY_STATUS_MESSAGE;
import java.io.Serializable;
import java.sql.*;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Load extends AbstractBean
        implements Serializable, NtosPropertyKeys {

    /**
     * Constant representing the table field name in the database for the
     * {@code Load} order number (primary key) field.
     */
    public static final String FLD_ID = "ORDER_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} trip number field.
     */
    public static final String FLD_TRIP = "TRIP_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} dispatched date field.
     */
    public static final String FLD_DISP_DATE = "DISPATCH_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} dispatched distance field.
     */
    public static final String FLD_DISP_DIST = "DISPATCH_DISTANCE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} deadhead distance field.
     */
    public static final String FLD_DEDHED = "DEADHEAD_DISTANCE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} gross revenue field.
     */
    public static final String FLD_REVENUE = "GROSS_REVENUE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} piece count field.
     */
    public static final String FLD_PCS = "PIECES";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} freight weight field.
     */
    public static final String FLD_WGT = "WEIGHT";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} commodity field.
     */
    public static final String FLD_COMMODITY = "COMMODITY";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} Bill of Lading (BOL) field.
     */
    public static final String FLD_BOL = "BOL";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} reference number field.
     */
    public static final String FLD_REF = "REFERENCE_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} pickup number field.
     */
    public static final String FLD_PICKUP = "PICKUP_NUMBER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} tarped field.
     */
    public static final String FLD_TARPED = "TARPED";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} tarp height field.
     */
    public static final String FLD_TARP_HGT = "TARP_HEIGHT";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} hazardous materials (HazMat) field.
     */
    public static final String FLD_HAZMAT = "HAZ_MAT";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} team required field.
     */
    public static final String FLD_TEAM = "TEAM";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} Transportation Workers' Identification Credential (TWIC)
     * field.
     */
    public static final String FLD_TWIC = "TWIC";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} Count by Driver (CBD) field.
     */
    public static final String FLD_CBD = "CBD";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} top customer field.
     */
    public static final String FLD_TOP = "TOP_CUSTOMER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} Less Than Truckload (LTL) field.
     */
    public static final String FLD_LTL = "LTL";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} ramps required field.
     */
    public static final String FLD_RAMPS = "RAMPS";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} over-dimensional (OD) freight field.
     */
    public static final String FLD_OD = "OVERSIZED";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} freight height field.
     */
    public static final String FLD_HGT = "HEIGHT";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} freight width field.
     */
    public static final String FLD_WD = "WIDTH";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} notes field.
     */
    public static final String FLD_NOTES = "LOAD_NOTES";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} booked date field.
     */
    public static final String FLD_BKD_DATE = "BOOKED_DATE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} broker or agent field.
     */
    public static final String FLD_BRKR = "BROKER";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} dispatched by field.
     */
    public static final String FLD_DISP_BY = "DISPATCHED_BY";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} assigned to unit field.
     */
    public static final String FLD_ASSIGNED = "ASSIGNED_UNIT";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} completed (all stops made) field.
     */
    public static final String FLD_COMP = "COMPLETED";
    /**
     * Constant representing the table field name in the database for the
     * {@code Load} settled (payment received) field.
     */
    public static final String FLD_SETT = "SETTLED";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Load.class);

    private static final String TABLE_NAME = "LOADS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE "
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     *
     * @return a list of all records in the table or an empty list
     */
    public static List<Load> getAllRecords() {
        return selectRecords(null, null);
    }

    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     *
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     *
     * @return an ordered list of all records, or an empty list
     */
    public static List<Load> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }

    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then
     * {@code null} is returned.
     *
     * @param value the value to find
     *
     * @return the matching record, or {@code null} if no match
     */
    public static Load findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        List<Load> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null}
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     *
     * @param where the search criteria by which to find the desired record
     *
     * @return the matching record if only one matches, the first matching
     *         record if more than one record matches, {@code null} if no
     *         records match
     */
    public static Load findRecord(String where) {
        List<Load> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }

    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     *
     * @param where the search criteria on which to filter the records
     *
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Load> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it
     * will have no effect on the ordering of the results and the records will
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     *
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be
     *                ordered
     *
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Load> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }

    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param id the primary key value of the record to be deleted
     *
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }

        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);

        Connection conn = null;
        PreparedStatement pst = null;

        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);

            pst = conn.prepareStatement(sql);

            pst.setString(1, id);

            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Load.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s",
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return true;
    }

    private static List<Load> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        List<Load> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Load l = new Load();

                l.orderNumber = rs.getString(1);
                l.tripNumber = rs.getString(2);
                l.dispatchDate = rs.getDate(3).toLocalDate();
                l.dispatchDistance = rs.getInt(4);
                l.deadheadDistance = rs.getInt(5);
                l.grossRevenue = rs.getDouble(6);
                l.pieces = rs.getInt(7);
                l.weight = rs.getInt(8);
                l.commodity = rs.getString(9);
                l.bol = rs.getString(10);
                l.referenceNumber = rs.getString(11);
                l.pickupNumber = rs.getString(12);
                l.tarped = rs.getBoolean(13);
                l.tarpHeight = rs.getInt(14);
                l.hazMat = rs.getBoolean(15);
                l.team = rs.getBoolean(16);
                l.twic = rs.getBoolean(17);
                l.cbd = rs.getBoolean(18);
                l.topCustomer = rs.getBoolean(19);
                l.ltl = rs.getBoolean(20);
                l.ramps = rs.getBoolean(21);
                l.oversized = rs.getBoolean(22);
                l.height = rs.getInt(23);
                l.width = rs.getInt(24);
                l.loadNotes = rs.getString(25);
                l.broker = rs.getString(26);
                l.dispatchedBy = rs.getString(27);
                l.assignedTo = rs.getString(28);
                l.completed = rs.getBoolean(29);
                l.settled = rs.getBoolean(30);

                records.add(l);
            }

            rs.close();
            stmt.close();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);

            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg,
                    StatusDisplayer.MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);

            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Load.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));

            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);

            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }

        return records;
    }

    /**
     * Constructs a new, empty {@code Load}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     */
    public Load() {

    }

    /**
     * Retrieves the order number for this {@code Load}.
     *
     * @return the order number
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the order number for this {@code Load}. This is a <strong>required
     * </strong> field and is the <em>primary key</em> in the table.
     * <p>
     * This data type for this field is a string, with a maximum length of
     * thirty (30) characters. This field does not allow {@code null} values nor
     * empty strings.
     * <p>
     * If the specified {@code orderNumber} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code orderNumber} is not unique in the table, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param orderNumber the order number
     *
     * @throws NullPointerException if {@code orderNumber} is blank, empty, or
     *                              {@code null}
     * @throws InvalidDataException if {@code orderNumber} is not unique in the
     *                              table
     */
    public void setOrderNumber(String orderNumber) {
        if (orderNumber == null || orderNumber.isBlank() || orderNumber
                .isEmpty()) {
            throw new NullPointerException(
                    "orderNumber is blank, empty, or null.");
        } else if (!isOrderNumberUniqueInTable(orderNumber)) {
            throw new InvalidDataException(orderNumber + " has already been "
                    + "used for a Load.");
        }
        String old = getOrderNumber();
        this.orderNumber = orderNumber.substring(0, 30);
        firePropertyChange("orderNumber", old, getOrderNumber());
    }

    /**
     * Retrieves the trip number for this {@code Load}.
     *
     * @return the trip number
     */
    public String getTripNumber() {
        return tripNumber;
    }

    /**
     * Sets the trip number for this {@code Load}. This is a <strong>required
     * </strong> field and must be unique in the table.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters.
     * <p>
     * If the specified {@code tripNumber} is blank, empty, or {@code null}, it
     * will be set to the same value as the
     * {@link #getOrderNumber() orderNumber}. This is done because not all
     * companies use both an order and a trip number.
     * <p>
     * If the specified {@code tripNumber} is not unique in the table, an
     * {@link InvalidDataException} is thrown.
     *
     * @param tripNumber the trip number
     *
     * @throws InvalidDataException if {@code tripNumber} is not unique in the
     *                              table
     */
    public void setTripNumber(String tripNumber) {
        if (tripNumber == null || tripNumber.isBlank() || tripNumber.isEmpty()) {
            tripNumber = getOrderNumber();
        }
        if (!isTripNumberUniqueInTable(tripNumber)) {
            throw new InvalidDataException(
                    tripNumber + " has already been used "
                    + "for a Load.");
        }
        String old = getTripNumber();
        this.tripNumber = tripNumber.substring(0, 30);
        firePropertyChange("tripNumber", old, getTripNumber());
    }

    /**
     * Retrieves the date that this {@code Load} was dispatched.
     *
     * @return the dispatch date
     */
    public LocalDate getDispatchDate() {
        return dispatchDate;
    }

    /**
     * Sets the date that this {@code Load} was dispatched. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a date. The dispatched date does not
     * indicate the start of the load, only that the load has been successfully
     * obtained and assigned to a unit.
     * <p>
     * If the specified {@code dispatchDate} is {@code null}, it is assigned as
     * the current date.
     * <p>
     * If the specified {@code dispatchDate} is after the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param dispatchDate the dispatch date
     */
    public void setDispatchDate(LocalDate dispatchDate) {
        if (dispatchDate == null) {
            dispatchDate = LocalDate.now();
        } else if (!isDateValid(dispatchDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(
                    df.format(dispatchDate) + " is after "
                    + "the current date (" + df.format(LocalDate.now()) + ").");
        }
        LocalDate old = getDispatchDate();
        this.dispatchDate = dispatchDate;
        firePropertyChange("dispatchDate", old, getDispatchDate());
    }

    /**
     * Retrieves the distance dispatched for the {@code Load}.
     *
     * @return the dispatched distance
     */
    public int getDispatchDistance() {
        return dispatchDistance;
    }

    /**
     * Sets the distance dispatched for the {@code Load}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type for this field is an integer. This value only covers the
     * distance estimated to be travelled from the shipper to the final
     * receiver. This is only an estimate.
     * <p>
     * If the specified {@code dispatchDistance} is less than or equal to zero,
     * an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param dispatchDistance the dispatched distance
     *
     * @throws InvalidDataException if {@code dispatchDistance} is less than or
     *                              equal to zero
     */
    public void setDispatchDistance(int dispatchDistance) {
        if (dispatchDistance <= 0) {
            throw new InvalidDataException(dispatchDistance + " is not valid. "
                    + "It must be greater than zero.");
        }
        int old = getDispatchDistance();
        this.dispatchDistance = dispatchDistance;
        firePropertyChange("dispatchDistance", old, getDispatchDistance());
    }

    /**
     * Retrieves the distance to be travelled from the prior delivery for this
     * {@code Load}.
     *
     * @return the distance to travel for the pickup
     */
    public int getDeadheadDistance() {
        return deadheadDistance;
    }

    /**
     * Sets the distance to be travelled from the prior delivery for this
     * {@code Load}. This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is an integer. This value only covers the
     * distance from the prior load's delivery to this load's pickup location.
     * This value is only an estimate.
     * <p>
     * If the specified {@code deadheadDistance} is less than zero, an
     * {@link InvalidDataException} is thrown. A value of zero is allowed so
     * that a {@code Load} may be entered where the driver is picking up at the
     * location that s/he just delivered.
     * <p>
     * This is a bound property.
     *
     * @param deadheadDistance
     *
     * @throws InvalidDataException if {@code deadheadDistance} is less than
     *                              zero
     */
    public void setDeadheadDistance(int deadheadDistance) {
        if (deadheadDistance < 0) {
            throw new InvalidDataException(deadheadDistance + " is not valid. "
                    + "It must be zero or greater.");
        }
        int old = getDeadheadDistance();
        this.deadheadDistance = deadheadDistance;
        firePropertyChange("deadheadDistance", old, getDeadheadDistance());
    }

    /**
     * Retrieves the gross revenue paid for this {@code Load}.
     *
     * @return the gross revenue
     */
    public double getGrossRevenue() {
        return grossRevenue;
    }

    /**
     * Sets the gross revenue paid for this {@code Load}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a double, and it must be greater than
     * zero.
     * <p>
     * If the specified {@code grossRevenue} is less than or equal to zero, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param grossRevenue the gross revenue
     *
     * @throws InvalidDataException if {@code grossRevenue} is less than or
     *                              equal to zero
     */
    public void setGrossRevenue(double grossRevenue) {
        if (grossRevenue <= 0.00d) {
            throw new InvalidDataException("grossRevenue must be greater than "
                    + "zero.");
        }
        double old = getGrossRevenue();
        this.grossRevenue = grossRevenue;
        firePropertyChange("grossRevenue", old, getGrossRevenue());
    }

    /**
     * Retrieves the piece count for this {@code Load}.
     *
     * @return the pieces
     */
    public int getPieces() {
        return pieces;
    }

    /**
     * Sets the piece count for this {@code Load}. This field is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is an integer, and must be greater than zero.
     * This value can indicate the actual count of each piece on the load or the
     * number of pallets/skids/lifts that make up the load.
     * <p>
     * If the specified {@code pieces} is less than or equal to zero, the
     * default value of one will be set.
     * <p>
     * This is a bound property.
     *
     * @param pieces the pieces
     */
    public void setPieces(int pieces) {
        if (pieces < 1) {
            pieces = 1;
        }
        int old = getPieces();
        this.pieces = pieces;
        firePropertyChange("pieces", old, getPieces());
    }

    /**
     * Retrieves the weight of the freight on this {@code Load}.
     *
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    /**
     * Sets the weight of the freight on this {@code Load}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is an integer, and must be greater than zero.
     * This value can indicate the weight of just the freight (typical use-case)
     * or the total weight of the freight and combination vehicle.
     * <p>
     * If the specified {@code weight} is less than or equal to zero, it will be
     * set to the default value of ten thousand (10,000).
     * <p>
     * This is a bound property.
     *
     * @param weight the weight
     */
    public void setWeight(int weight) {
        if (weight < 1) {
            weight = 10000;
        }
        int old = getWeight();
        this.weight = weight;
        firePropertyChange("weight", old, getWeight());
    }

    /**
     * Retrieves the commodity for this {@code Load}.
     *
     * @return the commodity
     */
    public String getCommodity() {
        return commodity;
    }

    /**
     * Sets the commodity for this {@code Load}. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a string, with a maximum length of
     * seventy-five (75) characters. This value should describe the type of
     * freight being hauled. The default value is &ldquo;General Freight: All
     * Kinds&rdquo;.
     * <p>
     * If the specified {@code commodity} is blank, empty, or {@code null}, then
     * the default value will be set.
     * <p>
     * This is a bound property.
     *
     * @param commodity the commodity description
     */
    public void setCommodity(String commodity) {
        if (commodity == null || commodity.isBlank() || commodity.isEmpty()) {
            commodity = context.getResourceMap(getClass()).getString(
                    "commodity.default.text");
        }
        String old = getCommodity();
        this.commodity = commodity.substring(0, 75);
        firePropertyChange("commodity", old, getCommodity());
    }

    /**
     * Retrieves the <em>Bill of Lading (BOL)</em>, which is the shipping
     * document, for this {@code Load}.
     *
     * @return the BOL
     */
    public String getBol() {
        return bol;
    }

    /**
     * Sets the <em>Bill of Lading (BOL)</em>, which is the shipping document
     * for this {@code Load}. This is a <strong>required</strong> field, though
     * not until the load is picked up at the shipper's location.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters. Typically, this number will not be known until the load
     * has been picked up, at which time the driver signs and receives the BOL.
     * <p>
     * This is a bound field.
     *
     * @param bol the BOL
     */
    public void setBol(String bol) {
        String old = getBol();
        this.bol = bol.substring(0, 40);
        firePropertyChange("bol", old, getBol());
    }

    /**
     * Retrieves the reference number, if any, for this {@code Load}.
     *
     * @return the reference number or {@code null}
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the reference number for this {@code Load}. This is an <em>optional
     * </em> field.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters. Reference numbers are occasionally needed for the
     * shipper to be certain they are shipping the correct freight for their
     * customers. Not all loads will have a reference number.
     * <p>
     * This is a bound property.
     *
     * @param referenceNumber the reference number, if any
     */
    public void setReferenceNumber(String referenceNumber) {
        String old = getReferenceNumber();
        this.referenceNumber = referenceNumber.substring(0, 40);
        firePropertyChange("referenceNumber", old, getReferenceNumber());
    }

    /**
     * Retrieves the pickup number, if any, for this {@code Load}.
     *
     * @return the pickup number or {@code null}
     */
    public String getPickupNumber() {
        return pickupNumber;
    }

    /**
     * Sets the pickup number for this {@code Load}. This is an <em>optional
     * </em> field.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters. Pickup numbers are occasionally needed for the shipper
     * to be certain of the load the driver is picking up. Not all loads will
     * have a pickup number.
     * <p>
     * This is a bound property.
     *
     * @param pickupNumber the pickup number, if any
     */
    public void setPickupNumber(String pickupNumber) {
        String old = getPickupNumber();
        this.pickupNumber = pickupNumber.substring(0, 40);
        firePropertyChange("pickupNumber", old, getPickupNumber());
    }

    /**
     * Determines whether this {@code Load} requires to be tarped.
     *
     * @return {@code true} if tarps are required; {@code false} otherwise
     */
    public boolean isTarped() {
        return tarped;
    }

    /**
     * Sets whether this {@code Load} requires to be tarped. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}.
     * <p>
     * This is a bound property.
     *
     * @param tarped {@code true} if tarps are required; {@code false} if not
     */
    public void setTarped(boolean tarped) {
        boolean old = isTarped();
        this.tarped = tarped;
        firePropertyChange("tarped", old, isTarped());
    }

    /**
     * Retrieves the required height of the tarps, if the {@code Load} needs to
     * be tarped.
     *
     * @return the tarp height
     */
    public int getTarpHeight() {
        return tarpHeight;
    }

    /**
     * Sets the required height (drop) of the tarps, if the {@code Load} needs
     * to be tarped.
     * <p>
     * The data type of this field is an integer, and has a default value of
     * zero.
     * <p>
     * If the load {@link #isTarped() requires tarps}, and the specified
     * {@code tarpHeight} is less than or equal to zero, an
     * {@link InvalidDataException} is thrown. If tarps <em>are not</em>
     * required for the load, then this value is set to zero, regardless of the
     * value passed in.
     * <p>
     * This is a bound property.
     *
     * @param tarpHeight the tarp height
     *
     * @throws InvalidDataException as described above
     */
    public void setTarpHeight(int tarpHeight) {
        if (isTarped() && tarpHeight <= 0) {
            throw new InvalidDataException(tarpHeight + " is not valid. The "
                    + "tarp height must be specified for a load requiring "
                    + "tarps.");
        } else {
            tarpHeight = 0;
        }
        int old = getTarpHeight();
        this.tarpHeight = tarpHeight;
        firePropertyChange("tarpHeight", old, getTarpHeight());
    }

    /**
     * Determines whether this {@code Load} contains any hazardous materials.
     *
     * @return {@code true} if hazardous materials are present; {@code false}
     *         otherwise
     */
    public boolean isHazMat() {
        return hazMat;
    }

    /**
     * Sets whether this {@code Load} contains any hazardous materials. This is
     * a <strong>required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}. The only time this flag needs to be set to {@code true} is
     * if there are hazardous materials in the load in a reportable quantity. If
     * there is, then the driver is required to display placards and have a
     * Hazardous Materials endorsement on their CDL.
     * <p>
     * This is a bound property.
     *
     * @param hazMat {@code true} if reportable quantities of hazardous
     *               materials are present in this load; {@code false} if not
     */
    public void setHazMat(boolean hazMat) {
        boolean old = isHazMat();
        this.hazMat = hazMat;
        firePropertyChange("hazMat", old, isHazMat());
    }

    /**
     * Determines whether this {@code Load} requires team drivers.
     *
     * @return {@code true} if team drivers are required; {@code false} if not
     */
    public boolean isTeam() {
        return team;
    }

    /**
     * Sets whether this {@code Load} requires team drivers. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type for this field is a boolean, with a default value of
     * {@code false}. If a load is time-sensitive, or it is a classified
     * government load, it may require driving teams so that the load either is
     * delivered in a shortened time-frame, or so there is always a driver with
     * the load.
     * <p>
     * This is a bound property.
     *
     * @param team {@code true} if a driving team is require; {@code false} if
     *             not
     */
    public void setTeam(boolean team) {
        boolean old = isTeam();
        this.team = team;
        firePropertyChange("team", old, isTeam());
    }

    /**
     * Determines whether a <em>Transportation Workers' Identification
     * Credential (TWIC)</em> is required for this {@code Load}.
     *
     * @return {@code true} if a TWIC is required; {@code false} if not
     */
    public boolean isTwic() {
        return twic;
    }

    /**
     * Sets whether a <em>Transportation Workers' Identification Credential
     * (TWIC)</em> is required for this {@code Load}. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}. A TWIC may be required in order to access a port in order
     * to pickup or deliver freight. Port access is controlled by maritime law
     * and the Department of Homeland Security's Transportation Security Agency
     * in the United States. The DHS TSA controls obtaining a TWIC for
     * commercial drivers.
     * <p>
     * This is a bound property.
     *
     * @param twic {@code true} if a TWIC is required; {@code false} if not
     */
    public void setTwic(boolean twic) {
        boolean old = isTwic();
        this.twic = twic;
        firePropertyChange("twic", old, isTwic());
    }

    /**
     * Determines whether the {@code Load} requires the driver to count and/or
     * confirm the pieces of freight.
     *
     * @return {@code true} if required; {@code false} if not
     */
    public boolean isCbd() {
        return cbd;
    }

    /**
     * Sets whether the {@code Load} requires the driver to count and/or confirm
     * the pieces of freight. This is a <strong>required</strong> field.
     * <p>
     * The data type for this field is a boolean, with a default value of
     * {@code true}. All loads require that the driver signs the BOL, which is
     * stating that the driver has verified the freight. Therefore, if the count
     * is not correct, then the driver can be held responsible for the expense.
     * Therefore, the overwhelmingly vast majority of loads require the driver
     * to count/confirm the freight.
     * <p>
     * This is a bound property.
     *
     * @param cbd {@code true} if the driver must count/confirm; {@code false}
     *            if not
     */
    public void setCbd(boolean cbd) {
        boolean old = isCbd();
        this.cbd = cbd;
        firePropertyChange("cbd", old, isCbd());
    }

    /**
     * Determines whether this {@code Load} is for a top customer.
     *
     * @return {@code true} if so, {@code false} if not
     */
    public boolean isTopCustomer() {
        return topCustomer;
    }

    /**
     * Sets whether this {@code Load} is for a top customer. This is a <strong>
     * required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code true}. Since customers are the life-blood of any customer, it is
     * strongly advised that the company treat every customer as if they are the
     * only customer. To this end, the top customer field is defaulted to be set
     * to {@code true}. Set to {@code false} if the customer is truly not one of
     * the company's top customers.
     * <p>
     * This is a bound property.
     *
     * @param topCustomer {@code true} for a top customer; {@code false} for all
     *                    others
     */
    public void setTopCustomer(boolean topCustomer) {
        boolean old = isTopCustomer();
        this.topCustomer = topCustomer;
        firePropertyChange("topCustomer", old, isTopCustomer());
    }

    /**
     * Determines whether this {@code Load} is <em>Less Than Truckload (LTL)
     * </em>.
     *
     * @return {@code true} if LTL, {@code false} if full-truckload
     */
    public boolean isLtl() {
        return ltl;
    }

    /**
     * Sets whether this {@code Load} is <em>Less Than Truckload (LTL)</em>.
     * This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}. The vast majority of loads hauled by Owner/Operators are
     * full truckloads, but occasionally, they may be required to haul an LTL
     * load.
     * <p>
     * This is a bound property.
     *
     * @param ltl {@code true} for LTL, {@code false} for truckload (default)
     */
    public void setLtl(boolean ltl) {
        boolean old = isLtl();
        this.ltl = ltl;
        firePropertyChange("ltl", old, isLtl());
    }

    /**
     * Determines whether this {@code Load} requires ramps at the pickup or
     * delivery.
     *
     * @return {@code true} if ramps are required; {@code false} if not
     */
    public boolean isRamps() {
        return ramps;
    }

    /**
     * Sets whether this {@code Load} requires ramps at the pickup or delivery.
     * This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}. Typically, the only companies that will ever set this
     * field to {@code true} are heavy-haul using RGN (low-boy) trailers, or
     * those pulling step-deck trailers.
     * <p>
     * This is a bound property.
     *
     * @param ramps {@code true} if ramps are required; {@code false} if not
     */
    public void setRamps(boolean ramps) {
        boolean old = isRamps();
        this.ramps = ramps;
        firePropertyChange("ramps", old, isRamps());
    }

    /**
     * Determines whether this {@code Load} consists of over-sized freight.
     *
     * @return {@code true} if over-sized; {@code false} if not
     */
    public boolean isOversized() {
        return oversized;
    }

    /**
     * Sets whether this {@code Load} consists of over-sized freight. This is a
     * <strong>required</strong> field.
     * <p>
     * The data type for this field is a boolean, with a default value of
     * {@code false}. This field is only applicable to companies that haul
     * open-deck freight, which could hang over the sides or back of the
     * trailer.
     * <p>
     * This is a bound field.
     *
     * @param oversized {@code true} if over-sized; {@code false} if not
     */
    public void setOversized(boolean oversized) {
        boolean old = isOversized();
        this.oversized = oversized;
        firePropertyChange("oversized", old, isOversized());
    }

    /**
     * Retrieves the height of this {@code Load}.
     *
     * @return the load height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of this {@code Load}. This is a <strong>required</strong>
     * field.
     * <p>
     * The data type of this field is an integer, with a default value of 96
     * inches. This field is only used for {@link #isOversized() oversized}
     * loads. For all other loads, regardless of what the parameter is set to,
     * the value will be set to the default.
     * <p>
     * If this {@code Load} is oversized, and this value is less than or equal
     * to zero, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param height the height of the freight for oversized loads
     */
    public void setHeight(int height) {
        if (!isOversized()) {
            height = 96;
        } else if (height <= 0) {
            throw new InvalidDataException(height + " is less than or equal to "
                    + "zero. Height must be greater than zero.");
        }
        int old = getHeight();
        this.height = height;
        firePropertyChange("height", old, getHeight());
    }

    /**
     * Retrieves the width of this {@code Load}.
     *
     * @return the load width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of this {@code Load}. This is a <strong>required</strong>
     * field.
     * <p>
     * The data type of this field is an integer, with a default value of 102
     * inches. This field is only used for {@link #isOversized() oversized}
     * loads. For all other loads, regardless of what the parameter is set to,
     * the value will be set to the default.
     * <p>
     * If this {@code Load} is oversized, and this value is less than or equal
     * to zero, an {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param width the width of the freight for oversized loads
     */
    public void setWidth(int width) {
        if (!isOversized()) {
            width = 102;
        } else if (width <= 0) {
            throw new InvalidDataException(width + " is less than or equal to "
                    + "zero. Width must be greater than zero.");
        }
        int old = getWidth();
        this.width = width;
        firePropertyChange("width", old, getWidth());
    }

    /**
     * Retrieves the notes on this {@code Load}.
     *
     * @return the load notes
     */
    public String getLoadNotes() {
        return loadNotes;
    }

    /**
     * Sets the notes on this {@code Load}. This is an <em>optional</em> field.
     * <p>
     * The data type of this field is a string, with a maximum length determined
     * by the system on which the application is running, but typically measured
     * in gigabytes. This field accepts free-form text.
     * <p>
     * This is a bound property.
     *
     * @param loadNotes the load notes
     */
    public void setLoadNotes(String loadNotes) {
        String old = getLoadNotes();
        this.loadNotes = loadNotes;
        firePropertyChange("loadNotes", old, getLoadNotes());
    }

    /**
     * Retrieves the date this load was booked.
     *
     * @return the booking date
     */
    public LocalDate getBookedDate() {
        return bookedDate;
    }

    /**
     * Sets the date this {@code Load} was booked. This is a <strong>required
     * </strong> field.
     * <p>
     * The data type of this field is a date.
     * <p>
     * If the specified {@code bookedDate} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code bookedDate} is later than the current date, an
     * {@link InvalidDataException} is thrown.
     * <p>
     * This is a bound property.
     *
     * @param bookedDate the booking date
     *
     * @throws NullPointerException if {@code bookedDate} is {@code null}
     * @throws InvalidDataException if {@code bookedDate} is after the current
     *                              date
     */
    public void setBookedDate(LocalDate bookedDate) {
        if (bookedDate == null) {
            throw new NullPointerException("bookingDate is null.");
        } else if (!isDateValid(bookedDate)) {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd");
            throw new InvalidDataException(df.format(bookedDate) + " is after "
                    + "the current date (" + df.format(LocalDate.now()) + ").");
        }
        LocalDate old = getBookedDate();
        this.bookedDate = bookedDate;
        firePropertyChange("bookedDate", old, getBookedDate());
    }

    /**
     * Retrieves the {@link Customer broker} for this {@code Load}.
     *
     * @return the broker or agent
     */
    public Customer getBroker() {
        if (broker == null || broker.isBlank() || broker.isEmpty()) {
            return null;
        }
        return Customer.findByPrimaryKey(broker);
    }

    /**
     * Sets the {@link Customer broker} for this {@code Load}. This is a
     * <strong>required</strong> field.
     * <p>
     * The broker is the person or company through which the load was obtained.
     * If the load was obtained without going through a third-party broker, the
     * default {@code Customer} with the ID of &ldquo;SELF&rdquo; is returned,
     * which has all {@code null} values by default.
     * <p>
     * If the specified {@code broker} is {@code null}, it is assigned the
     * default holder {@code Customer}.
     * <p>
     * This is a bound property.
     *
     * @param broker the broker or agent
     */
    public void setBroker(Customer broker) {
        if (broker == null) {
            broker = Customer.findByPrimaryKey("SELF");
        }
        Customer old = getBroker();
        this.broker = broker.getCustomerId();
        firePropertyChange("broker", old, getBroker());
    }
    
    public Employee getDispatchedBy() {
        if (dispatchedBy == null) {
            return null;
        }
        return Employee.findByPrimaryKey(dispatchedBy);
    }
    
    public void setDispatchedBy(Employee dispatchedBy) {
        if (dispatchedBy == null) {
            dispatchedBy = Employee.findByPrimaryKey("SELF");
        }
        Employee old = getDispatchedBy();
        this.dispatchedBy = dispatchedBy.getEmployeeId();
        firePropertyChange("dispatchedBy", old, getDispatchedBy());
    }

    /**
     * Retrieves the {@link Unit} to which this {@code Load} is assigned.
     *
     * @return the assigned unit
     */
    public Unit getAssignedTo() {
        if (assignedTo == null || assignedTo.isBlank() || assignedTo.isEmpty()) {
            return null;
        }
        return Unit.findByPrimaryKey(assignedTo);
    }

    /**
     * Sets the {@link Unit} to which this {@code Load} is assigned. This is a
     * <strong>required</strong> field.
     * <p>
     * The unit is the primary vehicle which may either be the truck (preferred)
     * or the trailer (for preloaded trailers). The default {@code Unit}, which
     * has the ID of &ldquo;SELF&rdquo;, may be used. The default unit has all
     * values set to {@code null} by default.
     * <p>
     * If the specified {@code assignedTo} is {@code null}, then it is set to
     * the default {@code Unit}.
     * <p>
     * This is a bound field.
     *
     * @param assignedTo the assigned unit
     */
    public void setAssignedTo(Unit assignedTo) {
        if (assignedTo == null) {
            assignedTo = Unit.findByPrimaryKey("SELF");
        }
        Unit old = getAssignedTo();
        this.assignedTo = assignedTo.getUnitNumber();
        firePropertyChange("dispatchedBy", old, getAssignedTo());
    }

    /**
     * Determines whether this {@code Load} has been completed (i.e., all stops
     * have been completed).
     *
     * @return {@code true} if completed; {@code false} if in progress
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * Sets whether this {@code Load} has been completed (i.e., all stops have
     * been completed). This is a <strong>required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}. This field should only be set to {@code true} from the
     * last stop of the load.
     * <p>
     * This is a bound field.
     *
     * @param completed {@code true} if completed; {@code false} if in progress
     */
    public void setCompleted(boolean completed) {
        boolean old = isCompleted();
        this.completed = completed;
        firePropertyChange("completed", old, isCompleted());
    }

    /**
     * Determines whether this {@code Load} has been settled (paid).
     *
     * @return {@code true} if settled; {@code false} if not
     */
    public boolean isSettled() {
        return settled;
    }

    /**
     * Sets whether this {@code Load} has been settled (paid). This is a
     * <strong>required</strong> field.
     * <p>
     * The data type of this field is a boolean, with a default value of
     * {@code false}. This field should only be set once the settlement sheet
     * and payment has been received and distributed to the various accounts in
     * the accounting system.
     * <p>
     * This is a bound property.
     *
     * @param settled {@code true} if settled; {@code false} if not
     */
    public void setSettled(boolean settled) {
        boolean old = isSettled();
        this.settled = settled;
        firePropertyChange("settled", old, isSettled());
    }
    
    /**
     * Retrieves all of the {@link Stop stops} for this {@code Load}. The stops
     * are returned as a {@link List} and in stop number order.
     * <p>
     * This is a <em>read-only</em> property.
     * 
     * @return an ordered list of all stops or an empty list
     */
    public List<Stop> getStops() {
        return Stop.findRecords(Stop.FLD_LOAD + " = " + orderNumber,
                Stop.FLD_STOP + " ASC");
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderNumber != null ? orderNumber.hashCode() : 0);
        hash += (tripNumber != null ? tripNumber.hashCode() : 0);
        hash += (dispatchDate != null ? dispatchDate.hashCode() : 0);
        hash += dispatchDistance;
        hash += deadheadDistance;
        hash += (grossRevenue != null ? grossRevenue.hashCode() : 0);
        hash += pieces;
        hash += weight;
        hash += (commodity != null ? commodity.hashCode() : 0);
        hash += (bol != null ? bol.hashCode() : 0);
        hash += (referenceNumber != null ? referenceNumber.hashCode() : 0);
        hash += (pickupNumber != null ? pickupNumber.hashCode() : 0);
        hash += (tarped ? 1 : 0);
        hash += tarpHeight;
        hash += (hazMat ? 1 : 0);
        hash += (team ? 1 : 0);
        hash += (twic ? 1 : 0);
        hash += (cbd ? 1 : 0);
        hash += (topCustomer ? 1 : 0);
        hash += (ltl ? 1 : 0);
        hash += (ramps ? 1 : 0);
        hash += (oversized ? 1 : 0);
        hash += height;
        hash += width;
        hash += (loadNotes != null ? loadNotes.hashCode() : 0);
        hash += (broker != null ? broker.hashCode() : 0);
        hash += (dispatchedBy != null ? dispatchedBy.hashCode() : 0);
        hash += (assignedTo != null ? assignedTo.hashCode() : 0);
        hash += (completed ? 1 : 0);
        hash += (settled ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Load)) {
            return false;
        }
        Load other = (Load) object;
        if ((this.orderNumber == null && other.orderNumber != null)
                || (this.orderNumber != null
                && !this.orderNumber.equals(other.orderNumber))) {
            return false;
        }
        if ((this.tripNumber == null && other.tripNumber != null)
                || (this.tripNumber != null
                && !this.tripNumber.equals(other.tripNumber))) {
            return false;
        }
        if ((this.dispatchDate == null && other.dispatchDate != null)
                || (this.dispatchDate != null
                && !this.dispatchDate.equals(other.dispatchDate))) {
            return false;
        }
        if (this.dispatchDistance != other.dispatchDistance) {
            return false;
        }
        if (this.deadheadDistance != other.deadheadDistance) {
            return false;
        }
        if ((this.grossRevenue == null && other.grossRevenue != null)
                || (this.grossRevenue != null
                && !this.grossRevenue.equals(other.grossRevenue))) {
            return false;
        }
        if (this.pieces != other.pieces) {
            return false;
        }
        if (this.weight != other.weight) {
            return false;
        }
        if ((this.commodity == null && other.commodity != null)
                || (this.commodity != null
                && !this.commodity.equals(other.commodity))) {
            return false;
        }
        if ((this.bol == null && other.bol != null)
                || (this.bol != null && !this.bol.equals(other.bol))) {
            return false;
        }
        if ((this.referenceNumber == null && other.referenceNumber != null)
                || (this.referenceNumber != null
                && !this.referenceNumber.equals(other.referenceNumber))) {
            return false;
        }
        if ((this.pickupNumber == null && other.pickupNumber != null)
                || (this.pickupNumber != null 
                && !this.pickupNumber.equals(other.pickupNumber))) {
            return false;
        }
        if (this.tarped != other.tarped) {
            return false;
        }
        if (this.tarpHeight != other.tarpHeight) {
            return false;
        }
        if (this.hazMat != other.hazMat) {
            return false;
        }
        if (this.team != other.team) {
            return false;
        }
        if (this.twic != other.twic) {
            return false;
        }
        if (this.cbd != other.cbd) {
            return false;
        }
        if (this.topCustomer != other.topCustomer) {
            return false;
        }
        if (this.ltl != other.ltl) {
            return false;
        }
        if (this.ramps != other.ramps) {
            return false;
        }
        if (this.oversized != other.oversized) {
            return false;
        }
        if ((this.getBroker() == null && other.getBroker() != null)
                || (this.getBroker() != null
                && !this.getBroker().equals(other.getBroker()))) {
            return false;
        }
        if ((this.getDispatchedBy() == null && other.getDispatchedBy() != null)
                || (this.getDispatchedBy() != null 
                && !this.getDispatchedBy().equals(other.getDispatchedBy()))) {
            return false;
        }
        if ((this.getAssignedTo() == null && other.getAssignedTo() != null)
                || (this.getAssignedTo() != null
                && !this.getAssignedTo().equals(other.getAssignedTo()))) {
            return false;
        }
        return this.completed == other.completed 
                && this.settled == other.settled;
    }

    @Override
    public String toString() {
        String lading = (bol == null || bol.isBlank() || bol.isEmpty())
                ? "UNKNOWN" : bol;
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        List<Stop> stops = getStops();
        return context.getResourceMap(getClass()).getString("toString",
                orderNumber, tripNumber, lading, stops.get(0).getCustomer(),
                stops.get(stops.size() - 1).getCustomer(), nf.format(grossRevenue), 
                nf.format(grossRevenue / dispatchDistance));
    }
    
    public String debugginString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Load} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() tripNumber} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        validate();
        if (nue) {
            insert();
        } else {
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_TRIP + ", " + FLD_DISP_DATE + ", " 
                + FLD_DISP_DIST + ", " + FLD_DEDHED + ", "
                + FLD_REVENUE + ", " + FLD_PCS + ", " 
                + FLD_WGT + ", " + FLD_COMMODITY + ", "
                + FLD_BOL + ", " + FLD_REF + ", " 
                + FLD_PICKUP + ", " + FLD_TARPED + ", "
                + FLD_TARP_HGT + ", " + FLD_HAZMAT + ", "
                + FLD_TEAM + ", " + FLD_TWIC + ", "
                + FLD_CBD + ", " + FLD_TOP + ", "
                + FLD_LTL + ", " + FLD_RAMPS + ", "
                + FLD_OD + ", " + FLD_HGT + ", " + FLD_WD + ", "
                + FLD_NOTES + ", " + FLD_BRKR + ", "
                + FLD_DISP_BY + ", " + FLD_ASSIGNED + ", "
                + FLD_COMP + ", " + FLD_SETT + ") VALUES(?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, orderNumber);
            pst.setString(2, tripNumber);
            pst.setDate(3, Date.valueOf(dispatchDate));
            pst.setInt(4, dispatchDistance);
            pst.setInt(5, deadheadDistance);
            pst.setDouble(6, grossRevenue);
            pst.setInt(7, pieces);
            pst.setInt(8, weight);
            pst.setString(9, commodity);
            pst.setString(10, bol);
            pst.setString(11, referenceNumber);
            pst.setString(12, pickupNumber);
            pst.setBoolean(13, tarped);
            pst.setInt(14, tarpHeight);
            pst.setBoolean(15, hazMat);
            pst.setBoolean(16, team);
            pst.setBoolean(17, twic);
            pst.setBoolean(18, cbd);
            pst.setBoolean(19, topCustomer);
            pst.setBoolean(20, ltl);
            pst.setBoolean(21, ramps);
            pst.setBoolean(22, oversized);
            pst.setInt(23, height);
            pst.setInt(24, width);
            pst.setString(25, loadNotes);
            pst.setString(26, broker);
            pst.setString(27, dispatchedBy);
            pst.setString(28, assignedTo);
            pst.setBoolean(29, completed);
            pst.setBoolean(30, settled);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Load.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_TRIP + " = ?, " + FLD_DISP_DATE + " = ?, " 
                + FLD_DISP_DIST + " = ?, " + FLD_DEDHED + " = ?, "
                + FLD_REVENUE + " = ?, " + FLD_PCS + " = ?, " 
                + FLD_WGT + " = ?, " + FLD_COMMODITY + " = ?, "
                + FLD_BOL + " = ?, " + FLD_REF + " = ?, " 
                + FLD_PICKUP + " = ?, " + FLD_TARPED + " = ?, "
                + FLD_TARP_HGT + " = ?, " + FLD_HAZMAT + " = ?, "
                + FLD_TEAM + " = ?, " + FLD_TWIC + " = ?, "
                + FLD_CBD + " = ?, " + FLD_TOP + " = ?, "
                + FLD_LTL + " = ?, " + FLD_RAMPS + " = ?, "
                + FLD_OD + " = ?, " + FLD_HGT + " = ?, " + FLD_WD + " = ?, "
                + FLD_NOTES + " = ?, " + FLD_BRKR + " = ?, "
                + FLD_DISP_BY + " = ?, " + FLD_ASSIGNED + " = ?, "
                + FLD_COMP + " = ?, " + FLD_SETT + " = ? "
                + " WHERE " + FLD_ID + " = ?";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, tripNumber);
            pst.setDate(2, Date.valueOf(dispatchDate));
            pst.setInt(3, dispatchDistance);
            pst.setInt(4, deadheadDistance);
            pst.setDouble(5, grossRevenue);
            pst.setInt(6, pieces);
            pst.setInt(7, weight);
            pst.setString(8, commodity);
            pst.setString(9, bol);
            pst.setString(10, referenceNumber);
            pst.setString(11, pickupNumber);
            pst.setBoolean(12, tarped);
            pst.setInt(13, tarpHeight);
            pst.setBoolean(14, hazMat);
            pst.setBoolean(15, team);
            pst.setBoolean(16, twic);
            pst.setBoolean(17, cbd);
            pst.setBoolean(18, topCustomer);
            pst.setBoolean(19, ltl);
            pst.setBoolean(20, ramps);
            pst.setBoolean(21, oversized);
            pst.setInt(22, height);
            pst.setInt(23, width);
            pst.setString(24, loadNotes);
            pst.setString(25, broker);
            pst.setString(26, dispatchedBy);
            pst.setString(27, assignedTo);
            pst.setBoolean(28, completed);
            pst.setBoolean(29, settled);
            pst.setString(30, orderNumber);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Load.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        if (orderNumber == null || orderNumber.isBlank()
                || orderNumber.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "orderNumber"));
        }
        if (tripNumber == null || tripNumber.isBlank() || tripNumber.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "tripNumber"));
        }
        if (dispatchDate == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", 
                            "dispatchDate"));
        }
        if (grossRevenue == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "grossRevenue"));
        }
        if (pieces <= 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", "pieces", "Pieces "
                            + "must be greater than zero."));
        }
        if (weight <= 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", "weight", "Weight "
                            + "must be greater than zero."));
        }
        if (commodity == null || commodity.isBlank() || commodity.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "commodity"));
        }
        if (tarped && tarpHeight <= 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", "tarpHeight", 
                            "Tarp height must be greater than zero because "
                            + "load is tarped."));
        }
        if (oversized && height <= 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", "height", "Height "
                            + "must be greater than zero because load is "
                            + "oversized."));
        }
        if (oversized && width <= 0) {
            throw new ValidationException(this, 
                    resourceMap.getString("invalid.value", "width", "Width "
                            + "must be greater than zero because load is "
                            + "oversized."));
        }
        if (broker == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "broker"));
        }
        if (dispatchedBy == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "dispatchedBy"));
        }
        if (assignedTo == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "assignedTo"));
        }
    }

    private boolean isOrderNumberUniqueInTable(String orderNumber) {
        Load test = findByPrimaryKey(orderNumber);
        return test == null;
    }

    private boolean isTripNumberUniqueInTable(String tripNumber) {
        Load test = findRecord(FLD_TRIP + " = " + tripNumber);
        return test == null;
    }

    private boolean isDateValid(LocalDate date) {
        return date.isBefore(LocalDate.now());
    }

    private String orderNumber;
    private String tripNumber = orderNumber;
    private LocalDate dispatchDate = LocalDate.now();
    private String dispatchedBy = "SELF";
    private int dispatchDistance = 500;
    private int deadheadDistance = 1;
    private Double grossRevenue = 1500.00;
    private int pieces = 1;
    private int weight = 25000;
    private String commodity = "General Freight: All Kinds";
    private String bol;
    private String referenceNumber;
    private String pickupNumber;
    private boolean tarped = false;
    private int tarpHeight = 0;
    private boolean hazMat = false;
    private boolean team = false;
    private boolean twic = false;
    private boolean cbd = true;
    private boolean topCustomer = true;
    private boolean ltl = false;
    private boolean ramps = false;
    private boolean oversized = false;
    private int height = 96;
    private int width = 102;
    private String loadNotes;
    private LocalDate bookedDate = LocalDate.now();
    private String broker = "N/A";
    private String assignedTo = "SELF";
    private boolean completed = false;
    private boolean settled = false;

}
