/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   Customer.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 29, 2024
 *  Modified   :   Jun 29, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 29, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.data;

import com.pekinsoft.api.StatusDisplayer.MessageType;
import com.pekinsoft.api.StatusMessage;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.desktop.error.ErrorLevel;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.data.support.ValidationException;
import com.pekinsoft.utils.ObjectUtils;
import com.pekinsoft.utils.PropertyKeys;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Customer extends AbstractBean 
        implements Serializable, PropertyKeys {

    /**
     * Constant representing the table field name in the database for the
     * {@code Customer} primary key identity field.
     */
    public static final String FLD_ID = "CUSTOMER_ID";
    /**
     * Constant representing the table field name in the database for the
     * {@code Customer} company name field.
     */
    public static final String FLD_NAME = "COMPANY";
    /**
     * Constant representing the table field name in the database for the
     * {@code Customer} street address field.
     */
    public static final String FLD_STREET = "STREET";
    /**
     * Constant representing the table field name in the database for the
     * {@code Customer} suite number field.
     */
    public static final String FLD_SUITE = "SUITE";
    /**
     * Constant representing the table field name in the database for the
     * {@code Customer} city field.
     */
    public static final String FLD_CITY = "CITY";
    /**
     * Constant representing the table field name in the database for the
     * US State or Canadian Province in which the {@code Customer} is located.
     */
    public static final String FLD_REGION = "REGION";
    /**
     * Constant representing the table field name in the database for the
     * US Zip or Canadian Postal Code for the {@code Customer}'s address.
     */
    public static final String FLD_POSTAL = "POSTAL_CODE";
    /**
     * Constant representing the table field name in the database for the 
     * country in which the {@code Customer} is located.
     */
    public static final String FLD_COUNTRY = "COUNTRY";
    /**
     * Constant representing the table field name in the database for the name
     * of the contact person at the {@code Customer}.
     */
    public static final String FLD_CONTACT = "CONTACT_NAME";
    /**
     * Constant representing the table field name in the database for the phone
     * number for the {@code Customer}.
     */
    public static final String FLD_PHONE = "PHONE";
    /**
     * Constant representing the table field name in the database for the fax
     * number for the {@code Customer}.
     */
    public static final String FLD_FAX = "FAX";
    /**
     * Constant representing the table field name in the database for the email
     * address for the {@code Customer}.
     */
    public static final String FLD_EMAIL = "EMAIL";
    /**
     * Constant representing the table field name in the database for the notes
     * about the {@code Customer}.
     */
    public static final String FLD_NOTES = "NOTES";
    /**
     * Constant representing the table field name in the database for the
     * default {@link Account} into which transactions for the {@code Customer}
     * should be recorded.
     */
    public static final String FLD_ACCT = "DEFAULT_ACCOUNT";
    /**
     * Constant representing the table field name in the database for the 
     * {@code CustomerType} to which the {@code Customer} is assigned.
     */
    public static final String FLD_TYPE = "CUSTOMER_TYPE";
    /**
     * Constant representing the table field name in the database for the 
     * inactive flag for the {@code Customer}.
     */
    public static final String FLD_INACT = "INACTIVE";

    private static final ApplicationContext context = Application.getInstance()
            .getContext();
    private static final ResourceMap resourceMap = context.getResourceMap(
            Database.class);
    private static final long serialVersionUID = 1L;
    private static final Logger logger = context.getLogger(Customer.class);
    
    private static final String TABLE_NAME = "CUSTOMERS";
    private static final String SCHEMA = Database.getInstance().getSchema();
    private static final String SELECT = "SELECT * FROM %1$s.%2$s";
    private static final String INSERT_PREFIX = "INSERT INTO %1$s.%2$s ";
    private static final String UPDATE_PREFIX = "UPDATE %1$s.%2$s SET ";
    private static final String DELETE_STATEMENT = "DELETE FROM %1$s.%2$s WHERE " 
            + FLD_ID + " = ?";

    /**
     * Retrieves all records in the table. If the table contains no records, an
     * empty {@link List} is returned.
     * 
     * @return a list of all records in the table or an empty list
     */
    public static List<Customer> getAllRecords() {
        return selectRecords(null, null);
    }
    
    /**
     * Retrieves all records in the table, ordered as directed. If the table
     * contains no records, an empty {@link List} is returned.
     * 
     * @param orderBy the field(s) on which to order the records, and either the
     *                {@code ASC} or {@code DESC} ordering keyword for each
     *                field
     * 
     * @return an ordered list of all records, or an empty list
     */
    public static List<Customer> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }
    
    /**
     * Retrieves the record identified by the specified {@code value} in its
     * primary key field.
     * <p>
     * If no record's primary key value matches the value specified, then 
     * {@code null} is returned.
     * 
     * @param value the value to find
     * 
     * @return the matching record, or {@code null} if no match
     */
    public static Customer findByPrimaryKey(String value) {
        if (value == null || value.isBlank() || value.isEmpty()) {
            return null;
        }
        String where = FLD_ID + " = " + value;
        return findRecord(where);
    }
    
    /**
     * Retrieves the record identified by the specified {@code email} address.
     * <p>
     * If no record matches the email address specified, then {@code null} is
     * returned.
     * 
     * @param email the email address to find
     * @return the record with the specified email address or {@code null}
     */
    public static Customer findByEmailAddress(String email) {
        if (email == null || email.isBlank() || email.isEmpty()) {
            return null;
        }
        String where = FLD_EMAIL + " = " + email;
        return findRecord(where);
    }
    
    /**
     * Retrieves the record identified by the specified {@code phone} number.
     * <p>
     * If no record matches the phone number specified, then {@code null} is
     * returned.
     * 
     * @param phone the phone number to find
     * @return the record with the specified phone number or {@code null}
     */
    public static Customer findByPhoneNumber(String phone) {
        if (phone == null || phone.isBlank() || phone.isEmpty()) {
            return null;
        }
        String where = FLD_PHONE + " = " + phone;
        return findRecord(where);
    }

    /**
     * Retrieves the record that matches the criteria contained in the
     * {@code where} parameter. If no records match the criteria, {@code null} 
     * is returned. If more than one record matches the criteria, only the first
     * record is returned.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, then the
     * first record in the table is returned. If the table is empty, a
     * {@code null} value is returned.
     * 
     * @param where the search criteria by which to find the desired record
     * 
     * @return the matching record if only one matches, the first matching 
     *         record if more than one record matches, {@code null} if no 
     *         records match
     */
    public static Customer findRecord(String where) {
        List<Customer> records = selectRecords(where, null);
        if (!records.isEmpty()) {
            return records.get(0);
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves a {@link List} of records that have the specified 
     * {@code company} name.
     * 
     * @param company the company name to find
     * 
     * @return a list of records matching the company name or an empty list
     */
    public static List<Customer> findByCompanyName(String company) {
        String where = FLD_NAME + " = " + company;
        return findRecords(where);
    }
    
    /**
     * Retrieves a filtered list of records based upon the criteria contained in
     * the {@code where} parameter.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * This method is guaranteed to never return {@code null}. If there are no
     * matches to the criteria (or no records in the table), an empty list is
     * returned.
     * 
     * @param where the search criteria on which to filter the records
     * 
     * @return the filtered list based on the criteria, or an empty list
     */
    public static List<Customer> findRecords(String where) {
        return selectRecords(where, null);
    }

    /**
     * Retrieves all records from the table that match the criteria contained in
     * the {@code where} parameter, and ordered as directed in the 
     * {@code orderBy} parameter. Neither parameter is required.
     * <p>
     * If the {@code where} parameter is blank, empty, or {@code null}, it will
     * have no effect on the results and all records will be returned.
     * <p>
     * If the {@code orderBy} parameter is blank, empty, or {@code null}, it 
     * will have no effect on the ordering of the results and the records will 
     * be returned in the database default order. This is typically the order in
     * which the records were entered.
     * <p>
     * This method is guaranteed to never return a {@code null} value, if no
     * records are found, the returned {@link List} will be empty.
     * 
     * @param where   the search criteria on which to filter the records
     * @param orderBy the ordering criteria in which the records should be 
     *                ordered
     * 
     * @return a list of all matching records, ordered as requested; or an empty
     *         list if no records match
     */
    public static List<Customer> findRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }
    
    /**
     * Deletes the specified record.
     * <p>
     * This action cannot be undone. It is assumed that the UI provided a means
     * for the user to change their mind about deleting the record and takes the
     * appropriate action based on that choice. This method does nothing to 
     * protect accidental deletion of the record.
     * <p>
     * If the specified {@code record} is {@code null}, no exception is thrown
     * and no action is taken.
     * 
     * @param id the primary key value of the record to be deleted
     * 
     * @return {@code true} upon success; {@code false} upon failure
     */
    public static boolean deleteRecord(String id) {
        if (id == null || id.isBlank() || id.isEmpty()) {
            return false;
        }
        
        String sql = String.format(DELETE_STATEMENT, SCHEMA, TABLE_NAME);
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, id);
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("delete.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("delete.error.title");
            String category = resourceMap.getString("category.db.delete");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Customer.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] The following error occurred, \"%2$s\""
                    + "while executing the statement %3$s, with the primary key "
                    + "value %4$s", 
                    e.getSQLState(), e.getMessage(), sql, id);
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return true;
    }
    
    private static List<Customer> selectRecords(String where, String orderBy) {
        String sql = String.format(SELECT, SCHEMA, TABLE_NAME);
        if (where != null && !where.isBlank() && !where.isEmpty()) {
            if (where.toLowerCase().startsWith("where")) {
                sql += " " + where;
            } else if (where.toLowerCase().startsWith(" where")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null && !orderBy.isBlank() && !orderBy.isEmpty()) {
            if (orderBy.toLowerCase().startsWith("order by")) {
                sql += " " + orderBy;
            } else if (orderBy.toLowerCase().startsWith(" order by")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }
        
        List<Customer> records = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = Database.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                Customer record = new Customer();
                record.customerId = rs.getString(1);
                record.company = rs.getString(2);
                record.street = rs.getString(3);
                record.suite = rs.getString(4);
                record.city = rs.getString(5);
                record.region = rs.getString(6);
                record.postalCode = rs.getString(7);
                record.country = rs.getString(8);
                record.contact = rs.getString(9);
                record.phone = rs.getString(10);
                record.fax = rs.getString(11);
                record.email = rs.getString(12);
                record.notes = rs.getString(13);
                record.defaultAccount = rs.getString(14);
                record.customerType = rs.getLong(15);
                record.inactive = rs.getBoolean(16);
                records.add(record);
            }
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("select.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("select.error.title");
            String category = resourceMap.getString("category.db.select");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Customer.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(stmt));
            state.put("ResultSet", String.valueOf(rs));
            state.put("SQL Statement", sql);
            state.put("Record Count (so far)", String.valueOf(records.size()));
            state.put("Records List", String.valueOf(records));
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            logger.log(Level.WARNING, msg, e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
        
        return records;
    }

    /**
     * Constructs a new, empty {@code Customer}. Once created, all fields are set
     * to {@code null} and need to be set manually.
     */
    public Customer() {

    }
    
    /**
     * Convenience constructor for creating a new broker or agent record, since
     * none of the contact information fields are required for them.
     * <p>
     * If the specified {@code customerType} is {@code null}, a default, all
     * {@code null} values {@code Customer} will be constructed.
     * 
     * @param customerType the {@link CustomerType} for a broker or agent
     */
    public Customer(CustomerType customerType) {
        if (customerType == null) {
            return;
        }
        this.customerType = customerType.getTypeId();
    }

    /**
     * Retrieves the unique identifier for this {@code Customer}.
     * 
     * @return the unique identifier
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the unique identifier for this {@code Customer}. This is a <strong>
     * required</strong> value and is the primary key in the table.
     * <p>
     * The data type of this field is a string, with a maximum length of ten 
     * (10) characters. This value <em>must be unique</em> within the table and
     * will be checked for uniqueness when the record is saved. If the value is
     * not unique, it will be made unique by appending a two-digit number to the
     * end, based upon how many other records start with the same characters.
     * <p>
     * If the specified {@code customerId} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param customerId the unique identifier
     * 
     * @throws NullPointerException if {@code customerId} is blank, empty, or
     *                              {@code null}
     */
    public void setCustomerId(String customerId) {
        String old = getCustomerId();
        this.customerId = customerId;
        firePropertyChange("customerId", old, getCustomerId());
    }

    /**
     * Retrieves the name of the company.
     * 
     * @return the company name
     */
    public String getCompany() {
        return company;
    }

    /**
     * Sets the name of the company.
     * <p>
     * The data type of this field is a string, with a maximum length of fifty
     * (50) characters.
     * <dl>
     * <dt>Field Requirements</dt>
     * <dd>If the {@link #getCustomerType() customerType} property is set for a
     * broker or agent, then this field is <em>optional</em> and {@code null}
     * values are permitted.<br>
     * <br>
     * If this {@code Customer} is of any other {@code customerType}, then this
     * is a <strong>required</strong> value.</dd></dl>
     * <p>
     * If the {@code customerType} <strong>is not</strong> set nor set to a
     * broker or agent, and {@code company} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param company the name of the company
     * 
     * @throws NullPointerException as defined above
     */
    public void setCompany(String company) {
        if (!isBrokerOrAgent() && (company == null || company.isBlank()
                || company.isEmpty())) {
            throw new NullPointerException("company is blank, empty, or null, "
                    + "and the customer type is not a broker or agent.");
        }
        String old = getCompany();
        this.company = company;
        firePropertyChange("company", old, getCompany());
    }

    /**
     * Retrieves the street address for the {@code Customer}.
     * 
     * @return the street address
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets the street address for the {@code Customer}.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters.
     * <dl>
     * <dt>Field Requirements</dt>
     * <dd>If the {@link #getCustomerType() customerType} property is set for a
     * broker or agent, then this field is <em>optional</em> and {@code null}
     * values are permitted.<br>
     * <br>
     * If this {@code Customer} is of any other {@code customerType}, then this
     * is a <strong>required</strong> value.</dd></dl>
     * <p>
     * If the {@code customerType} <strong>is not</strong> set nor set to a
     * broker or agent, and {@code street} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param street the street address
     * 
     * @throws NullPointerException as described above
     */
    public void setStreet(String street) {
        if (!isBrokerOrAgent() && (street == null || street.isBlank()
                || street.isEmpty())) {
            throw new NullPointerException("street is blank, empty, or null, "
                    + "and the customer type is not a broker or agent.");
        }
        String old = getStreet();
        this.street = street;
        firePropertyChange("street", old, getStreet());
    }

    /**
     * Retrieves the suite number for the {@code Customer}'s address, if any.
     * 
     * @return the suite number, if any
     */
    public String getSuite() {
        return suite;
    }

    /**
     * Sets the suite number for the {@code Customer}'s address, if any. This is
     * an <em>optional</em> value and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of fifteen
     * (15) characters.
     * <p>
     * This is a bound property.
     * 
     * @param suite the suite number, if any
     */
    public void setSuite(String suite) {
        String old = getSuite();
        this.suite = suite;
        firePropertyChange("suite", old, getSuite());
    }

    /**
     * Retrieves the city in which the {@code Customer} is located.
     * 
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the city in which the {@code Customer} is located.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters.
     * <dl>
     * <dt>Field Requirements</dt>
     * <dd>If the {@link #getCustomerType() customerType} property is set for a
     * broker or agent, then this field is <em>optional</em> and {@code null}
     * values are permitted.<br>
     * <br>
     * If this {@code Customer} is of any other {@code customerType}, then this
     * is a <strong>required</strong> value.</dd></dl>
     * <p>
     * If the {@code customerType} <strong>is not</strong> set nor set to a
     * broker or agent, and {@code city} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param city the city name
     * 
     * @throws NullPointerException as described above
     */
    public void setCity(String city) {
        if (!isBrokerOrAgent() && (city == null || city.isBlank()
                || city.isEmpty())) {
            throw new NullPointerException("city is blank, empty, or null, and "
                    + "the customer type is not a broker or agent.");
        }
        String old = getCity();
        this.city = city;
        firePropertyChange("city", old, getCity());
    }

    /**
     * Retrieves the region in which the {@code Customer}'s city is located. 
     * This is a US State or Canadian Province abbreviation.
     * 
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the region in which the {@code Customer}'s city is located. This is
     * a US State or Canadian Province abbreviation.
     * <p>
     * The data type of this field is a string, with a maximum length of two (2)
     * characters.
     * <dl>
     * <dt>Field Requirements</dt>
     * <dd>If the {@link #getCustomerType() customerType} property is set for a
     * broker or agent, then this field is <em>optional</em> and {@code null}
     * values are permitted.<br>
     * <br>
     * If this {@code Customer} is of any other {@code customerType}, then this
     * is a <strong>required</strong> value.</dd></dl>
     * <p>
     * If the {@code customerType} <strong>is not</strong> set nor set to a
     * broker or agent, and {@code region} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param region the region
     * 
     * @throws NullPointerException as described above
     */
    public void setRegion(String region) {
        if (!isBrokerOrAgent() && (region == null || region.isBlank()
                || region.isEmpty())) {
            throw new NullPointerException("region is blank, empty, or null, "
                    + "and customer type is not a broker or agent.");
        }
        String old = getRegion();
        this.region = region;
        firePropertyChange("region", old, getRegion());
    }

    /**
     * Retrieves the Postal Code for the {@code Customer}'s address.
     * 
     * @return the Postal Code
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the Postal Code for the {@code Customer}'s address.
     * <p>
     * The data type of this field is a string, with a maximum length of fifteen
     * (15) characters.
     * <dl>
     * <dt>Field Requirements</dt>
     * <dd>If the {@link #getCustomerType() customerType} property is set for a
     * broker or agent, then this field is <em>optional</em> and {@code null}
     * values are permitted.<br>
     * <br>
     * If this {@code Customer} is of any other {@code customerType}, then this
     * is a <strong>required</strong> value.</dd></dl>
     * <p>
     * If the {@code customerType} <strong>is not</strong> set nor set to a
     * broker or agent, and {@code postalCode} is blank, empty, or {@code null},
     * a {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param postalCode the Postal Code
     * 
     * @throws NullPointerException as described above
     */
    public void setPostalCode(String postalCode) {
        if (!isBrokerOrAgent() && (postalCode == null || postalCode.isBlank()
                || postalCode.isEmpty())) {
            throw new NullPointerException("postalCode is blank, empty, or "
                    + "null, and the customer type is not a broker or agent");
        }
        String old = getPostalCode();
        this.postalCode = postalCode;
        firePropertyChange("postalCode", old, getPostalCode());
    }
    
    /**
     * Retrieves the country in which the {@code Customer} is located.
     * 
     * @return the country
     */
    public String getCountry() {
        return country;
    }
    
    /**
     * Sets the country in which the {@code Customer} is located.
     * <p>
     * The data type of this field is a string, with a maximum length of thirty
     * (30) characters.
     * <dl>
     * <dt>Field Requirements</dt>
     * <dd>If the {@link #getCustomerType() customerType} property is set for a
     * broker or agent, then this field is <em>optional</em> and {@code null}
     * values are permitted.<br>
     * <br>
     * If this {@code Customer} is of any other {@code customerType}, then this
     * is a <strong>required</strong> value.</dd></dl>
     * <p>
     * If the {@code customerType} <strong>is not</strong> set nor set to a
     * broker or agent, and {@code country} is blank, empty, or {@code null},
     * a {@link NullPointerException} is thrown.
     * <p>
     * This is a bound property.
     * 
     * @param country 
     */
    public void setCountry(String country) {
        if (!isBrokerOrAgent() && (country == null || country.isBlank()
                || country.isEmpty())) {
            throw new NullPointerException("country is blank, empty, or null, "
                    + "and the customer type is not a broker or agent.");
        }
        String old = getCountry();
        this.country = country;
        firePropertyChange("country", old, getCountry());
    }

    /**
     * Retrieves the name of the contact person at the {@code Customer}.
     * 
     * @return the contact person's name
     */
    public String getContact() {
        return contact;
    }

    /**
     * Sets the name of the contact person at the {@code Customer}. This is an
     * <em>optional</em> field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters.
     * <p>
     * This is a bound property.
     * 
     * @param contact the contact person's name
     */
    public void setContact(String contact) {
        String old = getContact();
        this.contact = contact;
        firePropertyChange("contact", old, getContact());
    }

    /**
     * Retrieves the phone number for the {@code Customer}.
     * 
     * @return the phone number
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the phone number for the {@code Customer}. This is an <em>optional
     * </em> field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of twenty
     * (20) characters. If this field <em>is</em> used, the value is required to
     * be a valid US or Canadian phone number, with or without an international
     * dialing prefix, and any valid phone number format.
     * <p>
     * This is a bound property.
     * 
     * @param phone the phone number
     */
    public void setPhone(String phone) {
        String old = getPhone();
        this.phone = phone;
        firePropertyChange("phone", old, getPhone());
    }

    /**
     * Retrieves the fax number for the {@code Customer}.
     * 
     * @return the fax number
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the fax number for the {@code Customer}. This is an <em>optional
     * </em> field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of twenty
     * (20) characters. If this field <em>is</em> used, the value is required to
     * be a valid US or Canadian fax number, with or without an international
     * dialing prefix, and any valid fax number format.
     * <p>
     * This is a bound property.
     * 
     * @param fax the fax number
     */
    public void setFax(String fax) {
        String old = getFax();
        this.fax = fax;
        firePropertyChange("fax", old, getFax());
    }

    /**
     * Retrieves the email address for the {@code Customer}.
     * 
     * @return the email address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email address for the {@code Customer}. This is an <em>optional
     * </em> field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length of forty
     * (40) characters. If this field <em>is</em> used, the value is required to
     * be a valid email address.
     * <p>
     * This is a bound property.
     * 
     * @param email the email address
     */
    public void setEmail(String email) {
        String old = getEmail();
        this.email = email;
        firePropertyChange("email", old, getEmail());
    }

    /**
     * Retrieves the notes about this {@code Customer}.
     * 
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the notes about this {@code Customer}. This is an <em>optional</em>
     * field and {@code null} values are permitted.
     * <p>
     * The data type of this field is a string, with a maximum length defined by
     * the system on which the application is running, but typically measured in
     * gigabytes of data. This field allows for free-form text.
     * <p>
     * This is a bound property.
     * 
     * @param notes the notes
     */
    public void setNotes(String notes) {
        String old = getNotes();
        this.notes = notes;
        firePropertyChange("notes", old, getNotes());
    }

    /**
     * Retrieves the default {@link Account} for this {@code Customer}.
     * 
     * @return the default account
     */
    public Account getDefaultAccount() {
        return Account.findByPrimaryKey(defaultAccount);
    }

    /**
     * Sets the default {@link Account} for this {@code Customer}'s financial 
     * transactions. This is a <strong>required</strong> field.
     * <p>
     * This is a bound property.
     * 
     * @param defaultAccount the default account
     */
    public void setDefaultAccount(Account defaultAccount) {
        if (defaultAccount == null) {
            throw new NullPointerException("defaultAccount is null.");
        }
        this.defaultAccount = defaultAccount.getAccountNumber();
    }

    /**
     * Retrieves the {@link CustomerType} of this {@code Customer}.
     * 
     * @return the customer type
     */
    public CustomerType getCustomerType() {
        return CustomerType.findByPrimaryKey(customerType);
    }

    /**
     * Sets the {@link CustomerType} for this {@code Customer}. This is a 
     * <strong>required</strong> field.
     * <p>
     * This is a bound property.
     * 
     * @param customerType the customer type
     */
    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType.getTypeId();
    }

    /**
     * Determines whether this {@code Customer} is no longer an active customer.
     * 
     * @return {@code true} if the customer is no longer active
     */
    public boolean isInactive() {
        return inactive;
    }

    /**
     * Sets whether this {@code Customer} is no longer active. This is a 
     * <strong>required</strong> field, with a default value of {@code false}.
     * <p>
     * The data type of this field is a boolean.
     * <p>
     * This is a bound property.
     * 
     * @param inactive {@code true} to deactivate the customer
     */
    public void setInactive(boolean inactive) {
        boolean old = isInactive();
        this.inactive = inactive;
        firePropertyChange("inactive", old, isInactive());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (customerId != null ? customerId.hashCode() ^ 5 : 0);
        hash += (company != null ? company.hashCode() ^ 5 : 0);
        hash += (street != null ? street.hashCode() ^ 5 : 0);
        hash += (city != null ? city.hashCode() ^ 5 : 0);
        hash += (region != null ? region.hashCode() ^ 5 : 0);
        hash += (postalCode != null ? postalCode.hashCode() ^ 5 : 0);
        hash += (country != null ? country.hashCode() ^ 5 : 0);
        hash += (defaultAccount != null ? defaultAccount.hashCode() ^ 5 : 0);
        hash += (customerType != null ? customerType.hashCode() ^ 5 : 0);
        hash += (inactive ? 0 : 1);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.customerId == null && other.customerId != null)
                || (this.customerId != null 
                && !this.customerId.equals(other.customerId))) {
            return false;
        }
        if ((this.company == null && other.company != null)
                || (this.company != null && !this.company.equals(other.company))) {
            return false;
        }
        if ((this.street == null && other.street != null)
                || (this.street != null && !this.street.equals(other.street))) {
            return false;
        }
        if ((this.city == null && other.city != null)
                || (this.city != null && !this.city.equals(other.city))) {
            return false;
        }
        if ((this.region == null && other.region != null)
                || (this.region != null && !this.region.equals(other.region))) {
            return false;
        }
        if ((this.postalCode == null && other.postalCode != null)
                || (this.postalCode != null 
                && !this.postalCode.equals(other.postalCode))) {
            return false;
        }
        if ((this.country == null && other.country != null)
                || (this.country != null && !this.country.equals(other.country))) {
            return false;
        }
        if ((this.getDefaultAccount() == null && other.getDefaultAccount() != null)
                || (this.getDefaultAccount() != null 
                && !this.getDefaultAccount().equals(other.getDefaultAccount()))) {
            
        }
        return ((this.getCustomerType() == null && other.getCustomerType() != null)
                || (this.getCustomerType() != null &&
                !this.getCustomerType().equals(other.getCustomerType())));
    }

    @Override
    public String toString() {
        return company + " [" + customerId + "] (" + city + ", " + region + ")";
    }

    /**
     * Describes this {@code Customer} in manner useful for debugging
     * purposes. If a UI display string is desired, use
     * {@link #toString() toString} instead.
     * 
     * @return a debugging string that describes this object
     */
    public String debuggingString() {
        return ObjectUtils.debuggingString(this);
    }
    
    /**
     * Saves this {@code Customer} record to the database.
     * <p>
     * Prior to saving the record, its data is validated, including whether its
     * {@link #getTypeName() typeName} value is unique in the table for new and
     * updated records. If the record passes validation, it will be inserted or
     * updated in the table.
     * <p>
     * If the record fails validation, a {@link ValidationException} is thrown.
     * 
     * @param nue boolean to notify if this record is new or existing
     * 
     * @throws ValidationException if the record fails data validation
     */
    public void saveRecord(boolean nue) throws ValidationException {
        if (nue) {
            if (!isUniqueInTable()) {
                throw new ValidationException(this, 
                        resourceMap.getString("not.unique", customerId));
            }
            insert();
        } else {
            validate();
            update();
        }
    }
    
    private void insert() {
        String sql = String.format(INSERT_PREFIX, SCHEMA, TABLE_NAME);
        sql += "(" + FLD_ID + ", " + FLD_NAME + ", " + FLD_STREET + ", " 
                + FLD_SUITE + ", " + FLD_CITY + ", " + FLD_REGION + ", " 
                + FLD_POSTAL + ", " + FLD_COUNTRY + ", " + FLD_CONTACT + ", "
                + FLD_PHONE + ", " + FLD_FAX + ", " + FLD_EMAIL + ", "
                + FLD_NOTES + ", " + FLD_ACCT + ", " + FLD_TYPE + ", "
                + FLD_INACT + ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                + "?, ?, ?)";
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, customerId);
            pst.setString(2, company);
            pst.setString(3, street);
            pst.setString(4, suite);
            pst.setString(5, city);
            pst.setString(6, region);
            pst.setString(7, postalCode);
            pst.setString(8, country);
            pst.setString(9, contact);
            pst.setString(10, phone);
            pst.setString(11, fax);
            pst.setString(12, email);
            pst.setString(13, notes);
            pst.setString(14, defaultAccount);
            pst.setLong(15, customerType);
            pst.setBoolean(16, inactive);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("insert.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("insert.error.title");
            String category = resourceMap.getString("category.db.insert");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Customer.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void update() {
        String sql = String.format(UPDATE_PREFIX, SCHEMA, TABLE_NAME);
        sql += FLD_NAME + " = ?, " + FLD_STREET + " = ?, " 
                + FLD_SUITE + " = ?, " + FLD_CITY + " = ?, " + FLD_REGION + " = ?, " 
                + FLD_POSTAL + " = ?, " + FLD_COUNTRY + " = ?, " + FLD_CONTACT + " = ?, "
                + FLD_PHONE + " = ?, " + FLD_FAX + " = ?, " + FLD_EMAIL + " = ?, "
                + FLD_NOTES + " = ?, " + FLD_ACCT + " = ?, " + FLD_TYPE + " = ?, "
                + FLD_INACT + " = ? WHERE " + FLD_ID + " = " + customerId;
        
        Connection conn = null;
        PreparedStatement pst = null;
        
        try {
            conn = Database.getInstance().getConnection();
            conn.setAutoCommit(false);
            
            pst = conn.prepareStatement(sql);
            
            pst.setString(1, company);
            pst.setString(2, street);
            pst.setString(3, suite);
            pst.setString(4, city);
            pst.setString(5, region);
            pst.setString(6, postalCode);
            pst.setString(7, country);
            pst.setString(8, contact);
            pst.setString(9, phone);
            pst.setString(10, fax);
            pst.setString(11, email);
            pst.setString(12, notes);
            pst.setString(13, defaultAccount);
            pst.setLong(14, customerType);
            pst.setBoolean(15, inactive);
            pst.setString(16, customerId);
            
            pst.executeUpdate();
            
            conn.commit();
        } catch (SQLException e) {
            // First, get the message that will be used from here on.
            String msg = resourceMap.getString("update.error.message",
                    e.getSQLState(), e.getLocalizedMessage(), sql);
            
            // Fire a status message to show the message on the statusbar.
            StatusMessage sm = new StatusMessage(msg, MessageType.ERROR);
            context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
            
            // Get the title, category, and build the state map.
            String title = resourceMap.getString("update.error.title");
            String category = resourceMap.getString("category.db.update");
            Map<String, String> state = new HashMap<>();
            state.put("Class", Customer.class.getName());
            state.put("Database", Database.getInstance().getDbName());
            state.put("Schema", SCHEMA);
            state.put("Table", TABLE_NAME);
            state.put("Connection", String.valueOf(conn));
            state.put("Statement", String.valueOf(pst));
            state.put("SQL Statement", sql);
            
            // Create and fire a notification that provides more details.
            ErrorInfo info = new ErrorInfo(title, e.getLocalizedMessage(),
                    msg, category, e, ErrorLevel.ERROR, state);
            context.firePropertyChange(KEY_ADD_NOTIFICATION, null, info);
            
            // Log the error for future reference.
            msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                    + "Statement: %4$s", e.getSQLState(),
                    e.getClass().getSimpleName(), e.getMessage(), sql);
            logger.log(Level.WARNING, msg, e);
            
            try {
                conn.rollback();
            } catch (SQLException ex) {
                msg = resourceMap.getString("rollback.error.message",
                        ex.getSQLState(), ex.getLocalizedMessage(), sql);

                // Fire a status message to show the message on the statusbar.
                sm = new StatusMessage(msg, MessageType.ERROR);
                context.firePropertyChange(KEY_STATUS_MESSAGE, null, sm);
                
                // Log the error for future reference.
                msg = String.format("[%1$s] %2$s: Error inserting data: %3$s :: "
                        + "Statement: %4$s", ex.getSQLState(),
                        ex.getClass().getSimpleName(), ex.getMessage(), sql);
                logger.log(Level.WARNING, msg, e);
            }
        } finally {
            try {
                if (pst != null) {
                    pst.close();
                    pst = null;
                }
                if (conn != null) {
                    conn.setAutoCommit(true);
                    conn.close();
                    conn = null;
                }
            } catch (SQLException ignore) {
                // Actively ignoring this exception.
            }
        }
    }
    
    private void validate() throws ValidationException {
        if (customerId == null || customerId.isBlank() || customerId.isEmpty()) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "customerId"));
        }
        if (getCustomerType() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "customerType"));
        }
        if (getDefaultAccount() == null) {
            throw new ValidationException(this, 
                    resourceMap.getString("required.value.null", "defaultAccount"));
        }
        // Validate all other fields that are required for non-broker/agent.
        if (!getCustomerType().getTypeName().toLowerCase().contains("broker")
                && !getCustomerType().getTypeName().toLowerCase().contains("agent")) {
            if (company == null || company.isBlank() || company.isEmpty()) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", "company"));
            }
            if (street == null || street.isBlank() || street.isEmpty()) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", "street"));
            }
            if (city == null || city.isBlank() || city.isEmpty()) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", "city"));
            }
            if (region == null || region.isBlank() || region.isEmpty()) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", "region"));
            }
            if (postalCode == null || postalCode.isBlank() || postalCode.isEmpty()) {
                throw new ValidationException(this, 
                        resourceMap.getString("required.value.null", "postalCode"));
            }
            if (country == null || country.isBlank() || country.isEmpty()) {
                country = "USA"; // Default value
            }
        }
    }
    
    private boolean isUniqueInTable() {
        List<Customer> records = getAllRecords();
        int counter = 0;
        for (Customer record : records) {
            if (record.getCustomerId().startsWith(customerId)) {
                counter++;
            }
        }
        
        if (counter == 0) {
            return true;
        } else {
            if (counter < 10) {
                customerId += "0" + counter;
                return true;
            } else if (counter < 100) {
                customerId += counter;
                return true;
            } else {
                return false;
            }
        }
    }
    
    private boolean isBrokerOrAgent() {
        CustomerType type = getCustomerType();
        return type.getTypeName().toLowerCase().contains("broker")
                || type.getTypeName().toLowerCase().contains("agent");
    }

    private String customerId;
    private String company;
    private String street;
    private String suite;
    private String city;
    private String region;
    private String postalCode;
    private String country;
    private String contact;
    private String phone;
    private String fax;
    private String email;
    private String notes;
    private String defaultAccount;
    private Long customerType;
    private boolean inactive = false;

}
