/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   NtosStartupModule.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 19, 2024
 *  Modified   :   Jul 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.startup;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.SysExits;
import com.pekinsoft.ntos.NtosApplication;
import com.pekinsoft.utils.ArgumentParser;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Sean Carrick
 */
public class Launcher {

    // Set ntos-startup-module as the MAIN PROJECT in NetBeans ID. This way we
    //+ can simply click on the run or debug buttons regardless of which project
    //+ we are in within the parent project.
    
    public static void main(String[] args) {
        /* TODO: For now, we are simply launching. Read below for more info...
         * 
         * We need to figure out how to use Process and ProcessBuilder to launch
         * NTOS so that we can continue to re-launch it as long as its exit 
         * status is one of the SysExits restart constants. I have successfully
         * done this in an Ant project, but it was not launching from JAr files,
         * but from the exploded build directory. It worked great in that 
         * context, but from a JAr on the command line it did not work.
         * 
         * Since Maven uses the built JArs to run the project, even in the IDE,
         * we may be able to figure out how to make this work now. Either way,
         * we need this startup module in order to prevent cyclic dependencies,
         * since the startup module sets the classpath by depending on all 
         * modules that make up the application, and modules will need to have
         * access to ntos-base-module for the plugins system.
         */
        ArgumentParser parser = new ArgumentParser(args);
        if (parser.isSwitchPresent("--ide")) {
            Application.launch(NtosApplication.class, args);
        } else {
            try {
                String java = System.getProperty("java.home");
                if (!java.endsWith(File.separator)) {
                    java += File.separator;
                }
                java += "bin" + File.separator + "java";
                String classPath = System.getProperty("java.class.path");
                String jarName = Launcher.class.getProtectionDomain().getCodeSource()
                        .getLocation().toURI().getPath();

                List<String> commandLine = new ArrayList<>();
                commandLine.add(java);
                URI splash = NtosApplication.class.getClassLoader().getResource(
                        "com/pekinsoft/ntos/resources/splash.png").toURI();
                commandLine.add("-splash:" + splash);
                commandLine.add("-cp");
                commandLine.add(classPath);
                if (jarName.endsWith(".jar")) {
                    commandLine.add(jarName);
                }
                commandLine.add("com.pekinsoft.ntos.NtosApplication");
                commandLine.addAll(Arrays.asList(args));

                ProcessBuilder pb = new ProcessBuilder(commandLine);

                try {
                    Process proc = pb.start();
                    proc.waitFor();

                    while (proc.exitValue() == SysExits.EX_RESTART.getValue()
                            || proc.exitValue() == SysExits.EX_RESTART_UPDATE.getValue()) {
                        proc = pb.start();
                        proc.waitFor();
                    }

                    Runtime.getRuntime().exit(proc.exitValue());
                } catch (IOException | InterruptedException e) {
                    System.err.println("Exception: " + e.getMessage());
                    Throwable cause = e;
                    while (cause != null) {
                        System.err.println("Message: " + e.getMessage());
                        for (StackTraceElement el : e.getStackTrace()) {
                            System.err.println("\t" + el.toString());
                        }
                        cause = cause.getCause();
                    }
                }
            } catch (URISyntaxException e) {
                System.err.println(e);
            }
        }
    }
}
