# Project TODOs
## Northwind Truckers' Operating System
### (NTOS)

### General Work

In each class on the "one" side of a one-to-many relationship, I have added a
method to retrieve all of the related records. This is a *read-only* property. I
have added this property to the following classes:

- `AccountType`
- `CustomerType`
- `DepreciationType`
- `EmploymentType`
- `Load`
- `ServiceType`
- `UnitType`
- `TxType`

While doing this work, I have updated the table creation files to **not have**
the database tables doing the `null` nor unique checks, exception for the primary
key field because that is required by the database system. All of this validation
is now done in the DAO classes themselves on the setter and on save.

### Odometer Readings

I have updated the `Unit` DAO class, as well as the `UNITS` create file, to have
the odometer reading stored in the `Unit` class. Now, whenever an odometer
reading needs to be stored, it will simply update the value in the 
`Unit.odometer` property. I have updated the `FuelPurchase` class, as well as 
the `Fuel_Purchases.create` file to reflect this change.

***NOTE***: On this iteration of the DAO API, I am removing all `NOT NULL` 
requirements from the table creation scripts. This is because I am doing all of 
the data validation and integrity checks in the objects themselves and having it
performed on the database is simply redundant. Besides, by not having the database
perform these checks, operation should be sped up a bit to eliminate lag as the
tables grow in size.

***NOTE***: I have also changed the names of the `*.sample` files to `*.defaults`,
because I think those should be available so the user can install the default
chart of account and various types. I have edited those files to contain numerous
records, which should be helpful.

### Next in the Data Module

All that I have left to do in the `ntos-data-module` project is to modify the
`Database` class to work more in the way I am thinking now.
