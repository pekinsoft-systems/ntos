/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pekinsoft.ntos.common.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author sean
 */
public class JLimitedTextField extends JTextField {

    public JLimitedTextField(int maxLength) {
        this.maxLength = maxLength;
        firstWarning = 2 * Math.round(maxLength / 3);
        lastWarning = 4 * Math.round(maxLength / 5);
        this.msgFont = new Font("Arial", Font.PLAIN, 9);
        this.message = "Characters Remaining: " + maxLength;

        this.problem = new JLabel();

        this.getDocument().addDocumentListener(new LimitedDocumentListener());

        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (getText().length() >= lastWarning) {
                    problem.setForeground(Color.red.darker().darker().darker());
                } else if (getText().length() >= firstWarning) {
                    problem.setForeground(Color.orange.darker().darker().darker());
                } else {
                    problem.setForeground(Color.blue);
                }
                System.out.println("Checking length of text.");
                if (getText().length() >= maxLength) {
                    e.consume();
                }
            }
        });

        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                getParent().remove(problem);
                revalidate();
                repaint();
            }

            @Override
            public void focusGained(FocusEvent e) {
                problem.setFont(msgFont);
                problem.setForeground(Color.blue);
                problem.setLocation(getLocation().x + problem.getWidth(), getLocation().y + getHeight());
                problem.setSize(getWidth(), getFontMetrics(msgFont).getHeight());
                problem.setText(message);

                getParent().add(problem);

                revalidate();
                repaint();
            }
        });
    }

    private void updateMessage() {
        int remaining = maxLength - getText().length();
        String newMsg = "Characters Remaining: " + remaining;
        System.out.println("newMsg = " + newMsg);

//        if (!newMsg.equals(message)) {
        message = newMsg;
        problem.setText(message);
//        }
    }

    private final JLabel problem;
    private final int maxLength;
    private final int firstWarning;
    private final int lastWarning;
    private final Font msgFont;

    private String message;

    private class LimitedDocumentListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            updateMessage();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateMessage();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateMessage();
        }

    }

}
