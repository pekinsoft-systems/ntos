/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-common-module
 *  Class      :   MarkdownEditorKit.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 21, 2024
 *  Modified   :   Oct 21, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 21, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.common.ui.text;

import javax.swing.text.*;
import java.io.*;
import java.util.List;
import org.commonmark.node.*;
import org.commonmark.node.AbstractVisitor;
import org.commonmark.ext.gfm.tables.TableBlock;
import org.commonmark.ext.gfm.tables.TableHead;
import org.commonmark.ext.gfm.tables.TableBody;
import org.commonmark.ext.gfm.tables.TableRow;
import org.commonmark.ext.gfm.tables.TableCell;
import org.commonmark.ext.gfm.tables.*;

public class MarkdownEditorKit extends StyledEditorKit {

    private org.commonmark.parser.Parser parser;

    public MarkdownEditorKit() {
        // Enable GFM tables extension
        parser = org.commonmark.parser.Parser.builder().extensions(List.of(
                TablesExtension.create()))
                .build();
    }

    @Override
    public void read(Reader in, javax.swing.text.Document doc, int pos)
            throws IOException, BadLocationException {
        // Parse Markdown
        BufferedReader reader = new BufferedReader(in);
        StringBuilder markdownContent = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            markdownContent.append(line).append("\n");
        }

        // Parse markdown with tables
        Node document = parser.parse(markdownContent.toString());

        // Clear the document first
        doc.remove(0, doc.getLength());

        // Traverse nodes and render them
        renderMarkdownToDocument(document, (StyledDocument) doc);
    }

    private void renderMarkdownToDocument(Node document, StyledDocument doc)
            throws BadLocationException {
        TableVisitor visitor = new TableVisitor(doc);
        document.accept(visitor);
    }

    @Override
    public javax.swing.text.Document createDefaultDocument() {
        return new DefaultStyledDocument();
    }

    @Override
    public String getContentType() {
        return "text/markdown";
    }

    private static class TableVisitor extends AbstractVisitor {

        private StyledDocument doc;
        private SimpleAttributeSet defaultAttributes;

        public TableVisitor(StyledDocument doc) {
            this.doc = doc;
            this.defaultAttributes = new SimpleAttributeSet();
            StyleConstants.setFontFamily(defaultAttributes, "Monospaced");
            StyleConstants.setFontSize(defaultAttributes, 12);
        }

        @Override
        public void visit(CustomNode customNode) {
            switch (customNode) {
                case TableHead tableHead -> visitTableHead(tableHead);
                case TableBody tableBody -> visitTableBody(tableBody);
                case TableRow tableRow -> visitTableRow(tableRow);
                case TableCell tableCell -> visitTableCell(tableCell);
                default -> // Visit other customNodes (non-table-related)
                    super.visit(customNode);
            }
        }

        @Override
        public void visit(CustomBlock customBlock) {
            if (customBlock instanceof TableBlock block) {
                visitTableBlock(block);
            } else {
                super.visit(customBlock);
            }
        }

        private void visitTableBlock(TableBlock tableBlock) {
            // Render the table block by visiting its children
            visitChildren(tableBlock);
        }

        private void visitTableHead(TableHead tableHead) {
            // Render the table head (header row)
            visitChildren(tableHead);
        }

        private void visitTableBody(TableBody tableBody) {
            // Render the table body (content rows)
            visitChildren(tableBody);
        }

        private void visitTableRow(TableRow tableRow) {
            // Render a table row
            visitChildren(tableRow);

            // After each row, add a line break for separation
            try {
                doc.insertString(doc.getLength(), "\n", defaultAttributes);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        private void visitTableCell(TableCell tableCell) {
            StringBuilder cellContent = new StringBuilder();

            // Visit the child nodes of the TableCell to extract the text
            tableCell.accept(new AbstractVisitor() {
                @Override
                public void visit(Text text) {
                    cellContent.append(text.getLiteral());
                }

                // You can handle other inline elements (emphasis, strong, etc.) here as needed
            });

            // Render the extracted text
            try {
                String formattedCell = formatCell(cellContent.toString());
                doc.insertString(doc.getLength(), formattedCell,
                        defaultAttributes);
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }

        private String formatCell(String cellText) {
            // Add spacing for table cell formatting
            return String.format("| %-10s ", cellText);
        }
    }
}
