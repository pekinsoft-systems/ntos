/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-common-module
 *  Class      :   Terrains.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 19, 2024
 *  Modified   :   Oct 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.common;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public enum Terrains {
    
    PLAINS,
    PRAIRIE,
    ROLLING_PRAIRIE,
    HILLY,
    FOOTHILLS,
    SHORTER_MOUNTAINS,
    TALLER_MOUNTAINS,
    COASTAL,
    DESERT,
    TUNDRA;
    
    public String toString() {
        return switch (this) {
            case COASTAL -> "Coastal plains";
            case DESERT -> "Southwestern deserts";
            case FOOTHILLS -> "Foothills";
            case HILLY -> "Rolling hills";
            case PLAINS -> "Flat plains";
            case PRAIRIE -> "Fairly flat prairies";
            case ROLLING_PRAIRIE -> "Rolling prairies, very small hills";
            case SHORTER_MOUNTAINS -> "Shorter mountains (eastern)";
            case TALLER_MOUNTAINS -> "Taller mountains (western)";
            case TUNDRA -> "Northern Canadian tundra";
            default -> "UNKNOWN CONSTANT";
        };
    }
    
    public static Terrains of(String value) {
        return switch (value) {
            case "Coastal plains" -> COASTAL;
            case "Southwestern deserts" -> DESERT;
            case "Foothills" -> FOOTHILLS;
            case "Rolling hills" -> HILLY;
            case "Flat plains" -> PLAINS;
            case "Fairly flat prairies" -> PRAIRIE;
            case "Rolling prairies, very small hills" -> ROLLING_PRAIRIE;
            case "Shorter mountains (eastern)" -> SHORTER_MOUNTAINS;
            case "Taller mountains (western)" -> TALLER_MOUNTAINS;
            case "Northern Canadian tundra" -> TUNDRA;
            default -> PRAIRIE;
        };
    }
    
}
