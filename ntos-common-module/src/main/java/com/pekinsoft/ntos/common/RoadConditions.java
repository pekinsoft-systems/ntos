/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-common-module
 *  Class      :   RoadConditions.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 19, 2024
 *  Modified   :   Oct 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.common;

/**
 * {@code RoadConditions} define some constants for use in the application to
 * explain the conditions found on the road. Some of these conditions include
 * things like sunny, rainy, icy, snowy, road construction, light traffic, heavy
 * traffic, etc.
 * <p>
 * There are some constants that combine other conditions to give a better
 * picture of the conditions on the road.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public enum RoadConditions {
    
    SUNNY_LIGHT_TRAFFIC,
    SUNNY_HEAVY_TRAFFIC,
    SUNNY_IN_TOWN,
    SUNNY_EXPRESSWAY,
    SUNNY_HIGHWAY,
    RAINY_LIGHT_TRAFFIC,
    RAINY_HEAVY_TRAFFIC,
    RAINY_IN_TOWN,
    RAINY_EXPRESSWAY,
    RAINY_HIGHWAY,
    SNOWY_LIGHT_TRAFFIC,
    SNOWY_HEAVY_TRAFFIC,
    SNOWY_IN_TOWN,
    SNOWY_EXPRESSWAY,
    SNOWY_HIGHWAY,
    ICY_LIGHT_TRAFFIC,
    ICY_HEAVY_TRAFFIC,
    ICY_IN_TOWN,
    ICY_EXPRESSWAY,
    ICY_HIGHWAY;
    
    @Override
    public String toString() {
        return switch(this) {
            case ICY_EXPRESSWAY -> "Icy, interstate/expressway";
            case ICY_HEAVY_TRAFFIC -> "Icy, heavy traffic";
            case ICY_HIGHWAY -> "Icy, two-lane highway";
            case ICY_IN_TOWN -> "Icy, in-town, surface streets";
            case ICY_LIGHT_TRAFFIC -> "Icy, light traffic";
            case RAINY_EXPRESSWAY -> "Rainy, interstate/expressway";
            case RAINY_HEAVY_TRAFFIC -> "Rainy, heavy traffic";
            case RAINY_HIGHWAY -> "Rainy, two-lane highway";
            case RAINY_IN_TOWN -> "Rainy, in-town, surface streets";
            case RAINY_LIGHT_TRAFFIC -> "Rainy, light traffic";
            case SNOWY_EXPRESSWAY -> "Snowy, interstate/expressway";
            case SNOWY_HEAVY_TRAFFIC -> "Snowy, heavy traffic";
            case SNOWY_HIGHWAY -> "Snowy, two-lane highway";
            case SNOWY_IN_TOWN -> "Snowy, in-town, surface streets";
            case SNOWY_LIGHT_TRAFFIC -> "Snowy, light traffic";
            case SUNNY_EXPRESSWAY -> "Sunny, dry interstate/expressway";
            case SUNNY_HEAVY_TRAFFIC -> "Sunny, dry with heavy traffic";
            case SUNNY_HIGHWAY -> "Sunny, dry two-lane highway";
            case SUNNY_IN_TOWN -> "Sunny, dry in-town on surface streets";
            case SUNNY_LIGHT_TRAFFIC -> "Sunny, dry with light traffic";
            default -> "UNKNOWN CONSTANT";
        };
    }
    
    public static RoadConditions of(String value) {
        return switch(value) {
            case "Icy, interstate/expressway" -> ICY_EXPRESSWAY;
            case "Icy, heavy traffic" -> ICY_HEAVY_TRAFFIC;
            case "Icy, two-lane highway" -> ICY_HIGHWAY;
            case "Icy, in-town, surface streets" -> ICY_IN_TOWN;
            case "Icy, light traffic" -> ICY_LIGHT_TRAFFIC;
            case "Rainy, interstate/expressway" -> RAINY_EXPRESSWAY;
            case "Rainy, heavy traffic" -> RAINY_HEAVY_TRAFFIC;
            case "Rainy, two-lane highway" -> RAINY_HIGHWAY;
            case "Rainy, in-town, surface street" -> RAINY_IN_TOWN;
            case "Rainy, light traffic" -> RAINY_LIGHT_TRAFFIC;
            case "Snowy, interstate/expressway" -> SNOWY_EXPRESSWAY;
            case "Snowy, heavy traffic" -> SNOWY_HEAVY_TRAFFIC;
            case "Snowy, two-lane highway" -> SNOWY_HIGHWAY;
            case "Snowy, in-town, surface streets" -> SNOWY_IN_TOWN;
            case "Snowy, light traffic" -> SNOWY_LIGHT_TRAFFIC;
            case "Sunny, dry interstate/expressway" -> SUNNY_EXPRESSWAY;
            case "Sunny, dry with heavy traffic" -> SUNNY_HEAVY_TRAFFIC;
            case "Sunny, dry two-lane highway" -> SUNNY_HIGHWAY;
            case "Sunny, dry in-town on surface streets" -> SUNNY_IN_TOWN;
            case "Sunny, dry with light traffic" -> SUNNY_LIGHT_TRAFFIC;
            default -> SUNNY_LIGHT_TRAFFIC;
        };
    }
    
}
