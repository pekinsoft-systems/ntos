/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-common-module
 *  Class      :   CommonModulePlugin.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 17, 2024
 *  Modified   :   Jul 17, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 17, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.common;

import com.pekinsoft.api.Plugin;
import java.util.Collections;
import java.util.List;
import javax.help.HelpSet;
import javax.swing.ActionMap;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class CommonModulePlugin implements Plugin {
    
    public CommonModulePlugin () {
        
    }

    @Override
    public void install() {
        
    }

    @Override
    public void ready() {
        
    }

    @Override
    public void activate() {
        
    }

    @Override
    public void deactivate() {
        
    }

    @Override
    public boolean isDeactivated() {
        return false;
    }

    @Override
    public void uninstall() {
        
    }

    @Override
    public boolean isUninstalled() {
        return false;
    }

    @Override
    public List<ActionMap> mergeGlobalActions() {
        return Collections.emptyList();
    }

    @Override
    public List<HelpSet> mergeHelpSets() {
        return Collections.emptyList();
    }

    @Override
    public String getPluginName() {
        return "NTOS Common Module (<em>Required</em>)";
    }

    @Override
    public String getPluginVersion() {
        return "0.5.1";
    }

}
