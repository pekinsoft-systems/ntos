/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-common-module
 *  Class      :   TableCellListener.java
 *  Author     :   Borrowed from tips4java at github.com
 *  Created    :   Jul 20, 2024
 *  Modified   :   Jul 20, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 20, 2024  tips4java            Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.common.helpers;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

/**
 * Listens for changes made to the data in the table via the TableCellEditor.
 * When editing is started, the value of the cell is saved. When editing is
 * stopped, the new value is saved. When the old and new values are different,
 * then the provided Action is invoked.
 * <p>
 * The source of the Action is a TableCellListener instance.
 *
 * @author tips4java
 *
 * @version 1.0
 * @since 1.0
 */
public class TableCellListener implements PropertyChangeListener, Runnable {

    /**
     * Create a TableCellListener.
     *
     * @param table  the table to be monitored for data changes
     * @param action the Action to invoke when cell data is changed
     */
    public TableCellListener(JTable table, Action action) {
        this.table = table;
        this.action = action;
        this.table.addPropertyChangeListener(this);
    }

    private TableCellListener(JTable table, int row, int column, Object oldValue,
            Object newValue) {
        this.table = table;
        this.row = row;
        this.column = column;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    /**
     * Get the table of the cell that was changed.
     *
     * @return the table
     */
    public JTable getTable() {
        return table;
    }

    /**
     * Get the row that was last edited.
     *
     * @return the row
     */
    public int getRow() {
        return row;
    }

    /**
     * Get the column that was last edited.
     *
     * @return the column
     */
    public int getColumn() {
        return column;
    }

    /**
     * Get the old value of the cell.
     *
     * @return the old value
     */
    public Object getOldValue() {
        return oldValue;
    }

    /**
     * Get the new value of the cell.
     *
     * @return the new value
     */
    public Object getNewValue() {
        return newValue;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if ("tableCellEditor".equals(evt.getPropertyName())) {
            if (table.isEditing()) {
                processEditingStarted();
            } else {
                processEditingStopped();
            }
        }
    }

    /*
     * Save information of the cell about to be edited.
     */
    private void processEditingStarted() {
        /*
         * The invokeLater is necessary because the editing row and editing
         * column of hte table have not been set when the "tableCellEditor"
         * PropertyChangeEvent is fired.
         *
         * This results in the "run" method being invoked.
         */
        SwingUtilities.invokeLater(this);
    }

    @Override
    public void run() {
        row = table.convertColumnIndexToModel(table.getEditingRow());
        column = table.convertColumnIndexToModel(table.getEditingColumn());
        oldValue = table.getModel().getValueAt(row, column);
        newValue = null;
    }

    /*
     * Update the cell history when necessary.
     */
    private void processEditingStopped() {
        newValue = table.getModel().getValueAt(row, column);
        if (newValue == null && oldValue == null) {
            return;
        }
        if (newValue == null || !newValue.equals(oldValue)) {
            // Make a copy of the data in case another cell starts editing
            //+ while processing the change.
            TableCellListener tcl = new TableCellListener(getTable(), getRow(),
                    getColumn(), getOldValue(), getNewValue());

            ActionEvent event = new ActionEvent(tcl,
                    ActionEvent.ACTION_PERFORMED,
                    "");
            action.actionPerformed(event);
        }
    }

    private JTable table;
    private Action action;

    private int row;
    private int column;
    private Object oldValue;
    private Object newValue;

}
