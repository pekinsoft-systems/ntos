/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   NtosPreferenceKeys.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 16, 2024
 *  Modified   :   Jul 16, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 16, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.common;

import com.pekinsoft.api.PreferenceKeys;

/**
 *
 * @author Sean Carrick
 */
public interface NtosPreferenceKeys extends PreferenceKeys {
    
    static final String PREF_DATE_TIME_FORMAT = "formats.date.time";
    static final String PREF_LOOK_AND_FEEL = "Application.lookAndFeel";
    
    static final String PREF_STATUSBAR_VISIBLE = "statusbar.visible";
    static final String PREF_SHOW_BUTTON_TEXT = "toolbar.showButtonText";
    
    // Company Policy Keys:
    static final String PREF_MINIMUM_WORK_AGE = "minimum.employment.age";
    static final String PREF_AUDIT_EXPL_MIN_LEN = "audits.explanation.minimum.length";
}
