/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   ApplicationActions.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 15, 2024
 *  Modified   :   Jul 15, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 15, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.actions;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.ntos.view.AboutDialog;
import com.pekinsoft.ntos.view.OptionsDialog;
import com.pekinsoft.ntos.view.PluginManager;
import java.awt.event.ActionEvent;
import java.lang.System.Logger.Level;
import java.util.Objects;
import javax.swing.ActionMap;
import com.pekinsoft.ntos.common.NtosPreferenceKeys;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class ApplicationActions extends AbstractBean implements NtosPreferenceKeys {
    
    public static ApplicationActions getInstance() {
        if (instance == null) {
            instance = new ApplicationActions(Application.getInstance().getContext());
        }
        return instance;
    }
    
    private ApplicationActions (ApplicationContext context) {
        Objects.requireNonNull(context, "The ApplicationContext cannot be null.");
        this.context = context;
        buttonTextVisible = context.getPreferences(ApplicationContext.USER)
            .getBoolean(KEY_SHOW_BUTTON_TEXT, false);
        statusBarVisible = context.getPreferences(ApplicationContext.USER)
                .getBoolean(KEY_STATUSBAR_VISIBLE, true);
    }
    
    public ActionMap getActionMap() {
        return context.getActionMap(getClass(), this);
    }
    
    @AppAction(menuActionIndex = 127,
            menuBaseName = "help",
            menuSepBefore = true)
    public void showAboutDialog(ActionEvent evt) {
        AboutDialog dlg = new AboutDialog(context.getApplication().getMainFrame(), context);
        context.getApplication().show(dlg);
    }
    
    @AppAction(menuActionIndex = 127,
            menuBaseName = "tools",
            menuSepBefore = true)
    public void showOptionsDialog(ActionEvent evt) {
        context.getApplication().show(OptionsDialog.getInstance());
    }
    
    @AppAction(menuActionIndex = 126,
            menuBaseName = "tools",
            menuSepBefore = true)
    public void showPluginManager(ActionEvent evt) {
        System.getLogger(ApplicationActions.class.getName())
                .log(Level.INFO, "Showing the PluginManager...");
        context.getApplication().show(PluginManager.getInstance());
    }
    
    public boolean isStatusBarVisible() {
        return statusBarVisible;
    }
    
    public void setStatusBarVisible(boolean viewStatusBar) {
        boolean old = isStatusBarVisible();
        this.statusBarVisible = viewStatusBar;
        firePropertyChange("statusBarVisible", old, isStatusBarVisible());
    }
    
    @AppAction(selectedProperty = "statusBarVisible",
            menuActionIndex = 127,
            menuBaseName = "view",
            menuSepBefore = true)
    public void showStatusBar(ActionEvent evt) {
        context.getApplication().getMainView().getStatusBar().setVisible(
                isStatusBarVisible());
        context.getPreferences(ApplicationContext.USER)
                .putBoolean(KEY_STATUSBAR_VISIBLE, statusBarVisible);
    }
    
    private boolean buttonTextVisible = false;
    public boolean isButtonTextVisible() {
        return buttonTextVisible;
    }
    
    public void setButtonTextVisible(boolean buttonTextVisible) {
        boolean old = isButtonTextVisible();
        this.buttonTextVisible = buttonTextVisible;
        firePropertyChange("buttonTextVisible", old, isButtonTextVisible());
    }
    
    @AppAction(selectedProperty = "buttonTextVisible",
            menuActionIndex = 125,
            menuBaseName = "view",
            menuSepBefore = true)
    public void showButtonText(ActionEvent evt) {
        context.getPreferences(ApplicationContext.USER)
                .putBoolean(KEY_SHOW_BUTTON_TEXT, isButtonTextVisible());
        if (context.getApplication().getMainView() instanceof WindowManager wm) {
            wm.showButtonText(isButtonTextVisible());
        }
    }
    
    @AppAction(menuActionIndex = 126,
            menuBaseName = "view",
            menuSepAfter = true)
    public void showToolBars(ActionEvent evt) {
        ToolBarVisibility tbv = new ToolBarVisibility(context);
        tbv.setVisible(true);
    }
    
    private static ApplicationActions instance = null;
    
    private final ApplicationContext context;
    
    private boolean statusBarVisible = false;

}
