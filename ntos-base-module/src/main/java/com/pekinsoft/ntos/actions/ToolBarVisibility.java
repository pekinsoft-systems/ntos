/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   ToolBarVisibility.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 18, 2024
 *  Modified   :   Jul 18, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 18, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.actions;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.FrameView;
import com.pekinsoft.framework.ResourceMap;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.prefs.Preferences;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class ToolBarVisibility extends JDialog {
    
    public ToolBarVisibility (ApplicationContext context) {
        Objects.requireNonNull(context, "The ApplicationContext cannot be null.");
        this.context = context;
        
        initComponents();
    }
    
    private void initComponents() {
        setName("ToolBarVisibility");
        setLayout(new FlowLayout(FlowLayout.LEADING, 3, 3));
        setType(Window.Type.POPUP);
        setModal(true);
        setMaximumSize(new Dimension(135, Integer.MAX_VALUE));
        setMinimumSize(new Dimension(135, 125));
        
        List<String> tbNames = new ArrayList<>();
        List<JToolBar> toolBars = context.getApplication().getMainView()
                .getToolBars();
        
        for (JToolBar toolBar : toolBars) {
            tbNames.add(toolBar.getName());
        }
        
        JButton button = new JButton();
        button.setName("button");
        button.addActionListener(e -> {
            dispose();
        });
        
        int height = button.getPreferredSize().height;
        
        ResourceMap rm = context.getResourceMap(getClass());
        Preferences prefs = context.getPreferences(ApplicationContext.USER);
        for (String name : tbNames) {
            JCheckBox chkBox = new JCheckBox();
            chkBox.setName(name + "CheckBox");
            chkBox.setSelected(prefs.getBoolean(name + ".visible", true));
            chkBox.addChangeListener(new SelectionChanged());
            height += chkBox.getHeight();
            add(chkBox);
        }
        add(button);
        
        rm.injectComponents(this);
        
        FrameView mainView = context.getApplication().getMainView();
        int frameWidth = mainView.getFrame().getWidth();
        int y = -1;
        for (JToolBar bar : mainView.getToolBars()) {
            y = Math.max(bar.getLocationOnScreen().y + bar.getHeight() + 5, y);
        }
        setPreferredSize(new Dimension(135, height));
        setResizable(false);
        setLocation((frameWidth - getWidth()) / 2, y);
        System.out.println("ToolBarVisibility.width = " + getWidth());
    }
    
    private final ApplicationContext context;
    
    private class SelectionChanged implements ChangeListener {

        @Override
        public void stateChanged(ChangeEvent e) {
            Object source = e.getSource();
            if (source instanceof JCheckBox chkBox) {
                int index = chkBox.getName().indexOf("CheckBox");
                String tbName = chkBox.getName().substring(0, index);
                if (context.getApplication().getMainView() instanceof WindowManager wm) {
                    if (chkBox.isSelected()) {
                        wm.showToolBar(tbName);
                    } else {
                        wm.hideToolBar(tbName);
                    }
                }
            }
        }
        
    }

}
