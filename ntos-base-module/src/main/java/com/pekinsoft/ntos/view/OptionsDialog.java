/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   OptionsDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 16, 2024
 *  Modified   :   Jul 16, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 16, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.view;

import com.pekinsoft.api.OptionsPanelProvider;
import com.pekinsoft.framework.*;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.lookup.Lookups;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.*;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class OptionsDialog extends JDialog
        implements ActionListener, PropertyChangeListener {

    public static OptionsDialog getInstance() {
        if (instance == null) {
            instance = new OptionsDialog(Application.getInstance().getContext());
        }
        return instance;
    }

    private OptionsDialog(ApplicationContext context) {
        super(context.getApplication().getMainFrame(), true);
        Objects.requireNonNull(context, "The ApplicationContext cannot be null.");

        this.context = context;
        this.providers = new ArrayList<>();

        initComponents();
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        boolean old = isDirty();
        this.dirty = dirty;
        firePropertyChange("dirty", old, isDirty());
    }

    @AppAction(enabledProperty = "dirty")
    public void saveChanges(ActionEvent evt) {
        for (OptionsPanelProvider provider : providers) {
            boolean saved = provider.save();
            if (!saved) {
                logger.log(Level.WARNING, "Options for \"{0}\" not saved",
                        provider.tabTitle());
            }
        }
        setDirty(false);
    }
    
    @AppAction
    public void doClose(ActionEvent evt) {
        String sessionFileName = "";
        try {
            sessionFileName = getContext().getLocalStorage()
                    .getConfigDirectory().toString();
            if (!sessionFileName.endsWith(File.separator)) {
                sessionFileName += File.separator;
            }
            sessionFileName += OptionsDialog.this.getName()
                    + ".session.xml";

            getContext().getSessionStorage().save(OptionsDialog.this,
                    sessionFileName);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to save session state for \""
                    + "{0}\" to file \"{1}\": {2}",
                    OptionsDialog.this.getName(), sessionFileName,
                    ex.getMessage());
        } finally {
            String message = getResourceMap().getString("message.restart.required");
            String title = getResourceMap().getString("message.restart.required.title");
            int choice = JOptionPane.showConfirmDialog(OptionsDialog.this, message, title,
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (choice == JOptionPane.YES_OPTION) {
                getApplication().exit(SysExits.EX_RESTART);
            } else {
                OptionsDialog.this.dispose();
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        if (pce.getPropertyName() != null) {
            switch (pce.getPropertyName()) {
                case "dirty" -> {
                    setDirty((boolean) pce.getNewValue());
                }
                case "restartRequired" -> {
                    restartRequired = (boolean) pce.getNewValue();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand() != null) {
            optionsLayout.show(categoriesPanel, ae.getActionCommand());
        }
    }

    protected ApplicationContext getContext() {
        return context;
    }

    protected Application getApplication() {
        return getContext().getApplication();
    }

    protected ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass());
    }

    private void initComponents() {
        ResourceMap rm = getResourceMap();

        // Configure the dialog.
        setName("OptionsDialog");
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        addWindowListener(new DialogAdapter());

        // Create and construct the toolbar.
        categoriesToolBar = new JToolBar();
        categoriesToolBar.setName("categoriesToolBar");
        categoriesToolBar.setFloatable(false);

        // Create the Toggle Buttons for the toolbar.
        JToggleButton generalButton = new JToggleButton();
        generalButton.setName("generalButton");
        generalButton.addActionListener(this);
        generalButton.setHorizontalTextPosition(SwingConstants.CENTER);
        generalButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        categoriesToolBar.add(generalButton);

        JToggleButton defaultsButton = new JToggleButton();
        defaultsButton.setName("defaultsButton");
        defaultsButton.addActionListener(this);
        defaultsButton.setHorizontalTextPosition(SwingConstants.CENTER);
        defaultsButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        categoriesToolBar.add(defaultsButton);

        JToggleButton policiesButton = new JToggleButton();
        policiesButton.setName("policiesButton");
        policiesButton.addActionListener(this);
        policiesButton.setHorizontalTextPosition(SwingConstants.CENTER);
        policiesButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        categoriesToolBar.add(policiesButton);

        JToggleButton appearanceButton = new JToggleButton();
        appearanceButton.setName("appearanceButton");
        appearanceButton.addActionListener(this);
        appearanceButton.setHorizontalTextPosition(SwingConstants.CENTER);
        appearanceButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        categoriesToolBar.add(appearanceButton);

        JToggleButton miscellaneousButton = new JToggleButton();
        miscellaneousButton.setName("miscellaneousButton");
        miscellaneousButton.addActionListener(this);
        miscellaneousButton.setHorizontalTextPosition(SwingConstants.CENTER);
        miscellaneousButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        categoriesToolBar.add(miscellaneousButton);

        // Create and construct the categories panel.
        optionsLayout = new CardLayout();
        categoriesPanel = new JPanel(optionsLayout);
        categoriesPanel.setName("categoriesPanel");

        generalTabs = new JTabbedPane();
        generalTabs.setName("generalTabs");
        categoriesPanel.add(generalTabs, "general");

        defaultsTabs = new JTabbedPane();
        defaultsTabs.setName("defaultsTabs");
        categoriesPanel.add(defaultsTabs, "defaults");

        policiesTabs = new JTabbedPane();
        policiesTabs.setName("policiesTabs");
        categoriesPanel.add(policiesTabs, "policies");

        appearanceTabs = new JTabbedPane();
        appearanceTabs.setName("appearanceTabs");
        categoriesPanel.add(appearanceTabs, "appearance");

        miscTabs = new JTabbedPane();
        miscTabs.setName("miscTabs");
        categoriesPanel.add(miscTabs, "miscellaneous");
        
        // Set up and configure the command panel.
        commandPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        commandPanel.setName("commandPanel");
        
        okButton = new JButton();
        okButton.setName("okButton");
        okButton.setAction(getContext().getActionMap(this).get("saveChanges"));
        // Make the OK Button the default button on the dialog, so that it is 
        //+ activated by the enter key.
        getRootPane().setDefaultButton(okButton);
        commandPanel.add(okButton);
        
        cancelButton = new JButton();
        cancelButton.setName("cancelButton");
        cancelButton.setAction(getContext().getActionMap(this).get("doClose"));
        
        // Add a KeyStroke to the JRootPane's input map that will activate the
        //+ Cancel Button's action. Also add the action to the JRootPane's 
        //+ ActionMap.
        KeyStroke cancel = (KeyStroke) cancelButton.getAction()
                .getValue(Action.ACCELERATOR_KEY);
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(cancel, "doCancel");
        getRootPane().getActionMap().put("doCancel", cancelButton.getAction());
        
        commandPanel.add(cancelButton);

        // Now, locate all instances of OptionsPanelProvider and install them.
        Lookup<OptionsPanelProvider> lkp = Lookups.getDefault();
        for (OptionsPanelProvider provider
                : lkp.lookupAll(OptionsPanelProvider.class)) {
            provider.getPanel().addPropertyChangeListener(this);
            providers.add(provider);
            switch (provider.category().toLowerCase()) {
                case "general" ->
                    generalTabs.addTab(provider.tabTitle(), provider.getPanel());
                case "defaults" ->
                    defaultsTabs.addTab(provider.tabTitle(), provider.getPanel());
                case "policies" ->
                    policiesTabs.addTab(provider.tabTitle(), provider.getPanel());
                case "appearance" ->
                    appearanceTabs.addTab(provider.tabTitle(), provider.getPanel());
                case "miscellaneous" ->
                    miscTabs.addTab(provider.tabTitle(), provider.getPanel());
            }
        }

        // Add the components to the dialog
        setLayout(new BorderLayout());
        add(categoriesToolBar, BorderLayout.NORTH);
        add(categoriesPanel, BorderLayout.CENTER);
        add(commandPanel, BorderLayout.SOUTH);

        rm.injectComponents(this);
    }

    private static final Logger logger = System.getLogger(OptionsDialog.class.getName());
    private static OptionsDialog instance = null;

    private final ApplicationContext context;
    private final List<OptionsPanelProvider> providers;

    private JToolBar categoriesToolBar;
    private JPanel categoriesPanel;
    private CardLayout optionsLayout;
    private JTabbedPane generalTabs;
    private JTabbedPane defaultsTabs;
    private JTabbedPane policiesTabs;
    private JTabbedPane appearanceTabs;
    private JTabbedPane miscTabs;
    private JPanel commandPanel;
    private JButton okButton;
    private JButton cancelButton;

    private boolean dirty;
    private boolean restartRequired = false;

    private class DialogAdapter extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            ActionEvent evt = new ActionEvent(e.getSource(), e.getID(), "doClose");
        }
    }

}
