/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   UpdatesTableModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 19, 2024
 *  Modified   :   Jul 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.view.support;

import com.pekinsoft.api.Plugin;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.NtosApplication;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Objects;
import javax.swing.table.DefaultTableModel;

/**
 * An abstraction of the {@link DefaultTableModel} for use by the application in
 * the <em>Plugin Manager</em> tables. The tables on each tab of the Plugin
 * Manager represent the plugins in a different manner and this class was
 * created to ease the development of the various table models that may be 
 * needed.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public abstract class PluginManagerTableModel extends DefaultTableModel 
        implements PropertyChangeListener {

    private static final long serialVersionUID = 7343886560723989657L;
    
    public PluginManagerTableModel (ApplicationContext context) {
        Objects.requireNonNull(context, "The ApplicationContext cannot be null.");
        this.context = context;
        application = (NtosApplication) context.getApplication();
        application.addPropertyChangeListener(this);
        this.context.addPropertyChangeListener(this);
    }
    
    public Plugin getSelectedPlugin(int row) {
        return (plugins == null || plugins.isEmpty()) || row == -1
                ? null
                : plugins.get(row);
    }

    @Override
    public int getRowCount() {
        return plugins == null
                ? 0
                : plugins.size();
    }
    
    protected ApplicationContext getContext() {
        return context;
    }
    
    protected NtosApplication getApplication() {
        return application;
    }
    
    protected ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass());
    }
    
    protected List<Plugin> plugins;
    
    private final ApplicationContext context;
    private final NtosApplication application;

}
