/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   InstalledTableModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 19, 2024
 *  Modified   :   Jul 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.view.support;

import com.pekinsoft.api.Plugin;
import com.pekinsoft.framework.ApplicationContext;
import java.beans.PropertyChangeEvent;
import java.util.List;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class InstalledTableModel extends PluginManagerTableModel {
    
    public InstalledTableModel (ApplicationContext context) {
        super(context);
        plugins = getApplication().getInstalledPlugins();
        
        translate();
    }

    @Override
    public Object getValueAt(int row, int column) {
        if ((row < 0 || row > plugins.size()) || (column < 0 || column > 4)) {
            return null;
        }
        if (tableValues != null) {
            return tableValues[row][column];
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        if ((row < 0 || row >= tableValues.length) || (column != 0)) {
            return;
        }
        if (column == 0) {
            tableValues[row][column] = aValue;
        } 
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return switch (columnIndex) {
            case 0, 3 -> Boolean.class;
            default -> String.class;
        };
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        boolean isPlugin = getSelectedPlugin(row).getPluginName().toLowerCase()
                .contains("plugin");
        if (row > -1) {
            return switch(column) {
                case 0 -> isPlugin;
                default -> false;
            };
        } else {
            return false;
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() != null) switch (evt.getPropertyName()) {
            case "installedPlugins", "pluginActivated", "pluginDeactivated" -> {
                plugins = (List<Plugin>) evt.getNewValue();
                translate();
            }
        }
    }
    
    private void translate() {
        tableValues = new Object[plugins.size()][4];
        for (int x = 0; x < tableValues.length; x++) {
            tableValues[x][0] = false;
            tableValues[x][1] = plugins.get(x).getPluginName();
            tableValues[x][2] = plugins.get(x).getPluginVersion();
            tableValues[x][3] = !plugins.get(x).isDeactivated();
        }
        fireTableDataChanged();
    }
    
    private Object[][] tableValues;

}
