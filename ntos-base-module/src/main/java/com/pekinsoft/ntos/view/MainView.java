/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   MainView.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 15, 2024
 *  Modified   :   Jul 15, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 15, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.view;

import com.pekinsoft.api.Dockable;
import com.pekinsoft.api.DockingManager;
import com.pekinsoft.api.DockRegistrationException;
import com.pekinsoft.desktop.ButtonTabComponent;
import com.pekinsoft.desktop.error.*;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.FrameView;
import com.pekinsoft.framework.StatusBar;
import com.pekinsoft.utils.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import com.pekinsoft.ntos.common.NtosPreferenceKeys;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class MainView extends FrameView 
        implements DockingManager, NtosPreferenceKeys {
    
    public MainView (ApplicationContext context) {
        super(context);
        
        registration = new HashMap<>();
        openWindows = new ArrayList<>();
        
        initComponents();
    }

    @Override
    public void register(Dockable dock) {
        Objects.requireNonNull(dock, "The Dockable cannot be null.");
        if (registration.containsKey(dock.getDockId())) {
            throw new DockRegistrationException("The Dockable with the dockId "
                    + "\"" + dock.getDockId() + "\" has already been registered.");
        }
        registration.put(dock.getDockId(), dock);
    }

    @Override
    public void deregister(String dockId) {
        if (dockId == null || dockId.isBlank() || dockId.isEmpty()) {
            throw new NullPointerException("The dockId cannot be blank, empty, "
                    + "or null.");
        }
        if (!registration.containsKey(dockId)) {
            throw new DockRegistrationException("A Dockable with the dockId \"" 
                    + dockId + "\" has not been registered.");
        }
        Dockable dock = registration.remove(dockId);
        
        saveDock(dock);
        dock.close();
    }

    @Override
    public boolean isRegistered(String dockId) {
        if (dockId == null || dockId.isBlank() || dockId.isEmpty()) {
            return false;
        }
        return registration.containsKey(dockId);
    }

    @Override
    public void dock(String dockId) {
        if (dockId == null || dockId.isBlank() || dockId.isEmpty()) {
            throw new NullPointerException("The dockId cannot be blank, empty, "
                    + "or null.");
        }
        if (!registration.containsKey(dockId)) {
            throw new DockRegistrationException("A Dockable with the dockId \"" 
                    + dockId + "\" has not been registered.");
        }
        
        Dockable dock = registration.get(dockId);
        switch (dock.getDockingLocation().toUpperCase()) {
            case "CENTER" -> addCenterDock(dock);
            case "EAST" -> addEastDock(dock);
            case "SOUTH" -> addSouthDock(dock);
            case "WEST" -> addWestDock(dock);
        }
    }

    @Override
    public void undock(String dockId) {
        if (dockId == null || dockId.isBlank() || dockId.isEmpty()) {
            throw new NullPointerException("The dockId cannot be blank, empty, "
                    + "or null.");
        }
        if (!registration.containsKey(dockId)) {
            throw new DockRegistrationException("A Dockable with the dockId \"" 
                    + dockId + "\" has not been registered.");
        }
        
        Dockable dock = registration.get(dockId);
        int index = -1;
        switch (dock.getDockingLocation().toUpperCase()) {
            case "CENTER" -> {
                index = centerDocks.indexOfComponent(dock.getDockingComponent());
                if (index != -1) {
                    centerDocks.remove(index);
                }
            }
            case "EAST" -> {
                index = eastDocks.indexOfComponent(dock.getDockingComponent());
                if (index != -1) {
                    eastDocks.remove(index);
                }
            }
            case "SOUTH" -> {
                index = southDocks.indexOfComponent(dock.getDockingComponent());
                if (index != -1) {
                    southDocks.remove(index);
                }
            }
            case "WEST" -> {
                index = westDocks.indexOfComponent(dock.getDockingComponent());
                if (index != -1) {
                    westDocks.remove(index);
                }
            }
        }
        
        saveDock(dock);
    }

    @Override
    public void showMainFrame() {
        getApplication().show(this);
    }

    @Override
    public void show(JComponent jc) {
        if (jc instanceof Dockable dockable) {
            dock(dockable.getDockId());
        }
    }

    @Override
    public void show(JFrame jframe) {
        getApplication().show(jframe);
        openWindows.add(jframe);
    }

    @Override
    public void show(JDialog jd) {
        getApplication().show(jd);
    }

    @Override
    public void closeMainFrame() {
        // TODO: Implement closeMainFrame
    }

    @Override
    public void close(JComponent jc) {
        if (jc instanceof Dockable dockable) {
            undock(dockable.getDockId());
        }
    }

    @Override
    public void close(JFrame jframe) {
        // TODO: Save the jframe's session state.
        openWindows.remove(jframe);
        jframe.dispose();
    }

    @Override
    public void close(JDialog jd) {
        jd.setVisible(false);
    }

    @Override
    public Container find(String name) {
        Set<String> keySet = registration.keySet();
        for (String key : keySet) {
            if (name.equals(key)) {
                return registration.get(key).getDockingComponent();
            }
        }
        
        for (Window window : openWindows) {
            if (name.equals(window.getName())) {
                return window;
            } else if (window instanceof Frame f) {
                if (name.equals(f.getTitle())) {
                    return window;
                }
            }
        }
        
        return null;
    }

    @Override
    public JFrame getMainFrame() {
        return getApplication().getMainFrame();
    }

    @Override
    public void showToolBar(String name) {
        for (JToolBar toolBar : getToolBars()) {
            if (name.equals(toolBar.getName())) {
                toolBar.setVisible(true);
                getContext().getPreferences(ApplicationContext.USER)
                        .putBoolean(name + ".visible", true);
            }
        }
    }

    @Override
    public void hideToolBar(String name) {
        for (JToolBar toolBar : getToolBars()) {
            if (name.equals(toolBar.getName())) {
                toolBar.setVisible(false);
                getContext().getPreferences(ApplicationContext.USER)
                        .putBoolean(name + ".visible", false);
            }
        }
    }

    @Override
    public void showButtonText(boolean showButtonText) {
        boolean hideButtonText = !showButtonText;
        getContext().getPreferences(ApplicationContext.USER)
                .putBoolean(KEY_SHOW_BUTTON_TEXT, showButtonText);
        for (JToolBar toolBar : getToolBars()) {
            toolBar.putClientProperty("hideActionText", hideButtonText);
            for (Component c : toolBar.getComponents()) {
                if (c instanceof AbstractButton button) {
                    button.setHorizontalTextPosition(AbstractButton.CENTER);
                    button.setVerticalTextPosition(AbstractButton.BOTTOM);
                    button.putClientProperty("hideActionText", hideButtonText);
                }
            }
        }
    }

    @Override
    public void preload() {
        // TODO: Implement preload
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        super.propertyChange(evt);
        
        if (evt.getPropertyName() != null) switch (evt.getPropertyName()) {
            case "visible" -> {
                if (evt.getSource() == statusBar) {
                    boolean visible = (boolean) evt.getNewValue();
                    if (visible) {
                        setStatusBar(null);
                    } else {
                        setStatusBar(statusBar);
                    }
                }
            }
            case "toolBars" -> {
                boolean hideButtonText = !getContext().getPreferences(ApplicationContext.USER)
                        .getBoolean(KEY_SHOW_BUTTON_TEXT, false);
                for (JToolBar toolBar : getToolBars()) {
                    toolBar.putClientProperty("hideActionText", hideButtonText);
                    for (Component c : toolBar.getComponents()) {
                        if (c instanceof AbstractButton button) {
                            button.setHorizontalTextPosition(AbstractButton.CENTER);
                            button.setVerticalTextPosition(AbstractButton.BOTTOM);
                            button.putClientProperty("hideActionText", hideButtonText);
                        }
                    }
                }
            }
        }
    }
    
    private void initComponents() {
        eastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        eastSplit.setName("eastSplit");
        
        southSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        southSplit.setName("southSplit");
        
        westSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        westSplit.setName("westSplit");
        
        centerDocks = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
        centerDocks.setName("centerDocks");
        centerDocks.addContainerListener(visibilityListener);
        
        eastDocks = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
        eastDocks.setName("eastDocks");
        eastDocks.addContainerListener(visibilityListener);
        
        eastSplit.setRightComponent(eastDocks);
        
        southDocks = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
        southDocks.setName("southDocks");
        southDocks.addContainerListener(visibilityListener);
        
        southSplit.setBottomComponent(southDocks);
        
        westDocks = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
        westDocks.setName("westDocks");
        westDocks.addContainerListener(visibilityListener);
        
        westSplit.setLeftComponent(westDocks);
        
        eastRoot = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        eastRoot.setName("eastRoot");
        
        southRoot = new JPanel(new FlowLayout(FlowLayout.LEADING));
        southRoot.setName("southRoot");
        
        westRoot = new JPanel(new FlowLayout(FlowLayout.LEADING));
        westRoot.setName("westRoot");
        
        contentPanel = new JPanel(new BorderLayout());
        contentPanel.setName("contentPanel");
        contentPanel.add(eastRoot, BorderLayout.EAST);
        contentPanel.add(westRoot, BorderLayout.WEST);
        contentPanel.add(southRoot, BorderLayout.SOUTH);
        contentPanel.add(centerDocks, BorderLayout.CENTER);
        
        statusBar = new StatusBar(getApplication());
        statusBar.setName("statusBar");
        statusBar.setVisible(getContext().getPreferences(ApplicationContext.USER)
                .getBoolean(KEY_STATUSBAR_VISIBLE, true));
        statusBar.addPropertyChangeListener(this);
        getApplication().addPropertyChangeListener(statusBar);
        getContext().addPropertyChangeListener(statusBar);
        
        // Add components to the View.
        setStatusBar(statusBar);
        setComponent(contentPanel);
        
        // Make sure the toolbars abide by the user's text settings.
        boolean hideButtonText = !getContext().getPreferences(ApplicationContext.USER)
                .getBoolean(KEY_SHOW_BUTTON_TEXT, false);
        for (JToolBar toolBar : getToolBars()) {
            toolBar.putClientProperty("hideActionText", hideButtonText);
            for (Component c : toolBar.getComponents()) {
                if (c instanceof AbstractButton button) {
                    button.setHorizontalTextPosition(AbstractButton.CENTER);
                    button.setVerticalTextPosition(AbstractButton.BOTTOM);
                    button.putClientProperty("hideActionText", hideButtonText);
                }
            }
        }
    }
    
    private void addCenterDock(Dockable dock) {
        restoreDock(dock);
        
        centerDocks.addTab(dock.getTitle(), dock.getIcon(), 
                dock.getDockingComponent());
        int index = centerDocks.indexOfComponent(dock.getDockingComponent());
        centerDocks.setTabComponentAt(index, new ButtonTabComponent(centerDocks, 
                dock));
        centerDocks.setSelectedIndex(index);
        
        configureDocking();
    }
    
    private void addEastDock(Dockable dock) {
        restoreDock(dock);
        
        eastDocks.addTab(dock.getTitle(), dock.getIcon(),
                dock.getDockingComponent());
        int index = eastDocks.indexOfComponent(dock.getDockingComponent());
        eastDocks.setTabComponentAt(index, new ButtonTabComponent(eastDocks,
                dock));
        eastDocks.setSelectedIndex(index);
        
        configureDocking();
    }
    
    private void addSouthDock(Dockable dock) {
        restoreDock(dock);
        
        southDocks.addTab(dock.getTitle(), dock.getIcon(),
                dock.getDockingComponent());
        int index = southDocks.indexOfComponent(dock.getDockingComponent());
        southDocks.setTabComponentAt(index, new ButtonTabComponent(southDocks, 
                dock));
        southDocks.setSelectedIndex(index);
        
        configureDocking();
    }
    
    private void addWestDock(Dockable dock) {
        restoreDock(dock);
        
        westDocks.addTab(dock.getTitle(), dock.getIcon(), 
                dock.getDockingComponent());
        int index = westDocks.indexOfComponent(dock.getDockingComponent());
        westDocks.setTabComponentAt(index, new ButtonTabComponent(westDocks, 
                dock));
        
        westDocks.setSelectedIndex(index);
        
        configureDocking();
    }
    
    private void restoreDock(Dockable dock) {
        getContext().getResourceMap(dock.getClass())
                .injectComponents(dock.getDockingComponent());
        
        try {
            getContext().getSessionStorage().restore(dock.getDockingComponent(),
                    sessionFileName(dock));
        } catch (IOException e) {
            logger.log(Level.WARNING, "Unable to restore session state for \"{0}"
                    + "\": {1}", dock.getDockId(), e.getMessage()); // NOI18N
            
            // I18N: Showing to the user.
            ErrorInfo info = new ErrorInfo(e, ErrorLevel.WARNING,
                    getResourceMap().getString("message.restore.error",
                            dock.getDockId(), e.getLocalizedMessage()));
            getContext().firePropertyChange(PropertyKeys.KEY_ADD_NOTIFICATION, 
                    null, info);
        }
    }
    
    private void saveDock(Dockable dock) {
        try {
            getContext().getSessionStorage().save(dock.getDockingComponent(), 
                    sessionFileName(dock));
        } catch (IOException e) {
            // NOI18N: For developers only.
            logger.log(Level.WARNING, "Unable to save session state for \"{0}\""
                    + ": {1}", dock.getDockId(), e.getMessage());
        }
    }
    
    private String sessionFileName(Object obj) {
        if (obj == null) {
            return null;
        } else if (obj instanceof Component component) {
            if (component.getName() == null) {
                return null;
            } else {
                return component.getName() + ".session.xml";
            }
        } else if (obj instanceof Dockable dock) {
            return dock.getDockId() + ".session.xml";
        } else {
            return null;
        }
    }
    
    private void configureDocking() {
        // EAST, SOUTH, and WEST all visible.
        if (eastSplit.getRightComponent().isVisible()
                && southSplit.getBottomComponent().isVisible()
                && westSplit.getLeftComponent().isVisible()) {
            eastSplit.setLeftComponent(centerDocks);
            southSplit.setTopComponent(eastSplit);
            westSplit.setRightComponent(eastSplit);
            contentPanel.add(westSplit, BorderLayout.CENTER);
        // EAST and SOUTH are visible
        } else if (eastSplit.getRightComponent().isVisible()
                && southSplit.getBottomComponent().isVisible()
                && !westSplit.getLeftComponent().isVisible()) {
            eastSplit.setLeftComponent(centerDocks);
            southSplit.setTopComponent(eastSplit);
            contentPanel.add(southSplit, BorderLayout.CENTER);
        // EAST and WEST are visible
        } else if (eastSplit.getRightComponent().isVisible()
                && !southSplit.getBottomComponent().isVisible()
                && westSplit.getLeftComponent().isVisible()) {
            eastSplit.setLeftComponent(centerDocks);
            westSplit.setRightComponent(eastSplit);
            contentPanel.add(westSplit, BorderLayout.CENTER);
        // SOUTH and WEST are visible
        } else if (!eastSplit.getRightComponent().isVisible()
                && southSplit.getBottomComponent().isVisible()
                && westSplit.getLeftComponent().isVisible()) {
            southSplit.setTopComponent(centerDocks);
            westSplit.setRightComponent(southSplit);
            contentPanel.add(westSplit, BorderLayout.CENTER);
        // Only EAST is visible
        } else if (eastSplit.getRightComponent().isVisible()
                && !southSplit.getBottomComponent().isVisible()
                && !westSplit.getLeftComponent().isVisible()) {
            eastSplit.setLeftComponent(centerDocks);
            contentPanel.add(eastSplit, BorderLayout.CENTER);
        // Only SOUTH is visible
        } else if (!eastSplit.getRightComponent().isVisible()
                && southSplit.getBottomComponent().isVisible()
                && !westSplit.getLeftComponent().isVisible()) {
            southSplit.setTopComponent(centerDocks);
            contentPanel.add(southSplit, BorderLayout.CENTER);
        // Only WEST is visible
        } else if (!eastSplit.getRightComponent().isVisible()
                && !southSplit.getBottomComponent().isVisible()
                && westSplit.getLeftComponent().isVisible()) {
            westSplit.setRightComponent(centerDocks);
            contentPanel.add(westSplit, BorderLayout.CENTER);
        } else {
            contentPanel.add(centerDocks, BorderLayout.CENTER);
        }
        
        getRootPane().invalidate();
    }
    
    private static final Logger logger = System.getLogger(MainView.class.getName());
    
    private final Map<String, Dockable> registration;
    private final List<Window> openWindows;
    private final ContainerListener visibilityListener = new DockVisibilityListener();
    
    private JTabbedPane centerDocks;
    private JTabbedPane eastDocks;
    private JTabbedPane southDocks;
    private JTabbedPane westDocks;
    
    private JSplitPane eastSplit;
    private JSplitPane southSplit;
    private JSplitPane westSplit;
    
    private JPanel contentPanel;
    private JPanel eastRoot;
    private JPanel southRoot;
    private JPanel westRoot;
    
    private StatusBar statusBar;
    
    private class DockVisibilityListener implements ContainerListener {

        @Override
        public void componentAdded(ContainerEvent e) {
            e.getContainer().setVisible(true);
        }

        @Override
        public void componentRemoved(ContainerEvent e) {
            Container container = e.getContainer();
            if (container != null) {
                int tabCount = 0;
                if (container instanceof JTabbedPane pane) {
                    tabCount = pane.getTabCount();
                }
                container.setVisible(container.getComponentCount() == 0
                        || tabCount == 0);
            }
        }
        
    }

}
