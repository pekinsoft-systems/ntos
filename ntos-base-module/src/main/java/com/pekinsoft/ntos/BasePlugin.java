/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   BasePlugin.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 15, 2024
 *  Modified   :   Jul 15, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 15, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos;

import com.pekinsoft.api.Plugin;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.ntos.actions.ApplicationActions;
import java.util.ArrayList;
import java.util.List;
import javax.help.HelpSet;
import javax.swing.ActionMap;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class BasePlugin implements Plugin {
    
    public BasePlugin () {
        context = Application.getInstance().getContext();
    }

    @Override
    public void install() {
        installed = true;
    }

    @Override
    public void ready() {
        // TODO: Implement BasePlugin.ready
    }

    @Override
    public void activate() {
        activated = true;
    }

    @Override
    public void deactivate() {
        activated = false;
    }

    @Override
    public boolean isDeactivated() {
        return !activated;
    }

    @Override
    public void uninstall() {
        installed = false;
    }

    @Override
    public boolean isUninstalled() {
        return !installed;
    }

    @Override
    public List<ActionMap> mergeGlobalActions() {
        List<ActionMap> actionMaps = new ArrayList<>();
        
        actionMaps.add(context.getActionMap());
        actionMaps.add(ApplicationActions.getInstance().getActionMap());
        
        return actionMaps;
    }

    @Override
    public List<HelpSet> mergeHelpSets() {
        List<HelpSet> helpSets = new ArrayList<>();
        
        // TODO: Figure out how we're going to make this work.
        
        return helpSets;
    }

    @Override
    public String getPluginName() {
        return context.getResourceMap(getClass()).getString("Application.title")
                + " (<strong><em>Base Module</em></strong>)";
    }

    @Override
    public String getPluginVersion() {
        return context.getResourceMap(getClass()).getString("Application.version");
    }
    
    private final ApplicationContext context;
    private boolean installed = false;
    private boolean activated = false;

}
