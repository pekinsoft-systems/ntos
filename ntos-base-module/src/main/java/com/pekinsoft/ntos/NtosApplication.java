/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos
 *  Class      :   NtosApplication.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 15, 2024
 *  Modified   :   Jul 15, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 15, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos;

import com.pekinsoft.api.Plugin;
import com.pekinsoft.api.PreferenceKeys;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.GlobalActionSystem;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.lookup.Lookups;
import com.pekinsoft.ntos.common.NtosPreferenceKeys;
import static com.pekinsoft.ntos.common.NtosPreferenceKeys.KEY_SHOW_BUTTON_TEXT;
import com.pekinsoft.ntos.common.PropertyKeys;
import com.pekinsoft.ntos.view.MainView;
import com.pekinsoft.utils.ArgumentParser;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.prefs.Preferences;
import javax.swing.AbstractButton;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

/**
 *
 * @author Sean Carrick
 */
public class NtosApplication extends Application 
        implements NtosPreferenceKeys, PropertyKeys {

    public static void main(String[] args) {
        splash = SplashScreen.getSplashScreen();
        g = (splash == null) ? null : splash.createGraphics();
        
        Application.launch(NtosApplication.class, args);
    }
    
    public NtosApplication() {
        installedPlugins = new ArrayList<>();
    }
    
    /**
     * Installs the specified {@link Plugin} into the application.
     * <p>
     * If the specified {@code plugin} is {@code null}, or the {@code plugin} is
     * already installed, no exception is thrown and no action is taken.
     * <p>
     * This is a bound property.
     * 
     * @param plugin the {@code Plugin} to be installed
     */
    public void installPlugin(Plugin plugin) {
        if (plugin == null || installedPlugins.contains(plugin)) {
            return;
        }
        
        List<Plugin> old = null, mod = null;
        synchronized(installedPlugins) {
            old = getInstalledPlugins();
            installedPlugins.add(plugin);
            mod = getInstalledPlugins();
        }

        plugin.install();
        Preferences.userNodeForPackage(plugin.getClass())
                .putBoolean("installed", true);
        reconfigureGlobalActions();
        firePropertyChange("installedPlugins", old, mod);

        plugin.install();
    }
    
    /**
     * Uninstalls the specifed {@link Plugin} from the application.
     * <p>
     * If the specified {@code plugin} is {@code null}, or was not previously
     * {@link #installPlugin(Plugin) installed}, no exception is thrown and no
     * action is taken.
     * <p>
     * This is a bound property.
     * 
     * @param plugin the {@code Plugin} to be uninstalled
     */
    public void uninstallPlugin(Plugin plugin) {
        if (plugin == null || !installedPlugins.contains(plugin)) {
            return;
        }
        
        List<Plugin> old = null, mod = null;
        synchronized (installedPlugins) {
            old = getInstalledPlugins();
            installedPlugins.remove(plugin);
            mod = getInstalledPlugins();
        }
        
        plugin.uninstall();
        Preferences.userNodeForPackage(plugin.getClass())
                .putBoolean("installed", false);
        reconfigureGlobalActions();
        firePropertyChange("installedPlugins", old, mod);
    }
    
    /**
     * Activates the specified {@link Plugin}. This simply activates a 
     * {@code Plugin} that has been previously 
     * {@link #installPlugin(Plugin) installed} and subsequently
     * {@link #deactivatePlugin(Plugin) deactivated}. When this method returns,
     * the features provided by the {@code Plugin} will be available.
     * <p>
     * If the specified {@code plugin} is {@code null}, or has not been 
     * previously installed, no exception is thrown and no action is taken.
     * <p>
     * This is a bound property.
     * 
     * @param plugin the {@code Plugin} to be activated
     */
    public void activatePlugin(Plugin plugin) {
        if (plugin == null || !installedPlugins.contains(plugin)) {
            return;
        }
        
        String name = plugin.getPluginName();
        for (Plugin p : installedPlugins) {
            if (name.equals(p.getPluginName())) {
                p.activate();
                Preferences.userNodeForPackage(p.getClass())
                        .putBoolean("activated", true);
                break;
            }
        }
        reconfigureGlobalActions();
        
        firePropertyChange("pluginActivated", null, getInstalledPlugins());
    }
    
    /**
     * Deactivates the specified {@link Plugin}. This only deactivates the
     * {@code Plugin} and does not uninstall it. While deactivated, the features
     * provided by the {@code Plugin} will be unavailable.
     * <p>
     * If the specified {@code plugin} is {@code null}, or has not been 
     * previously {@link #installPlugin(Plugin) installed}, no exception is 
     * thrown and no action is taken.
     * <p>
     * This is a bound property.
     * 
     * @param plugin the {@code Plugin} to be deactivated
     */
    public void deactivatePlugin(Plugin plugin) {
        if (plugin == null || !installedPlugins.contains(plugin)) {
            return;
        }
        
        String name = plugin.getPluginName();
        for (Plugin p : installedPlugins) {
            if (name.equals(p.getPluginName())) {
                p.deactivate();
                Preferences.userNodeForPackage(p.getClass())
                        .putBoolean("activated", false);
                break;
            }
        }
        reconfigureGlobalActions();
        
        firePropertyChange("pluginDeactivated", null, getInstalledPlugins());
    }
    
    /**
     * Retrieves a <em>read-only</em> list of the installed 
     * {@link Plugin Plugins}. The returned list is guaranteed to only contain
     * plugins that are currently installed, but there is not guarantee whether
     * the plugin is activated.
     * 
     * @return the installed plugins
     */
    public List<Plugin> getInstalledPlugins() {
        return Collections.unmodifiableList(installedPlugins);
    }

    @Override
    protected void initialize(String[] args) {
        if (splash == null) {
            System.out.println("No splash screen available.");
        } else {
            // Just to be sure:
            g = splash.createGraphics();
        }
        
        if (g != null) {
            renderSplashFrame(g, 0);
            sleep(generator.nextLong(base, top));
            setLookAndFeel();
            
            // Database actually gets configured in the data module, but we'll
            //+ progress it here.
            renderSplashFrame(g, 1);
            sleep(generator.nextLong(base, top));
        }
        Preferences prefs = Preferences.userNodeForPackage(
                getContext().getApplicationClass());
        ArgumentParser parser = new ArgumentParser(args);
        
        if (g != null) {
            renderSplashFrame(g, 2);
            sleep(generator.nextLong(base, top));
        }
        Lookup<Plugin> lkp = Lookups.getDefault();
        for (Plugin plugin : lkp.lookupAll(Plugin.class)) {
            if (plugin.shouldInstall()) {
                plugin.install();
                
                if (plugin.shouldActivate()) {
                    plugin.activate();
                }
                
                installedPlugins.add(plugin);
            }
        }
        
        if (g != null) {
            renderSplashFrame(g, 3);
            sleep(generator.nextLong(base, top));
        }
        if (actionMaps == null) {
            actionMaps = new ArrayList<>();
        }
        
        for (Plugin plugin : installedPlugins) {
            if (!plugin.isDeactivated()) {
                actionMaps.addAll(plugin.mergeGlobalActions());
            }
        }
    }
    
    @Override
    protected void startup() {
        if (g != null) {
            renderSplashFrame(g, 4);
            sleep(generator.nextLong(base, top));
        }
        MainView mainView = new MainView(getContext());
        mainView.setMenuBar(GlobalActionSystem.createMenuBar(getContext(), actionMaps));
        toolBars = GlobalActionSystem.createToolBars(getContext(), actionMaps);
        
        // Make sure the toolbars abide by the user's text settings.
        boolean hideButtonText = !getContext().getPreferences(ApplicationContext.USER)
                .getBoolean(KEY_SHOW_BUTTON_TEXT, false);
        for (JToolBar toolBar : toolBars) {
            toolBar.putClientProperty("hideActionText", hideButtonText);
            for (Component c : toolBar.getComponents()) {
                if (c instanceof AbstractButton button) {
                    button.setHorizontalTextPosition(AbstractButton.CENTER);
                    button.setVerticalTextPosition(AbstractButton.BOTTOM);
                    button.putClientProperty("hideActionText", hideButtonText);
                }
            }
            toolBar.setVisible(getContext().getPreferences(ApplicationContext.USER)
                .getBoolean(toolBar.getName(), true));
        }
        
        if (g != null) {
            renderSplashFrame(g, 5);
            sleep(generator.nextLong(base, top));
        }
        mainView.setToolBars(toolBars);
        mainView.getFrame().setIconImage(getContext().getResourceMap()
                .getImageIcon("Application.icon").getImage());
        setMainView(mainView);
        
        if (g != null) {
            renderSplashFrame(g, 6);
            sleep(generator.nextLong(base, top));
        }
        show(mainView);
    }

    @Override
    protected void ready() {
        for (Plugin plugin : installedPlugins) {
            if (!plugin.isDeactivated()) {
                plugin.ready();
            }
        }
    }
    
    private void reconfigureGlobalActions() {
        if (actionMaps == null) {
            actionMaps = new ArrayList<>();
        } else {
            actionMaps.clear();
        }
        
        for (Plugin plugin : installedPlugins) {
            if (!plugin.isDeactivated()) {
                actionMaps.addAll(plugin.mergeGlobalActions());
            }
        }
        
        menuBar = GlobalActionSystem.createMenuBar(getContext(), actionMaps);
        toolBars = GlobalActionSystem.createToolBars(getContext(), actionMaps);
        
        // Make sure the toolbars abide by the user's text settings.
        boolean hideButtonText = !getContext().getPreferences(ApplicationContext.USER)
                .getBoolean(KEY_SHOW_BUTTON_TEXT, false);
        for (JToolBar toolBar : toolBars) {
            toolBar.putClientProperty("hideActionText", hideButtonText);
            for (Component c : toolBar.getComponents()) {
                if (c instanceof AbstractButton button) {
                    button.setHorizontalTextPosition(AbstractButton.CENTER);
                    button.setVerticalTextPosition(AbstractButton.BOTTOM);
                    button.putClientProperty("hideActionText", hideButtonText);
                }
            }
            toolBar.setVisible(getContext().getPreferences(ApplicationContext.USER)
                .getBoolean(toolBar.getName(), true));
        }
        
        getMainView().setMenuBar(menuBar);
        getMainView().setToolBars(toolBars);
    }
    
    private void renderSplashFrame(Graphics2D g, int frame) {
        final String[] messages = {
            getContext().getResourceMap().getString("message.initializing"),
            getContext().getResourceMap().getString("message.init.database"),
            getContext().getResourceMap().getString("message.init.plugins"),
            getContext().getResourceMap().getString("message.init.actions"),
            getContext().getResourceMap().getString("message.init.creatingMainFrame"),
            getContext().getResourceMap().getString("message.init.restoringSessionState"),
            getContext().getResourceMap().getString("message.init.complete")
        };
        
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setComposite(AlphaComposite.Clear);
        g.fillRect(40, 85, 200, 40);
        g.setPaintMode();
        g.setColor(Color.red);
        g.setFont(g.getFont().deriveFont(Font.BOLD));
        
        g.drawString(messages[frame], 40, 95);
        g.drawRect(progressBar.x, progressBar.y, progressBar.width, 
                progressBar.height);
        progress.width = ((progressBar.width - 2) / messages.length) * (frame + 1);
        g.setColor(Color.red);
        g.fillRect(progress.x, progress.y, progress.width, progress.height);
        splash.update();
    }
    
    private void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException ignore) {
            // Actively ignoring this exception.
        }
    }
    
    private static final Random generator = new Random();
    private static final long base = 250;
    private static final long top = 750;
    private static final Rectangle progressBar = new Rectangle(40, 105, 202, 5);
    private static final Rectangle progress = new Rectangle(41, 106, 200, 4);
    
    private static SplashScreen splash = null;
    private static Graphics2D g = null;
    
    private final List<Plugin> installedPlugins;
    
    private JMenuBar menuBar = null;
    private List<JToolBar> toolBars = null;
    
}
