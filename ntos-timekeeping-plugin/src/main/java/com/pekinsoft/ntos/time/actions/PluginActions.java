/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-timekeeping-plugin
 *  Class      :   PluginActions.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 19, 2024
 *  Modified   :   Jul 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.time.actions;

import com.pekinsoft.framework.*;
import com.pekinsoft.ntos.time.TimePreferences;
import java.awt.event.ActionEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javax.swing.JOptionPane;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class PluginActions extends AbstractBean implements TimePreferences {
    
    public static PluginActions getInstance() {
        if (instance == null) {
            instance = new PluginActions(Application.getInstance().getContext());
        }
        return instance;
    }
    
    private PluginActions (ApplicationContext context) {
        this.context = context;
        resourceMap = this.context.getResourceMap(getClass());
        dateFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM,
                FormatStyle.MEDIUM);
        setOnTour(context.getPreferences(ApplicationContext.USER)
                .getBoolean(KEY_ON_TOUR, false));
    }
    
    public boolean isOnTour() {
        return onTour;
    }
    
    public final void setOnTour(boolean onTour) {
        boolean old = isOnTour();
        this.onTour = onTour;
        firePropertyChange("onTour", old, isOnTour());
        firePropertyChange("atHome", isOnTour(), isAtHome());
    }
    
    public boolean isAtHome() {
        return !isOnTour();
    }
    
    public void setAtHome(boolean atHome) {
        boolean old = isAtHome();
        onTour = !atHome;
        firePropertyChange("atHome", old, isAtHome());
        firePropertyChange("onTour", isAtHome(), isOnTour());
    }
    
    @AppAction(selectedProperty = "onTour",
            groupId = "timeKeeping",
            menuActionIndex = 10,
            menuBaseName = "tools",
            menuSepBefore = true /*,
            toolbarActionIndex = 10,
            toolbarBaseName = "tools",
            toolbarSepBefore = true*/)
    public void startTour(ActionEvent evt) {
        LocalDateTime start = LocalDateTime.now();
        String message = resourceMap.getString("message.tour.started",
                start.format(dateFormat));
        String title = resourceMap.getString("messages.title", "Started");
        
        JOptionPane.showMessageDialog(context.getApplication().getMainFrame(), 
                message, title, JOptionPane.INFORMATION_MESSAGE);
        
        // Store this value to the Preferences.
        context.getPreferences(ApplicationContext.USER)
                .put(KEY_TOUR_START, start.format(dateFormat));
        context.getPreferences(ApplicationContext.USER)
                .putBoolean(KEY_ON_TOUR, onTour);
    }
    
    @AppAction(selectedProperty = "atHome",
            groupId = "timeKeeping",
            menuActionIndex = 11,
            menuBaseName = "tools",
            menuSepAfter = true /*,
            toolbarActionIndex = 11,
            toolbarBaseName = "tools",
            toolbarSepAfter = true*/)
    public void endTour(ActionEvent evt) {
        LocalDateTime start = LocalDateTime.now();
        String message = resourceMap.getString("message.tour.ended",
                start.format(dateFormat));
        String title = resourceMap.getString("messages.title", "Ended");
        
        JOptionPane.showMessageDialog(context.getApplication().getMainFrame(), 
                message, title, JOptionPane.INFORMATION_MESSAGE);
        
        context.getPreferences(ApplicationContext.USER)
                .putBoolean(KEY_ON_TOUR, onTour);
        
        // TODO: Store start/end timestamps and the total time out to the database.
    }
    
    private static PluginActions instance = null;
    
    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    private final DateTimeFormatter dateFormat;
    
    private boolean onTour = false;
    
}
