/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS
 *  Class      :   NtosTimekeepingPlugin.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 19, 2024
 *  Modified   :   Jul 19, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.ntos.time;

import com.pekinsoft.api.Plugin;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.ntos.time.actions.PluginActions;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;
import javax.help.HelpSet;
import javax.swing.ActionMap;

/**
 *
 * @author Sean Carrick
 */
public class TimeKeepingPlugin implements Plugin {
    
    public TimeKeepingPlugin() {
        context = Application.getInstance().getContext();
        resourceMap = context.getResourceMap(getClass());
    }

    @Override
    public void install() {
        // TODO: Initialize the Plugin prior to setting installed to true.
        
        installed = true;
    }

    @Override
    public void ready() {
        // TODO: Implement ready
    }

    @Override
    public void activate() {
        // TODO: Perform any activation that may be required.
        activated = true;
    }

    @Override
    public void deactivate() {
        // TODO: Perform any deactivation that may be required.
        activated = false;
    }

    @Override
    public boolean isDeactivated() {
        return !activated;
    }

    @Override
    public void uninstall() {
        // TODO: Perform any required cleanup when uninstalled
        installed = false;
    }

    @Override
    public boolean isUninstalled() {
        return !installed;
    }

    @Override
    public List<ActionMap> mergeGlobalActions() {
        List<ActionMap> actionMaps = new ArrayList<>();
        
        actionMaps.add(context.getActionMap(PluginActions.class, 
                PluginActions.getInstance()));
        
        return actionMaps;
    }

    @Override
    public List<HelpSet> mergeHelpSets() {
        List<HelpSet> helpSets = new ArrayList<>();
        
        return helpSets;
    }

    @Override
    public String getPluginName() {
        return resourceMap.getString("plugin.name");
    }

    @Override
    public String getPluginVersion() {
        return resourceMap.getString("plugin.version");
    }

    @Override
    public String getPluginDescription() {
        return resourceMap.getString("plugin.description");
    }

    @Override
    public boolean shouldInstall() {
        return Preferences.userNodeForPackage(getClass())
                .getBoolean("installed", true);
    }

    @Override
    public boolean shouldActivate() {
        return Preferences.userNodeForPackage(getClass())
                .getBoolean("activated", true);
    }
    
    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    
    private boolean installed = false;
    private boolean activated = false;
    
}
