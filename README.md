# Northwind Truckers' Operating System

## About NTOS

Northwind Truckers' Operating System (NTOS) is an all-in-one load-tracking, *per diem* calculating, and basic accounting system for one-truck Owner/Operator truck drivers.

There are only two accounting systems available for these small-business owners, and neither of them are full-featured. Both of those accounting systems do the basic accounting for one-truck operations, but neither of them provide for managing the other aspects of the business. NTOS aims to fill this void.

Not only will NTOS do basic accounting for the small-business trucker, but it will also manage their load history, customers, and automatically track and calculate their *per diem* tax write-off. 

## Roadmap
2024-07-20: At this point in time, the NTOS Project is getting off of the ground. We have our main window, an Options dialog, an About dialog, and a Plugin Manager has been started. We are starting to build a docking infrastructure for the main window which will be utilized by the various plugins and modules that will be created. Currently, the Project consists of five sub-projects: four modules (which are required by the application) and one plugin (which is optional). This is the naming convention that we have chosen to describe whether something is required to be present or is an optional addition. 

Sub-projects that provide a feature that is required to be present for the application to function are called "modules", such as `ntos-base-module` and `ntos-startup-module`. Modules will be installed and activated at application startup and will not be able to be disabled nor uninstalled via the *Plugin Manager*.

Sub-project that provide optional features are called "plugins", such as the `ntos-timekeeping-plugin`. These plugins will be able to be installed/uninstalled and activated/deactivated via the *Plugin Manager* interface. While the *modules* will always be present, they typically only provide features for the application itself, which make the application functional. Plugins, on the other hand, represent features the make the application **useful**. Without the plugins, NTOS would not be much.

Currently, the `ntos-startup-module` is used to launch the application, both in and out of the IDE. However, *it is very important to note* that while running in the IDE, the "`--ide`" command line parameter needs to be passed to the `ntos-startup-module` in order to properly debug the application. When this parameter is present, the restart function of the application will not work and, once the application shuts down, Maven will report a build failure due to exit status 81. This is proper, because exit status 81 is the integer value of `SysExits.EX_RESTART`, which is what is passed to the `exit` methods whenever the application should be restarted. However, by having `--ide` present within the development environment, you will be able to see all exception messages and be able to debug using breakpoints. Once you have worked out the kinks in your code, you can remove the `--ide` parameter from the `ntos-startup-module` sub-project properties in order to test whether restart logic that you may have created works properly.

With that seque, by not placing the `--ide` command line parameter in the `ntos-startup-module`'s properties, the restart functionality will work as expected. Whenever an `exit` method is called with `SysExits.EX_RESTART` or `SysExits.EX_RESTART_UPDATE` as the exit status, the application will shutdown, exit, and then launch again. However, as long as the `--ide` command line parameter is not supplied, the debugger will not attach to the created sub-process, so debugging will not be possible. Under normal circumstances while working on any sub-project, it is a best practice to supply the `--ide` command line parameter to the `ntos-startup-module` sub-project in order to avoid a lot of frustration.

## Contributing
Contributors are welcome! We need help with all aspects of this project. Coders, tech writers, translators, graphics artists...All are welcome and appreciated!

## Authors and acknowledgment
Current authors and contributors:

| Name | Location | Contribution |
|-     |-         |-             |
|Sean Carrick | Illinois, USA | Owner and Project Lead |
|Jiri Kovalsky | Buhimin, Czech Republic | Owner and Testing Lead |
|Kevin Nathan | Arizona, USA | Contributor-at-Large |

## License
Northwind Truckers' Operating System is being developed and will be released under the terms of the GNU General Public License

## Project status
Northwind Truckers' Operating System is currently moving along.
