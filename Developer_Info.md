# Developer Information
### Northwind Truckers' Operating System
#### NTOS Project

## Table of Contents
- [About This Document](#About-This-Document)
- [Supported Command Line Parameters](#Supported-Command-Line-Parameters)
- [Notes on the Customer Class](#Notes-on-the-Customer-Class)
- [The Data Module Project](#The-Data-Module-Project)
- [UI Enhancements](#UI-Enhancements)

## About This Document
This document is here so that developers of the *NTOS Project* can all stay on the same page regarding the development of the project. We will all need to update this document with information that every developer and/or contributor may need to have a more enjoyable time working on this project.

## Supported Command Line Parameters
The following table lists and explains the command line parameters that are supported by the *NTOS Project*. The first column shows the proper command line parameter switch and any arguments to it that may be required (or could be optional). The second column provides detailed information regarding how and when to use the command line parameter, including its effect on the application.

| Switch | Explanation and Purpose |
| -      | -                       |
| `--ide`| This switch takes no arguments and is simply a boolean value (either it is present or not). Its purpose is to set an application preference (takes place in the Application.initialize method) that can be checked throughout the application. Its presence can be used to perform alternate methods when in debugging mode than when in production mode. |
| `--debug` or `--config` | These two switches allow for setting the logging level. These are mostly for when the application is being used in a production environment. |
| `--show-arrivals` | This switch shows a window into the `ARRIVAL_AUDITS` table in the database, which shows early and late arrivals at stops. |

### Using the Preferences System when in the IDE
If you have the `--ide` switch set on your `ntos-startup-module` project, the `Application` superclass sets a flag (represented by `PreferenceKeys.PREF_DEVELOPMENT_ENVIRONMENT`) in the application's `Preferences` system. This makes the fact that the application is running in development mode available to the entire super-project, and all sub-projects, at runtime.

The reason this is important is that when using the `Preferences` system, when in development the application runs as your user account. On Linux and Mac, this means there is no write access to the Java Preferences API system location. Therefore, if you get the `Preferences` like this next example, you will see error messages in the output window while the application is running.

Example of using the Preferences API normally:

```java
public boolean save() {
    Preferences prefs = context.getPreferences(ApplicationContext.SYSTEM);

    /* If running from the IDE, you are running under your user account.
     * Therefore, on Linux and Mac, you will see errors printed in the
     * Output window, stating that preferences cannot be written.
     */
}
```

So, since the `Application` superclass stores the fact that the `--ide` switch is present or not, we can use that to our advantage:

```java
public boolean save() {
    Preferences prefs = (context.getPreferences(ApplicationContext.USER)
            .getBoolean(PreferenceKeys.PREF_DEVELOPMENT_ENVIRONMENT, false)
            ? context.getPreferences(ApplicationContext.USER)
            : context.getPreferences(ApplicationContext.SYSTEM));

    /* Now, we will use the user preferences node if we are running in the IDE,
     * so we can test that our settings are being saved properly. However, if
     * we are running from an installed location, the application is executing
     * as the administrative user, so the system preferences node is accessible.
     */
}
```

This is how we can use the fact that the `--ide` switch presence is stored in the user node of the preferences system to our advantage. By using the ternary operator for getting the required preferences node, we will not need to hunt down all of our preference node requests and change them prior to releasing the application.

Just like with the Preferences API, we can do the same type of thing with the Logging API. In fact, the `ApplicationContext` takes care of this for us, if we get our `Logger` from it. If the `--ide` switch is present at launch, the `Logger`s are set to the `Level.FINEST` setting so that we can have verbose logs while we are developing and testing the project.

Once the application is released, the `Logging.setLevel` will default to `Level.INFO`. However, if a user is having some issues (bugs or errors), we can have them modify the application shortcut to include one of `--debug` or `--config` switches to make the `Logger`s more verbose.

The `--debug` switch sets the `Logger.setLevel` to `Level.FINEST`, just like the `--ide` switch does. On the other hand, the `--config` switch sets the `Logger.setLevel` to the `Level.CONFIG` switch. Whereas the `Level.CONFIG` is more verbose than `Level.INFO`, it is not as verbose as `Level.FINEST`. Either way, we can help the user to generate log files that are more useful for us to help fix their issues.

## Notes on the Customer Class
This is something new that I just started working on today (2024-10-17).

The `Customer` class now stores ***all customers***, including *brokers* and *agents*. Earlier iterations of the project had a separate table for brokers and agents. This was due to having none of the fields being required (and all accepting `NULL` values) for the brokers/agents table. However, this caused data redundancy because brokers/agents have the same information as all other customers. Therefore, by keeping them in a separate table, we had two tables that had virtually *identical* definitions, though one had more fields that were required to have values.

Because of the duplicate table definitions, I did not like how I was doing things. Therefore, I set about trying to figure out how to condense brokers/agents with the customers table. After a lot of thought on this subject, I realized that the table in the database does not need to enforce the requirement of the data. Instead, the `Customer` class could enforce the data requirements, and do so in a much more flexible manner, thereby allow us to store brokers and agents in the same table in which shippers, receivers, and vendors are stored.

Once I had this realization, I set about updating the `Customer` class to reflect this new design idea. I also changed the `CUSTOMERS` table definition file to reflect that it would no longer enforce data requirements for those data points that may not be present for a broker or agent. Within these changes, I created the `private boolean isBrokerOrAgent` method to determine whether the current `Customer` object represents a broker, agent, or other customer type.

With that method in place, for the fields (properties) that are required for shippers, receivers, and vendors, the setter methods check if the `customerType` property has been set. If it has been, it only enforces the requirement for a value if the `customerType` is ***not*** a broker or agent. If the `customerType` has not been set, then the `Customer` class has to assume that the data is required for the field, so it enforces the data requirements.

### What are the Data Requirements for a Customer?
The data requirements for **all** customers are:

- `customerId` - **Must be** *unique* in the table. The uniqueness is checked at the time the record is saved. To check uniqueness, the table is searched for all records that start with the same characters as the new `customerId` value. All IDs that start with those characters are counted and a two-digit number is added to the end of the new `customerId`, if necessary. This allows for 100 customers to start with the same eight characters.

   For example, for a customer with the company name "Johnson Company", which is located in Seattle, WA, the automatic way of generating the `customerId` would be to take the first three letters of the company and city name, as well as the two-character state/province abbreviation. This would create the `customerId = "JOHSEAWA"`. When this record is added to the database, the uniqueness check method will find all records that have the same eight characters starting their `CUSTOMER_ID` and count them. If, for example there were three other customers whose IDs started with "JOHSEAWA", then this new record would have its ID updated to be "JOHSEAWA03". It would tack on "03" instead of "04" because the first record entered with those eight characters as its ID would not have anything appended to it. So, the second one entered would be "JOHSEAWA01", the third would be "JOHSEAWA02", etc. The reason for this logic is that it is simple to implement and will allow for up to 100 customers with the same eight characters first in their ID, which is not likely.
- `defaultAccount` - This is required for the default account into which revenue from this customer is deposited, or expenses for this customer are withdrawn.
- `customerType` - This is required so that we will be able to create views into this table for each type of customer. For example, there could be a view that shows only vendors, another one for only shippers, etc. To the user, it will look like shippers are in their own table, vendors are in their own table, and so forth. Definitionally speaking, however, all of those types of customers will be in the same table in the database.
- `inactive` - This is a simple boolean value, so even if it was not required, it would still be present. This is because even if the user never clicks on a `JCheckBox`, we would pull its value, which would be `false`.

The data that is **never required** for *any* customer type is:

- `suite` - A suite number for a customer's street address
- `contact` - The name of the contact person at the customer.
- `phone` - The customer's phone number
- `fax` - The customer's fax number
- `email` - The customer's email address
- `notes` - The notes on the customer

The fields that are required when the `customerType` **is not** a broker or agent:

- `street` - The street address
- `city` - The city in which they are located
- `region` - The state or province in which the city is located
- `postalCode` - The US Zip or Canadian Postal code
- `country` - The country in which the customer is situated

### What does this Mean for Developers?
This will only affect developers by placing a rule on the order in which the properties of a `Customer` object are set. For the save method of a GUI window where the user enters the data for a customer, the order of setting the properties will need to start with setting the `customerType` property first. This will allow for the `isBrokerOrAgent` method to be able to do its job. This will also prevent `NullPointerException`s from being thrown when setting the data for a broker or agent.

In other words, follow this example for setting the `Customer` properties in a save method:

```java
@AppAction(enabledProperty = "dirty")
public void save(ActionEvent evt) {
    Customer c = new Customer();

    c.setCustomerType(customerTypeField.getSelectedItem());
    // set the rest of the properties.

    // Save the record to the database:
    c.saveRecord();
}
```
As you can see, if you set the `customerType` property first, then all appropriate requirement checks will work fine.

I hope this explanation helps everyone to better understand the `Customer` class and how the `CUSTOMERS` table in the database is defined.

## The Data Module Project
I am currently in the process of updating all of the Data Access Objects (DAO). I had created one class for each of the tables in a somewhat haphazard manner and am now attempting to make them better.

For example, when I had generated *Entity Classes from Database*, the JPA Entities had various query definitions at the top of the class. Also, each entity that had a many-to-one relationship to another entity had a property that returned a `java.util.Collection` of all records in the other table that matched that record on the relationship field. As an example, look at this relationship between the `CUSTOMER_TYPES` and `CUSTOMERS` tables:

![`CUSTOMER_TYPES`->`CUSTOMERS` Relationship](.images/Customer-Types-Relationship.png "Relationship between CUSTOMER_TYPES and CUSTOMERS tables").

As you can see, `CUSTOMER_TYPES` has a one-to-many relationship to the `CUSTOMERS` table. Therefore, in a JPA Entity class, the `CustomerTypes` class had a property of `customers` (defined as `Collection<Customers>`). While this is a pretty cool feature of JPA, I didn't think it was enough to justify using JPA. However, the `EntityManager` of JPA did interest me a lot. Again, however, I didn't feel that there was enough control within the application to make JPA worthwhile. So I moved back to using my own Data Access Objects.

In my first iteration of the DAO for the *NTOS Project*, I tried to mimic some of the features of JPA. In this attempt, I created `public static final String` fields for the various *standard* selects that would be needed, but left it up to other classes to perform them as needed. In this attempt, I thought that a lot of what I was doing in the UI classes would be better served if it were in the DAOs. So, I moved onto my second iteration.

In attempt two, I moved the selection of records into the DAO classes and tried to mimic the JPA by having a collection of `Customer` objects within the `CustomerType` object for the type of the current `CustomerType` record. However, this led to all kinds of recursive *stack overflow* errors. So, I do not know how they are able to do that in the JPA Entity classes. Frustration set in, so I moved onto the current iteration of the DAO.

In this iteration, I have removed the selection fields from the classes and placed field name fields there instead. So, for the `CustomerType` DAO class, the following fields are defined:

```java
public class CustomerType extends AbstractBean implements Serializable, PropertyKeys {

    public static final String FLD_ID = "TYPE_ID";
    public static final String FLD_NAME = "TYPE_NAME";
    public static final String FLD_DESC = "DESCRIPTION";

    // The rest of the class...

}
```

With those fields in place, other classes can build their own select, insert, update, and delete statements, which will make it way more flexible. However, I felt that the DAO class should provide the methodology for actually performing the select, insert, update, and delete actions. To this end, I added methods to the DAO class, such as:

```java
public class CustomerType extends AbstractBean implements Serializable, PropertyKeys {

    public static final String FLD_ID = "TYPE_ID";
    public static final String FLD_NAME = "TYPE_NAME";
    public static final String FLD_DESC = "DESCRIPTION";

    public static List<CustomerType> getAllRecords() {
        return selectRecords(null, null);
    }

    public static List<CustomerType> getAllRecords(String orderBy) {
        return selectRecords(null, orderBy);
    }

    public static CustomerType findByPrimaryKey(long id) {
        String where = FLD_ID + " = " + id;
        List<CustomerType> records = selectRecords(where, null);

        if (records.isEmpty()) {
            return null;
        } else {
            return records.get(0);
        }
    }

    public static List<CustomerType> findAllRecords(String where) {
        return selectRecords(where, null);
    }

    public static List<CustomerType> findAllRecords(String where, String orderBy) {
        return selectRecords(where, orderBy);
    }

    // and on...

    private static List<CustomerType> selectRecords(String where, String orderBy) {
        String sql = "SELECT * FROM APP.CUSTOMER_TYPES";
        if (where != null) {
            if (where.startsWith("WHERE")) {
                sql += " " + where;
            } else if (where.startsWith(" WHERE")) {
                sql += where;
            } else {
                sql += " WHERE " + where;
            }
        }
        if (orderBy != null) {
            if (orderBy.startsWith("ORDER BY")) {
                sql += " " + orderBy;
            } else if (orderBy.startsWith(" ORDER BY")) {
                sql += orderBy;
            } else {
                sql += " ORDER BY " + orderBy;
            }
        }

        // The rest of the selection logic.
    }

    // The rest of the class...

}
```

As you can see in the code snippet above, I moved all of the selection logic into the DAO class, and only had one method that actually performs the selection of the data. This way, there is only a single point to debug if something does not work as expected. In the previous iteration of the DAO classes, one would have to search various methods within the DAO class, as well as other locations within the UI classes to find where the issue was located. This iteration of the DAO classes simplifies debugging moving forward.

Besides the fields and select methods being defined statically on the DAO class, saving a record is defined as an instance method within the DAO object:

```java
public class CustomerType extends AbstractBean implements Serializable, PropertyKeys {

    // Everything from above, as well as the constructors, getters, and setters.

    public void saveRecord(boolean nue) throws ValidationException {
        validate();

        if (nue) {
            insert(this);
        } else {
            update(this);
        }
    }

}
```

The `saveRecord` method takes a parameter (`nue`) to specify that it is a new record being added to the database. This allows the single `saveRecord` method to be called from the UI classes' `save` or `doSave` `AppAction` methods:

```java
public class SomeFrame extends JFrame implements Dockable {

    private boolean nue = false;

    // The rest of the class...use your imagination.

    @AppAction(enabledProperty = "dirty")
    public void save(ActionEvent evt) {
        CustomerType t = new CustomerType();
        t.setTypeName(typeNameField.getText());
        t.setDescription(descriptionField.getText());

        t.saveRecord(nue);

        typeNameField.setEditable(false);
        descriptionField.setEditable(false);

        setDirty(false);
    }

    @AppAction
    public void addRecord(ActionEvent evt) {
        nue = true;
        typeNameField.setEditable(true);
        descriptionField.setEditable(true);
    }

    @AppAction
    public void editRecord(ActionEvent evt) {
        nue = false;
        typeNameField.setEditable(true);
        descriptionField.setEditable(true);
    }

}
```

Because of having the instance method `saveRecord`, saving a record is simple. In the `addRecord` action event, the `nue` flag is set to true. Likewise, in the `editRecord` action event, the `nue` flag is set to false. This way, when the single `save` action event is used, the record object (`c` in this example) can save itself back to the database.

I am currently working on making all of the DAO classes work the same way as just described. At this point, `AccountType`, `Account`, and `CustomerType` are completed, so you can check them out to see what I'm doing. I am currently working on the `Customer` class (which has a lot of work to be completed), then I will move down the *Projects* window alphabetically. Once all of the DAO classes are updated as just described, I will begin testing them.

One reason that these updates are taking longer than I expected is that I am attempting to provide quality documentation on these classes. However, I am also trying to keep the documentation somewhat generic so that I can copy and paste it from class-to-class as I update them. If you find any errors or typos (or copy/paste mistakes), please feel free to fix them.

### Which Database are We Using?
To answer this question, I'm just going to say, "***Check out the `Database` class and you will be able to figure this out for yourself.***". When checking out that class, *pay special attentention to* the enclosed `enum` at the top of the class. Also, note that there is a `META-INF` folder in the module's *Other Sources* directory, which contains a `Database.properties` file. This file contains the settings for the database to use. Therefore, if you would prefer to use a *MariaDB* installation that you have on your system, you can change the settings here and add the dependency to the top-level project `POM` file. If you do this, when you commit your changes to the repository, simply ***exclude*** your `Database.properties` file. *Also, you may want to keep a backup copy of it elsewhere on your system so that you can **easily** replace it after you pull changes from the repository*.

The enclosed `enum` provides support for the following databases:

- Derby (Embedded) - *the default for the project*
- Derby (Server)
- H2DB
- HSQLDB
- IBM_DB2
- MariaDB
- Microsoft SQL
- MySQL
- Oracle DB
- PostGreSQL

The `enum` provides the appropriate JDBC URL and driver class name to use for each of the database systems. By the time the project is completed and ready to be released "*into the wild*", we should have all of the database drivers included in the POM as dependencies. That way, we will be able to package all of those JAR files in our installer program (which we will need to write or purchase).

One last note on the `Database` class: I did my best to provide quality documentation on the class and public methods. There are also some inline comments disbursed throughout the class. If you see any typos or errors in the information (*especially* in the JavaDocs), please fix it.

## UI Enhancements

I have added a sub-package in the `ntos-common-module`, named `com.pekinsoft.ntos.common.ui`,
which contains various UI features, such as in the `text` sub-package the class named
`MarkdownEditorKit`, and the `javax.swing.JTextField` subclass named `JLimitedTextField`.
I am hoping that these classes will be useful in developing the UI for the Project.













